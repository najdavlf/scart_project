import numpy as np
import sys

if len(sys.argv)<3 : 
  print("Usage: python %s mode nsca"%sys.argv[0])
  print("""  mode :
    0 : from beg to end
    1 : from beg+2 to end
    2 : from beg to end-1
    3 : from beg+2 to end-1
    4 : from beg to beg+2 and from end-1 to end
    5 : from ground to end
  nsca = minimum nb of scattering or surface if mode 5""")
  exit()

mode=int(sys.argv[1])
nsca_min=int(sys.argv[2])
maxlin=500

if mode==0:
  dbeg=0 ; dend=0
elif mode==1:
  dbeg=1 ; dend=0
elif mode==2:
  dbeg=0 ; dend=1
elif mode==3:
  dbeg=2 ; dend=1
elif mode==4:
  dbeg=1 ; dend=1
else:
  dbeg=2; dend=1

ilin=0
piv = 0 # 
niv = 0 # number of vertices in a path
nsf = 0 # number of surface hit
lines=""
verts=""
for vertex in open("all_paths.dat"):
  vspl = vertex.split()
  if len(vspl)<3: # new path
    if niv==0: continue               # beginning, skip as there is no line to add
    if (mode<5 and niv-piv>1+nsca_min)\
    or (mode==5 and nsf == nsca_min):  # number of vertices > nsca_min or nb reflections = nb hit surf max ?
      print("new ray")
      print(verts.split("\n")[piv:])
      if niv-dend-(piv+dbeg) > 1:     # at least one vertex ?
        if mode==4 : # one line for beginning and one for end
          lines += "\nl "+" ".join(["%i"%(i+1) for i in range(piv,piv+dbeg+1)])
          lines += "\nl "+" ".join(["%i"%(i+1) for i in range(niv-dend,niv+1)])
        else:        # a single line for the whole path
          lines += "\nl "+" ".join(["%i"%(i+1) for i in range(piv+dbeg, niv-dend)])
        ilin += 1
        if ilin>maxlin : break
    piv = niv
    nsf = 0
  else : # continue path
    vsplf = [float(u) for u in vspl]
    if vsplf[-1] < 1e-3: nsf += 1
    #if mode == 5 and nsf > nsca_min: continue # when mode = 5, nsca_min is nref_max
    niv = niv+1 # add a vertex
    verts += "v %f %f %f\n"%(vsplf[0], vsplf[1], vsplf[2])

fic=open("all_paths_%i_%i.obj"%(mode,nsca_min),"w")
fic.write(verts)
fic.write(lines)
fic.close()
