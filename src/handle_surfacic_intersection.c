#include "main.h"
#include "scart_context.h"
#include "handle_surfacic_intersection.h"
#include "utils.h"
#include "const.h"
#include <star/ssp.h>
#include <star/s3d.h>
#include <omp.h>

res_T
get_surface_properties(struct context * ctx, struct s3d_hit hit, struct intersection * inte) {
  INIT_RES ;
  int trouve = 0 ;
  int i ;
  int ith = omp_get_thread_num() ;

  for (i=0; i<ctx->scn_dat.surfaces_number; i++) {
    if (hit.prim.geom_id == ctx->scn_dat.surfaces[i].id) { 
      if (trouve==1) {
        res = RES_BAD_OP;
        goto error;
      }
      inte->type = ctx->scn_dat.surfaces[i].type;
      inte->ind_surf = hit.prim.geom_id;
      trouve = 1;
    }
  }

  if (inte->type==BOA) inte->albedo = ctx->grd_dat.albedo ;

  if (ctx->print==1){
    fprintf(ctx->debug[ith], "\ngeom_id %i, scene_id %i, identifiant %i\n", 
            hit.prim.geom_id, hit.prim.scene_prim_id, inte->ind_surf);
  }
exit:
  return res ;
error: 
  goto exit;
}

/***** BOA *****/
res_T
handle_ground_intersection(
    struct ssp_rng* rng,
    struct context* ctx,
    struct ray* r,
    struct event* ev,
    struct intersection* inte,
    struct results* results) 
{
  res_T res = RES_OK;
  int ith = omp_get_thread_num();
  const size_t nbflx = (ctx->nbflx)/(ctx->nbbds);
  const size_t ispec = (size_t)(r->ph.ibd*ctx->write_spectral); /* 0 or ibd */
  const size_t nbnet = ctx->nbnet/(ctx->nbbds);
  const size_t nbabs = ctx->nbabs/(ctx->nbbds);
  const size_t nbsyf = ctx->nbsyf/(ctx->nbbds);
  size_t ind, ind_h; 

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "with weight %g\n", r->ph.w);
    fflush(ctx->debug[ith]) ;
  }

  /* Counting downward flux and transmission */
  if (results != NULL) {
    ind = get_e_ind(ctx,ev->location,0,&ind_h);
    results->tmp_fdn[ind]+=r->ph.w;
    /* if do_ad, count only if inside ad (ind_h == 3) */
    if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
      results->tmp_net[0]+=r->ph.w;
      results->tmp_abs[0]+=r->ph.w;
      results->tmp_transm += r->ph.w/ctx->sun_dat.w_dni[ith];
    }
    if (ctx->direct_diffuse) {
      if (is_direct(ctx,r)) {
        results->tmp_dir[ind] += r->ph.w;
      } else {
        results->tmp_difdn[ind] += r->ph.w;
      }
    }
    /* if do_ad, count only if inside ad (ind_h == 3) */
    /* if photon wasn't reflected too many times */
    if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
    (r->ph.is_reflected < ctx->max_reflections_symb)) {
      results->tmp_fdn_alb[r->ph.is_reflected]+=r->ph.w;
    }
  }

  /* Reflecting particle */
  res = reflect(rng, ctx, r, inte, &(ev->has_scattered));

  if (results != NULL) {
    /* Counting upward flux */
    ind = get_e_ind(ctx,ev->location,0, &ind_h);
    results->tmp_fup[ind] += r->ph.w;
    /* if do_ad, count only if inside ad (ind_h == 3) */
    if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
      results->tmp_net[0] -= r->ph.w; /* upward = negative */
      results->tmp_abs[0] -= r->ph.w;
    }
    if (ctx->direct_diffuse>0) { /* only diffuse is possible */
      results->tmp_difup[ind] += r->ph.w;
    }
    /* if do_ad, count only if inside ad (ind_h == 3) */
    /* if symbolic albedo is on and photon wasn't reflected too many times */
    if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
    (r->ph.is_reflected < ctx->max_reflections_symb)) {
      results->tmp_fup_alb[r->ph.is_reflected] += r->ph.w;
    }
  }

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "with weight %g\n", r->ph.w);
    fflush(ctx->debug[ith]) ;
  }

  return res ;
}
/***************/

/***** TOA *****/
res_T
handle_toa_intersection(struct context * ctx, 
                        struct ray * r, 
                        struct event *ev, 
                        struct results * results) 
{
  res_T res = RES_OK;
  const size_t iz = ctx->atm_dat.nblv-1;
  const struct scart_surface surf = ctx->scn_dat.surfaces[iz];
  const double up[3] = {0.,0.,1} ;
  const double down[3] = {0.,0.,-1.};
  size_t ind_h;
  size_t ind = get_e_ind(ctx,ev->location,iz, &ind_h);
  size_t ind_spec;
  const size_t nbflx = ctx->nbflx/(ctx->nbbds);
  const size_t nbnet = ctx->nbnet/(ctx->nbbds);
  const size_t nbabs = ctx->nbabs/(ctx->nbbds);
  const size_t nbsyf = ctx->nbsyf/(ctx->nbbds);
  size_t ispec = (size_t)(r->ph.ibd*ctx->write_spectral); /* 0 or ibd */
  const int ith = omp_get_thread_num() ;
  int ind_sym;

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "with weight %g\n", r->ph.w);
    fflush(ctx->debug[ith]) ;
  }

  if (results!=NULL) {
    ind_sym = ((int)nbnet-1)*(ctx->max_reflections_symb) + r->ph.is_reflected;
    /* Counting contrib to fluxes at TOA */
    if (d3_dot(up,r->dir) > 0) { /* upward */
      results->tmp_fup[ind] += r->ph.w;
      /* if do_ad, count only if inside ad (ind_h == 3) */
      if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
        results->tmp_net[nbnet-1] -= r->ph.w; /* upward = negative */
        results->tmp_abs[nbabs-1] += r->ph.w;
        results->tmp_albedo += r->ph.w/ctx->sun_dat.w_dni[ith]; /* TOA albedo */
      }
      if (ctx->print==6) {
        fprintf(ctx->debug[ith], "outward TOA %f %f %f %f %lu\n", SPLIT3(ev->location), r->ph.w, ind);
      }
      if (ctx->direct_diffuse) { /* direct or diffuse depending on direction */
        ASSERT(~is_direct(ctx,r));
        results->tmp_difup[ind] += r->ph.w;
      }
      /* if symbolic albedo is on and photon wasn't reflected too many times */
      if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
      (r->ph.is_reflected < ctx->max_reflections_symb)) {
        results->tmp_fup_alb[ind_sym] += r->ph.w;
      }
    } else { /* downward */
      results->tmp_fdn[ind] += r->ph.w;
      /* if do_ad, count only if inside ad (ind_h == 3) */
      if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
        results->tmp_net[nbnet-1] += r->ph.w; /* upward = negative */
      }
      if (ctx->direct_diffuse) { /* downward at TOA can only be direct */
        ASSERT(is_direct(ctx,r));
        results->tmp_dir[ind] += r->ph.w;
      }
      if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
      (r->ph.is_reflected < ctx->max_reflections_symb)) {
        results->tmp_fdn_alb[ind_sym] += r->ph.w;
      }
    }
  }

  /* End path if upward */
  if (d3_dot(up,r->dir) > 0) { /* upward = escape => stop path */
    r->over = 1 ;
  }

  /* change ke hat */
  ind_spec=(r->ph.ibd)*(ctx->atm_dat.nbgp) + (r->ph.igp);
  if (d3_dot(down,r->dir) > 0.) { /* downward => in */
    ctx->ke_hat[ith] = surf.ke_hat_gas_below[ind_spec] +
                       KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith] +
                       KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith];
  } else {
    ctx->ke_hat[ith] = surf.ke_hat_gas_above[ind_spec] +
                       KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith] +
                       KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith];
  }

  return res ;
}
/***************/

/*** VIRTUAL ***/
res_T
handle_virtual_intersection(struct context * ctx,
                            struct ray * r,
                            struct event *ev,
                            int iz,
                            struct results * results) 
{
  res_T res = RES_OK;
  const struct scart_surface surf = ctx->scn_dat.surfaces[iz];
  const double down[3] = {0.,0.,-1.};
  size_t ispec = (size_t)(r->ph.ibd*ctx->write_spectral); /* 0 or ibd */
  size_t ind_h;
  size_t ind = get_e_ind(ctx,ev->location,iz,&ind_h);
  size_t ind_spec = (r->ph.ibd)*(ctx->atm_dat.nbgp) + (r->ph.igp);
  const size_t nbflx = ctx->nbflx/(ctx->nbbds);
  const size_t nbnet = ctx->nbnet/(ctx->nbbds);
  const size_t nbabs = ctx->nbabs/(ctx->nbbds);
  const size_t nbsyf = ctx->nbsyf/(ctx->nbbds);
  int ind_sym;
  int ith = omp_get_thread_num() ;

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "with weight %g\n", r->ph.w);
    fflush(ctx->debug[ith]) ;
  }
  if (0) printf("event at z = %g recorded at ind z %lu, e ind %lu, nbsurfs %lu\n", 
                      ev->location[2], iz, ind, ctx->scn_dat.surfaces_number);

  if (results != NULL) {
    ind_sym = iz*(ctx->max_reflections_symb)+r->ph.is_reflected;
    if (d3_dot(down,r->dir) > 0) { /* downward => in */
      results->tmp_fdn[ind] += r->ph.w;
      if (ctx->direct_diffuse>0) {
        if (is_direct(ctx,r)>0) {
          results->tmp_dir[ind] += r->ph.w;
        } else {
          results->tmp_difdn[ind] += r->ph.w;
        }
      }
      if (iz < ctx->atm_dat.nblv) { /* not LES bbox */
        /* if do_ad, count only if inside ad (ind_h == 3) */
        /* and photon wasn't reflected too many times */
        if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
        (r->ph.is_reflected < ctx->max_reflections_symb)) {
          results->tmp_fdn_alb[ind_sym] += r->ph.w;
        }
        /* if do_ad, count only if inside ad (ind_h == 3) */
        if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
          results->tmp_net[iz] += r->ph.w;
        }
      }
    } else { /* upward */
      results->tmp_fup[ind] += r->ph.w;
      if (ctx->direct_diffuse>0) {
        ASSERT(~is_direct(ctx,r));
        results->tmp_difup[ind] += r->ph.w;
      }
      if (iz < ctx->atm_dat.nblv) { /* not LES bbox */
        /* if do_ad, count only if inside ad (ind_h == 3) */
        /* and photon wasn't reflected too many times */
        if ((!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) && 
        (r->ph.is_reflected < ctx->max_reflections_symb)) {
          results->tmp_fup_alb[ind_sym] += r->ph.w;
        }
        /* if do_ad, count only if inside ad (ind_h == 3) */
        if (!(ctx->do_ad) || (ctx->do_ad && ind_h == 3)) {
          results->tmp_net[iz] -= r->ph.w;
        }
      }
    }
  }

  /* khat max between the two layers ; removes khat < k situations (numerical errors) */
  ctx->ke_hat[ith] = fmax(surf.ke_hat_gas_below[ind_spec] +
                       KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith] +
                       KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith],
                       surf.ke_hat_gas_above[ind_spec] +
                       KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith] +
                       KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith]);

  if (ctx->print==7) { 
    fprintf(ctx->debug[ith], "at height %g surface %lu of type %i\n",
        surf.height, surf.id, surf.type);
    fprintf(ctx->debug[ith], "max ql below %g max qi below %g\nmse liq %g mse ice %g\n",
      surf.ql_max_below, surf.qi_max_below, ctx->liq_op_dat.mse_nu[ith], ctx->ice_op_dat.mse_nu[ith]
      );
    fprintf(ctx->debug[ith], "max ql above %g max qi above %g\nmse liq %g mse ice %g\n",
      surf.ql_max_above, surf.qi_max_above, ctx->liq_op_dat.mse_nu[ith], ctx->ice_op_dat.mse_nu[ith]
      );
    fprintf(ctx->debug[ith], "kehat in (below) = %g + %g + %g = %g\n", 
      surf.ke_hat_gas_below[ind_spec],
      KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith],
      KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith],
      surf.ke_hat_gas_below[ind_spec] +
      KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith] +
      KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith]);
    fprintf(ctx->debug[ith], "kehat out (above) = %g + %g + %g = %g\n", 
      surf.ke_hat_gas_above[ind_spec],
      KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith],
      KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith],
      surf.ke_hat_gas_above[ind_spec] +
      KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith] +
      KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith]);
    fprintf(ctx->debug[ith], "kehat was set to   %g\n", 
      ctx->ke_hat[ith]);
  }

  if (ctx->print==1) { 
    fprintf(ctx->debug[ith], "kehat in (below) = total %g gas %g liquid %g ice %g\n", 
      surf.ke_hat_gas_below[ind_spec] +
      KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith] +
      KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith],
      surf.ke_hat_gas_below[ind_spec],
      KE_HAT_MAJ*surf.ql_max_below*ctx->liq_op_dat.mse_nu[ith],
      KE_HAT_MAJ*surf.qi_max_below*ctx->ice_op_dat.mse_nu[ith]);
    fprintf(ctx->debug[ith], "kehat out (above) = total %g gas %g liquid %g ice %g\n",
      surf.ke_hat_gas_above[ind_spec] +
      KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith] +
      KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith],
      surf.ke_hat_gas_above[ind_spec],
      KE_HAT_MAJ*surf.ql_max_above*ctx->liq_op_dat.mse_nu[ith],
      KE_HAT_MAJ*surf.qi_max_above*ctx->ice_op_dat.mse_nu[ith]);
    fprintf(ctx->debug[ith], "kehat was set to   %g\n", 
      ctx->ke_hat[ith]);
  }
  return res ;
}
/***************/

res_T
handle_surfacic_intersection(struct ssp_rng *rng,
                             struct context * ctx,
                             struct ray * r,
                             struct event * ev,
                             struct results * results) 
{
  res_T res = RES_OK;
  INIT_MSG ;
  int ith=omp_get_thread_num() ;
  struct intersection inte ;

  inte = INTERSECTION_NULL;

  res = get_surface_properties(ctx, r->hit, &inte);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not get surface properties\n",FUNC_NAME);
    goto error;
  }

  switch (inte.type) {
  
    case BOA :
      if (ctx->print==1) { fprintf(ctx->debug[ith], "of type BOA\n"); 
      } else if (ctx->print==2) {
        fprintf(ctx->debug[ith], "%g\t%g\t%g\n", ev->location[0], ev->location[1], ev->location[2]) ;
        fflush(ctx->debug[ith]) ;
      } else if (ctx->print==3) { /* pour analyser la distribution angulaire au BOA */
        fprintf(ctx->debug[ith], "%i\t%g\t%g\t%g\t%g\t%i\n", r->ph.id, r->ph.w, r->dir[0], r->dir[1], r->dir[2],r->ph.is_scattered);
      } else if (ctx->print==5) {
        fprintf(ctx->debug[ith],"%u %i %i %g %g %g %g BOA\n",r->ph.id,r->ph.is_scattered,r->ph.is_reflected,r->dir[0],r->dir[1],r->dir[2],r->ph.w);
        fflush(ctx->debug[ith]);
      }

      res = handle_ground_intersection(rng, ctx, r, ev, &inte, results);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR, 
            "%s: could not get handle ground intersection\n",FUNC_NAME);
        goto error;
      }
      if (ev->has_scattered) {
        ev->type_event = SURF_REF;
      } else {
        ev->type_event = SURF_ABS;
      }
      ev->filter = 1;
      break ;

    case TOA :
      if (ctx->print==1) { fprintf(ctx->debug[ith], "of type TOA\n");}
      res = handle_toa_intersection(ctx, r, ev, results);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR, 
            "%s: could not get handle TOA intersection\n",FUNC_NAME);
        goto error;
      }
      ev->type_event = SURF_TOA;

      if (ctx->print==1) { fprintf(ctx->debug[ith], "of type TOA\n"); 
      } else if (ctx->print==2) {
        fprintf(ctx->debug[ith], "%g\t%g\t%g\n", ev->location[0], ev->location[1], ev->location[2]) ;
        fflush(ctx->debug[ith]) ;
      } else if (ctx->print==3) { /* pour analyser la distribution angulaire au BOA et compter 0 si retour TOA*/
	fprintf(ctx->debug[ith], "%i\t%g\t%g\t%g\t%g\t%i\n", r->ph.id, r->ph.w, r->dir[0], r->dir[1], r->dir[2],r->ph.is_scattered) ;
      } else if (ctx->print==5) {
        fprintf(ctx->debug[ith],"%u %i %i %g %g %g %g TOA\n",r->ph.id,r->ph.is_scattered,r->ph.is_reflected,r->dir[0],r->dir[1],r->dir[2],r->ph.w);
        fflush(ctx->debug[ith]);
      }

      ev->filter = 1 ;
      break ;

    case VIRTUAL :
      if (ctx->print==1) { fprintf(ctx->debug[ith], "of type virtual\n");}
      res = handle_virtual_intersection(ctx, r, ev, inte.ind_surf, results);
      ev->type_event = SURF_NULL;
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR, 
            "%s: could not get handle virtual intersection\n",FUNC_NAME);
        goto error;
      }

      if (ctx->print==1) { fprintf(ctx->debug[ith], "of type COUNT\n") ;
      } else if (ctx->print==4) {
        /*fprintf(ctx.debug[ith], "# path\t# scattering\t# level\tuz\tz [km]\tx [km]\ty [km]\n");*/
	      fprintf(ctx->debug[ith], "%i\t%i\t%i\t%g\t%g\t%g\t%g\n",r->ph.id, r->ph.is_scattered, inte.ind_surf, r->dir[2], ev->location[2], ev->location[0], ev->location[1]);
      }
      ev->filter = 1 ;
      break ;
  }

exit: 
  if (ctx->print) fflush(ctx->debug[ith]);
  return res ;
error:
  goto exit;
}
