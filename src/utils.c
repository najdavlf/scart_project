#include "main.h"
#include "scart_context.h"
#include "utils.h"
#include "stdio.h"
#include "stdlib.h"
#include "const.h"
#include "math.h"
#include <star/ssp.h>
#include <omp.h>

double
get_max(double vec[], size_t size, int*ind) {
  size_t i ;
  double res ; 
  *ind=0 ;
  res = vec[*ind] ;
  for (i=1;i<size;i++) {
    if (vec[i]>res) {
      res=vec[i] ;
      *ind = i ; 
    }
  }
  return res ;
}

double 
rand_interp_lin(struct ssp_rng* rng, double x1, double x2, double y1, double y2, double x) {
  double y;
  double p1 = (x2-x)/(x2-x1);
  double r = ssp_rng_canonical(rng);
  if (r<p1) {
    y = y1;
  } else {
    y = y2;
  }
  return y;
}

double 
interp_lin(double x1, double x2, double y1, double y2, double x) {
  return y1 + (x-x1)/(x2-x1)*(y2-y1) ;
}

double
T(double theta, double P) {
  return theta*pow(Pref/P,-R/(M_DA*CP_DA)) ; /* K */
}

double 
rho(double P, double T) {
  return P*M_DA/(R*T) ;   /* kg.m^-3 */
}

double
eta(double nu) {
  /* nu in nm */
  /* eta in cm^-1 */
  return 1./(nu*pow(10.,-7)) ;
}

size_t
find_index_rev(double * tab, size_t size, double val) {
  size_t j=0 ;
  if (tab[1]>tab[0]) {
    fprintf(stderr, "warning: find_index_rev: tab is not reverse ordered\n");
    goto exit;
  }
  if (tab[j]<val-1e-9){
    fprintf(stderr, "warning: find_index_rev: tab max %e is still smaller than value %e\n", tab[j], val-1e-9);
    goto exit;
  }
  if (tab[size-1]>val) {
    fprintf(stderr, "warning: find_index_rev : tab min %f is already greater than value %f\n",tab[size-1],val) ;
    j = size-1;
    goto exit;
  }

  while ((j+1<size) & (tab[j+1]>val)) {
    j++ ;
  }

exit:
  return j;
error:
  goto exit;
}

size_t
find_index_after_value(double * tab, size_t size, double val) {
  /* finds the index in ordered tab of the first value > val */
  size_t j=0 ;
  if (tab[0]>tab[1]) {
    fprintf(stderr, " ! ERROR find_index_after_value: tab is not ordered\n");
    goto error ;
  }
  while ((j<size) & (tab[j]<val)) {
    j++ ;
  }
error : 
  return j ;
}

size_t
get_z_ind(struct context * ctx, double z) {
  size_t ind ;
  if (ctx->les_dat.z_reg) {
    /*printf("iz before trunc %f \t", z/(ctx->les_dat.zmax-ctx->les_dat.zmin)*(double)ctx->les_dat.nbz);*/
    ind = (size_t)(z/(ctx->les_dat.zmax-ctx->les_dat.zmin)*(double)ctx->les_dat.nbz) ;
    /*printf("after trunc %lu \n", ind);*/
  } else { /* look for last interface between layers that is below z */ 
    ind = find_index(ctx->les_dat.lev_tab, ctx->les_dat.nbz+1, z) ;
  }
  if (ind==ctx->les_dat.nbz+1) ind-- ;
  return ind ;
}

/* in case we want to average over a particular domain,
   ind=0 is outside, ind=1 is inside */
size_t  get_mod_ind_ad(const double pos, double dom_size, size_t nb_cells, double coords[])
{
  double tmp ;
  size_t ind = 0;
  tmp = (pos - floor(pos/dom_size)*dom_size); /* position in true domain */
  if (tmp >= coords[0] && tmp<= coords[1]) {
    ind=1;
  } else {
    ind=0;
  }
  /*printf("true pos %f, mod pos %f, min %f, inside ? %i\n", pos, tmp, coords[0], ind);*/
  return ind ;
}

size_t 
get_mod_ind(const double pos, double dom_size, size_t nb_cells)
{
  double tmp ;
  size_t ind = 0;
  tmp = (pos - floor(pos/dom_size)*dom_size); /* position in true domain */
  ind = floor(tmp/dom_size*nb_cells);         /* index in domain */
  if (ind == nb_cells) ind-- ;
  /*printf("pos %f ; dom size %f ; nb cells %lu ; ind %lu\n", pos, dom_size, nb_cells, ind);*/
  /*if (pos > 0) {
    tmp = pos - floor(pos/dom_size)*nb_cells;
  } else {
    tmp = pos + floor(-pos/dom_size)*nb_cells;
  }
  ind = floor(tmp/dom_size)*nb_cells;
  printf("pos %f ; tmp %f ; ind %lu\n", pos, tmp, ind);*/
  return ind ;
}

/* Cyclic horizontal boundaries */
size_t
get_x_ind(struct context * ctx, double x, int do_ica) {
  int ith = omp_get_thread_num() ;
  size_t ind=get_mod_ind(x,ctx->les_dat.xmax-ctx->les_dat.xmin,ctx->les_dat.nbx);
  if (do_ica) {
    ind = (ctx->I0[ith] + (size_t)ind%ctx->ica);
  }
  if (ctx->print==1) fprintf(ctx->debug[ith],"\nPosition %g at index %i (do_ica is %i)",x,ind,do_ica);
  return ind ;
}

size_t
get_y_ind(struct context * ctx, double y, int do_ica) {
  int ith = omp_get_thread_num() ;
  size_t ind=get_mod_ind(y,ctx->les_dat.ymax-ctx->les_dat.ymin,ctx->les_dat.nby);
  if (do_ica) {
    ind = ctx->J0[ith] + (size_t)ind%ctx->ica;
  }
  if (ctx->print==1) fprintf(ctx->debug[ith],"\nPosition %g at index %i (do_ica is %i)\n",y,ind,do_ica);
  return ind ;
}

size_t 
find_index(double * tab, size_t size, double val) {
  /* finds the index in ordered tab of the last value < val */
  size_t j=0 ;
  if ((size==1)) {
    if((tab[0]!=val)) {
      fprintf(stderr, " ! WARNING: tab is of size 1 and does not contain %f\n", val);
    }
    goto exit ;
  }
  if (tab[0]>tab[1]) {
    fprintf(stderr, " ! ERROR find_index: tab is not ordered\n");
    goto error ;
  }
  if (tab[0]>val){
    return 0;
  }
  while ((j+1<size) & (tab[j+1]<val)) {
    j++ ;
  }
exit :
  return j;
error : 
  goto exit;
}

res_T
get_cart_dir(const double theta, const double phi, double* sun_dir)
{ /* sun downward --> theta=0 => z = -1 */
  res_T res=RES_OK;
  double theta_rad = theta/180.*PI;
  double phi_rad = phi/180.*PI;
  sun_dir[0] = -(sin(theta_rad)*cos(phi_rad));
  sun_dir[1] = -(sin(theta_rad)*sin(phi_rad));
  sun_dir[2] = -(cos(theta_rad));
  return res ;
}

size_t 
get_horizontal_index(struct context * ctx, double pos[])
{
  size_t ind;
  size_t ix_map,iy_map;

  if (ctx->ica) { /* If ica, find the cell corresponding to the ica domain */
    /* First, get ix,iy index of the pos[0],pos[1] location transposed into the ICA subdomain */
    size_t ix_ica = get_x_ind(ctx,pos[0],1);
    size_t iy_ica = get_y_ind(ctx,pos[1],1);
    if (ctx->map_ica) { /* Direct correspondance between ica cells and map cells */
      ix_map = (ix_ica) / (ctx->ica) ;
      iy_map = (iy_ica) / (ctx->ica) ;
    } else {        /* ICA but no correspondance with map (nbx,nby) => need position */
      size_t ix_raw = get_x_ind(ctx,pos[0],0) ; /* true x index in raw data */
      size_t iy_raw = get_y_ind(ctx,pos[1],0) ; /* true y index in raw data */
      double xpos_ica = pos[0] + ctx->les_dat.dx*((int)ix_ica - (int)ix_raw);
      double ypos_ica = pos[1] + ctx->les_dat.dy*((int)iy_ica - (int)iy_raw);
      if (ctx->do_ad) {
        ix_map = get_mod_ind_ad(xpos_ica, (ctx->les_dat.xmax-ctx->les_dat.xmin),ctx->scn_dat.nbx, ctx->ad_coordinates[0]);
        iy_map = get_mod_ind_ad(ypos_ica, (ctx->les_dat.ymax-ctx->les_dat.ymin),ctx->scn_dat.nby, ctx->ad_coordinates[1]);
      } else {
        ix_map = get_mod_ind(xpos_ica,(ctx->les_dat.xmax-ctx->les_dat.xmin),ctx->scn_dat.nbx); 
        iy_map = get_mod_ind(ypos_ica,(ctx->les_dat.ymax-ctx->les_dat.ymin),ctx->scn_dat.nby); 
      }
    }
  } else { /* No ICA : old behaviour */
    if (ctx->do_ad) {
      ix_map = get_mod_ind_ad(pos[0],(ctx->les_dat.xmax-ctx->les_dat.xmin),ctx->scn_dat.nbx, ctx->ad_coordinates[0]); 
      iy_map = get_mod_ind_ad(pos[1],(ctx->les_dat.ymax-ctx->les_dat.ymin),ctx->scn_dat.nby, ctx->ad_coordinates[1]); 
    } else {
      ix_map = get_mod_ind(pos[0],(ctx->les_dat.xmax-ctx->les_dat.xmin),ctx->scn_dat.nbx);
      iy_map = get_mod_ind(pos[1],(ctx->les_dat.ymax-ctx->les_dat.ymin),ctx->scn_dat.nby);
    }
  }
   
  ind = ix_map + iy_map*ctx->scn_dat.nbx;

  return ind;
}

size_t
get_e_ind(struct context * ctx, double pos[], size_t iz, size_t * horizontal_index) {
  size_t ind;
  /*
  double down[3] = {0.,0.,-1.};
  int ith = omp_get_thread_num();
  */

  /* ind from 0 to nbx*nby*ctx->atm_dat.nblv, each pixel of each level */
  *horizontal_index = get_horizontal_index(ctx, pos);
  ind = *horizontal_index + iz*ctx->scn_dat.nbx*ctx->scn_dat.nby;
  
  /*
  if (ctx->print==1){
    fprintf(ctx->debug[ith], "Hit position %f %f %f - ", pos[0], pos[1], pos[2]);
    if (d3_dot(dir,down) > 0) { fprintf(ctx->debug[ith], "Downward "); }
    else { fprintf(ctx->debug[ith], "Upward "); }
    fprintf(ctx->debug[ith], "counter updated at %i %i %lu, ind %lu \t",
        ix_map,iy_map, iz, ind);
  }
  */
  return ind ;
}

double 
fmin(double a , double b) { 
  return a<b ? a:b ;
}
double 
fmax(double a , double b) { 
  return a>b ? a:b ;
}
