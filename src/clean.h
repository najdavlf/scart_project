res_T clean_scn(struct scn_data * sd);
res_T clean_col(struct col_data * cd);
res_T clean_les(struct les_data * ld);
res_T clean_atm(struct atm_data * ad);
res_T clean_cop(struct cop_data * cd);
