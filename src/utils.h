#ifndef UTILS_H
#define UTILS_H

double
get_max(double vec[], size_t size, int*indmax);

double 
rand_interp_lin(struct ssp_rng* rng, double x1, double x2, double y1, double y2, double x);
double 
interp_lin(double x1, double x2, double y1, double y2, double x);

double
T(double theta, double P);

double 
rho(double P, double T);

double
eta(double nu);

size_t
get_x_ind(struct context * ctx, double x, int do_ica);
size_t
get_y_ind(struct context * ctx, double y, int do_ica);
size_t
get_z_ind(struct context * ctx, double z);
size_t
get_mod_ind(double pos, double dom_size, size_t nb_cells);

size_t
find_index_rev(double * tab, size_t size, double val);
size_t
find_index_after_value(double * tab, size_t size, double val);
size_t
find_index(double * tab, size_t size, double val);

res_T
get_cart_dir(const double theta, const double phi, double * sun_dir);

size_t
get_e_ind(struct context * ctx, double pos[], size_t iz, size_t * horizontal_index);

double 
fmin(double a , double b); 
double 
fmax(double a , double b); 

int 
get_I(struct ray * r);
int
get_J(struct ray * r);

#endif
