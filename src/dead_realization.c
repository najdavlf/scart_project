#include "main.h"
#include "scart_context.h"
#include "const.h"
#include "realization.h"
#include "utils.h"
#include <star/ssp.h>
#include <star/smc.h>
#include <star/s3d.h>
#include <time.h>
#include <omp.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

res_T
realization(void* value, struct ssp_rng* rng, const unsigned ithread, void* Ncontext) {
  res_T res = RES_OK;

  struct smc_doubleN_context * Nctx = (struct smc_doubleN_context *)Ncontext ;
  struct context * ctx = (struct context *)Nctx->integrand_data ;         
  size_t Nctx_count = (size_t)Nctx->count; /* Le nombre de grandeurs calculées */

  const size_t maxsy = ctx->max_reflections_symb;
  const size_t nbbds = ctx->nbbds;
  const size_t nbabs = ctx->nbabs;
  const size_t nbnet = ctx->nbnet;
  const size_t nbsya = nbabs*maxsy;  /* length of symbolic vector */
  const size_t nbsyf = nbnet*maxsy;  /* length of symbolic vector */
  const size_t nbmsi = ctx->nbmsi;   /* only one viewing dir */
  const size_t nbflx = ctx->nbflx;   /* flux */
  const size_t nbdir = ctx->direct_diffuse*ctx->nbflx; /* flux dir */
  const size_t nbdif = ctx->direct_diffuse*ctx->nbflx; /* flux dif */
  const size_t nbsya2 = nbabs*(1+maxsy)*maxsy/2.;
  const size_t nbsyf2 = nbnet*(1+maxsy)*maxsy/2.;

  ASSERT(Nctx_count == 2*nbflx + nbdir + 2*nbdif +
                       nbmsi + nbnet + nbabs +
                       nbsya  + 2*nbsyf  + /* coefficients abs and fluxes */
                       nbsya  + 2*nbsyf2 + /* covariances abs and fluxes */
                       1 + 2);

  struct ray r ; 
  size_t i,j,k,iz,izi,izj ;
  size_t start, sstart;
  const size_t mod=1e5;
  const int ith = (int)ithread;
  const size_t nth = (size_t)omp_get_num_procs() ;
  static size_t real_num_all = 0;
  char file_debug[70];

  struct results results = ctx->results[ith];

  const time_t abstime = time(NULL);
  const struct tm ltime=*localtime(&abstime);
  int ibd;


  for (i=0; i<nth; i++) {
    ctx->first[i] = 1;
    ctx->real_num[i] = (ctx->nbsteps)/nth*i;
  }

  real_num_all++;
  if (real_num_all % mod == 0) 
   logger_print(&ctx->logger,LOG_OUTPUT,
      "%lu : %02i/%02i - %02i:%02i:%02i \n",(real_num_all), ltime.tm_mday, ltime.tm_mon+1, ltime.tm_hour, ltime.tm_min, ltime.tm_sec);

  ctx->real_num[ith]++;

  if (ctx->print==0) {
    ctx->debug[ith] = fopen(NULL, "w") ;
  }
  else if (ctx->print==1) {
      sprintf(file_debug, "debug%02i.txt", ith+1) ;
      ctx->debug[ith] = fopen(file_debug, "w") ;
      fprintf(ctx->debug[ith], "I am %i\n",ith);
      fprintf(ctx->debug[ith],"les zmin,zmax %g %g\natm zmin,zmax %g %g\nscn zmin,zmax %g %g\nall zboa,ztoa %g %g\n",
      ctx->les_dat.zmin,ctx->les_dat.zmax,
      ctx->atm_dat.zmin,ctx->atm_dat.zmax,
      ctx->scn_dat.zmin,ctx->scn_dat.zmax,
      ctx->zboa,ctx->ztoa);
      fflush(ctx->debug[ith]) ;
  } else if (ctx->print==2) {
    sprintf(file_debug, "path%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ;
    fprintf(ctx->debug[ith],"\n"); 
  } else if (ctx->print==3) {
    sprintf(file_debug, "angle%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else if (ctx->print==4) {
    sprintf(file_debug, "horizontal_sza%4g_proc%02i_dscale%01i.txt", ctx->the[ith], ith+1, ctx->delta_scaling) ;
    ctx->debug[ith] = fopen(file_debug, "a") ;
    if (ctx->first[ith]==1) {
    fprintf(ctx->debug[ith],"// SZA [°] %g, wavelength %gnm, LES %s, gas %i, mie %i, asym %g, ground albedo %g, nbsteps %lu\n",ctx->the[ith], ctx->mono_nu, ctx->les_dat.les_file, ctx->col_dat.atmosphere, ctx->liq_op_dat.cop_file!=NULL, ctx->liq_op_dat.g, ctx->grd_dat.albedo, ctx->nbsteps) ;
    fprintf(ctx->debug[ith], "# path\t# scattering\t# level\tuz\tz [km]\tx [km]\ty [km]\t# x0\t# xL\t# y0\t# yL\tw [-]\n");
    ctx->first[ith]=0 ;
    }
  } else if (ctx->print==5) {
    sprintf(file_debug, "directions%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else if (ctx->print==6) {
    printf("debug 6\n");
    sprintf(file_debug, "inds%02i.txt", ith+1);
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else {
    ctx->debug[ith] = fopen(NULL, "w") ;
    printf("Invalid print %i",ctx->print); ctx->print = 0;
  }

  for (i=0;i<nbnet/nbbds;i++){
    results.net[i] = 0.;
  }
  for (i=0;i<nbabs/nbbds;i++){
    results.abs[i] = 0.;
  }
  for (i=0;i<nbflx/nbbds;i++) {
    results.fdn[i] = 0.;
    results.fup[i] = 0.;
  }
  if (ctx->direct_diffuse) {
    for (i=0;i<nbdir/nbbds;i++) {
      results.dir[i] = 0.;
    }
    for (i=0;i<nbdif/nbbds;i++) {
      results.difdn[i] = 0.;
      results.difup[i] = 0.;
    }
  }
  if (ctx->do_msi) {
    for (i=0;i<nbmsi/nbbds;i++){
      results.msi[i] = 0.;
    }
  }
  if (maxsy) {
    for (i=0;i<nbsyf/nbbds;i++) {
      results.fup_alb[i] = 0;
      results.fdn_alb[i] = 0;
    }
    for (i=0;i<nbsya/nbbds;i++) {
      results.abs_alb[i] = 0;
    }
  }

  results.path_len = 0;
  results.albedo = 0;
  results.transm = 0;

  r = RAY_NULL;
  r.ph.id = real_num_all ;

  res = sample_sun(rng,ctx);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: could not sample sun\n",FUNC_NAME);
    goto error;
  }

  res = emission(rng,ctx,&r);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: could not emit particle\n",FUNC_NAME);
    goto error;
  }

  /* In ICA mode, saving origin horizontal location */
  if (ctx->ica) {
    size_t ix = (size_t)get_x_ind(ctx,r.org[0],0);
    size_t iy = (size_t)get_y_ind(ctx,r.org[1],0);
    ctx->I0[ith] = (ix/ctx->ica)*ctx->ica ; 
    ctx->J0[ith] = (iy/ctx->ica)*ctx->ica ; 
    if (ctx->print==1){
      fprintf(ctx->debug[ith],"Emitted at position %g %g\n",r.org[0],r.org[1]);
      fprintf(ctx->debug[ith],"This is raw indexes %lu %lu\n", ix,iy) ;
      fprintf(ctx->debug[ith],"Upper left cell of ICA subdomain %lu %lu\n", ctx->I0[ith],ctx->J0[ith]);
    }
  }

  res = set_cop_nu(ctx, &ctx->liq_op_dat, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set Mie data for wavelength %f\n",FUNC_NAME, r.ph.nu);
    goto error;
  }

  res = set_cop_nu(ctx, &ctx->ice_op_dat, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set Mie data for wavelength %f\n",FUNC_NAME, r.ph.nu);
    goto error;
  }

  res = set_atm_gp(ctx, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set atm data for iband %lu, gpoint %lu\n",FUNC_NAME, r.ph.ibd, r.ph.igp);
    goto error;
  }

  ctx->ke_hat[ith] = KE_HAT_NULL;

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "\n%i emitted at %g %g %g\n", r.ph.id, r.org[0], r.org[1], r.org[2]) ;
    fprintf(ctx->debug[ith], "with lambda %g in band %i and qd pt %i\n",r.ph.nu,r.ph.ibd, r.ph.igp);
    fprintf(ctx->debug[ith], "my g is %g and ssa %g\n",
          ctx->liq_op_dat.g, ctx->liq_op_dat.ssa);
  } else if (ctx->print==2) {
    fprintf(ctx->debug[ith], "%g\t%g\t%g\n", r.org[0], r.org[1], r.org[2]) ;
  }

  res = path(rng, ctx, &r, &results);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not compute path %lu\n",FUNC_NAME, real_num_all);
    goto error;
  }

  for (i=0; i<Nctx_count; i++) {
    SMC_DOUBLEN(value)[i] = 0;
  }

  ibd = nbbds > 1 ? r.ph.ibd : 0 ;
  start = 0;
  for (i=0;i<nbflx/nbbds;i++) {
    SMC_DOUBLEN(value)[ibd*(nbflx/nbbds) + i+start] = results.fdn[i];
  }
  start += nbflx;
  for (i=0;i<nbflx/nbbds;i++) {
    SMC_DOUBLEN(value)[ibd*(nbflx/nbbds) + i+start] = results.fup[i];
  }
  start += nbflx;
  if (ctx->direct_diffuse) {
    for (i=0;i<nbdir/nbbds;i++) {
      SMC_DOUBLEN(value)[ibd*(nbdir/nbbds)+i+start] = results.dir[i] ;
    }
    start += nbdir;
    for (i=0;i<nbdif/nbbds;i++) {
      SMC_DOUBLEN(value)[ibd*(nbdif/nbbds)+i+start] = results.difdn[i] ;
    }
    start += nbdif;
    for (i=0;i<nbdif/nbbds;i++) {
      SMC_DOUBLEN(value)[ibd*(nbdif/nbbds)+i+start] = results.difup[i] ;
    }
    start += nbdif;
  }
  for (i=0; i<nbabs/nbbds; i++) {
    SMC_DOUBLEN(value)[ibd*(nbabs/nbbds)+i+start] = results.abs[i];
  }
  start += nbabs;
  for (i=0; i<nbnet/nbbds; i++) {
    SMC_DOUBLEN(value)[ibd*(nbnet/nbbds)+i+start] = results.net[i];
  }
  start += nbnet;
  if (ctx->do_msi) {
    for (i=0; i<nbmsi/nbbds; i++) {
      SMC_DOUBLEN(value)[ibd*(nbmsi/nbbds)+i+start] = results.msi[i];
    }
    start += nbmsi;
  }

  if (maxsy>0) {
    /* z0 1..100 , z1 1..100 , .. , zmax 1..100 */
    /* z0 1 1..100 2 1..100 .. 100 1..100 , .. , zmax 1 1..100 2 1..100 .. 100 1..100 */
    sstart = start + 2*nbsyf + nbsya;
    for (iz=0; iz<nbnet/nbbds; iz++) { /* for each level */
      for (i=0; i<maxsy; i++) { /* for each nb ref */
        izi = iz*maxsy + i;
        SMC_DOUBLEN(value)[ibd*nbsyf/nbbds + start+       izi] = results.fdn_alb[izi];
        SMC_DOUBLEN(value)[ibd*nbsyf/nbbds + start+nbsyf+ izi] = results.fup_alb[izi];
        for (j=0;j<=i;j++) {
            izj = iz*maxsy + j;
            k = iz*(1+maxsy)*maxsy/2 + i*(i+1)/2 + j;
            SMC_DOUBLEN(value)[ibd*nbsyf2/nbbds +sstart+  k] = 
              results.fdn_alb[izi]*results.fdn_alb[izj];
            SMC_DOUBLEN(value)[ibd*nbsyf2/nbbds +sstart+ nbsyf2 +  k] = 
              results.fup_alb[izi]*results.fup_alb[izj];
        }
      }
    }
    for (iz=0; iz<nbabs/nbbds; iz++) { /* for each level */
      for (i=0; i<maxsy; i++) {
        izi = iz*maxsy + i;
        SMC_DOUBLEN(value)[ibd*nbsya/nbbds + start+nbsyf*2+izi] = results.abs_alb[izi];
        SMC_DOUBLEN(value)[ibd*nbsya/nbbds + sstart+nbsyf2*2+izi] = 
          results.abs_alb[izi]*results.abs_alb[izi];
      }
    }
    start = sstart + 2*nbsyf2 + nbsya;
  }

  SMC_DOUBLEN(value)[start  ] = results.path_len;
  SMC_DOUBLEN(value)[start+1] = results.transm/ctx->sun_dat.w_dni[ith]; /* down ground */
  SMC_DOUBLEN(value)[start+2] = results.albedo/ctx->sun_dat.w_dni[ith]; /* up toa */

exit:
  if (ctx->print){fflush(ctx->debug[ith]); fclose(ctx->debug[ith]); ctx->debug[ith]=NULL;}
  return res;
error : 
  if ((ctx->print==1)) {
    char file_bugged[70]; 
    sprintf(file_bugged,"bug-%09lu.out", real_num_all); 
    rename(file_debug,file_bugged); 
  }
  goto exit;
}
