#include "main.h"
#include "scart_context.h"
#include "netCDF.h"
#include "omp.h"
#include "utils.h"

double 
get_atm_ka(struct atm_data * ad, double height, int iband, int gpoint){
  size_t ind;
  size_t ilay; /* layer 0 = between 0 (level 0) and dz (level 1), layer 1 = between dz and 2dz */
  ilay = find_index(ad->vertical_levels, ad->nblv, height);
  ind  = iband*(ad->nbgp)*(ad->nbly) /* begin band i */ + gpoint*(ad->nbly) /* begin gpoint */ + ilay;
  return ad->ka[ind];
}

double
get_atm_ks(struct atm_data * ad, double height, int iband, int gpoint){
  size_t ind;
  size_t ilay; /* layer 0 = between 0 (level 0) and dz (level 1), layer 1 = between dz and 2dz */
  ilay = find_index(ad->vertical_levels, ad->nblv, height);
  ind  = iband*(ad->nbgp)*(ad->nbly) /* begin band i */ + gpoint*(ad->nbly) /* begin gpoint */ + ilay;
  return ad->ks[ind];
}

double
get_atm_ke(struct atm_data * ad, double height, int iband, int gpoint){
  return get_atm_ka(ad, height, iband, gpoint) + get_atm_ks(ad, height, iband, gpoint);
}

res_T 
clean_atm(struct atm_data * ad) {
  int i;
  res_T res = RES_OK;
  if (ad->vertical_levels!=NULL) mem_rm(ad->vertical_levels);
  if (ad->vertical_layers!=NULL) mem_rm(ad->vertical_layers);
  if (ad->z_tab!=NULL)           mem_rm(ad->z_tab);
  if (ad->nu_tab!=NULL)          mem_rm(ad->nu_tab);
  if (ad->ncISF!=NULL)           mem_rm(ad->ncISF);
  if (ad->gp_nb!=NULL)           mem_rm(ad->gp_nb);
  if (ad->wg!=NULL)              mem_rm(ad->wg);
  if (ad->ka!=NULL)              mem_rm(ad->ka);
  if (ad->ks!=NULL)              mem_rm(ad->ks);
  if (ad->ssa!=NULL)             mem_rm(ad->ssa);
  return res ;
}

res_T 
set_atm_ncdf_dims(int ncid, struct atm_data * ad) {
  INIT_RES ;
  size_t ind[1] ;
  ad->nblv = get_dim(ncid, vname) ;
  ad->nbly = get_dim(ncid, lname) ;
  ad->nbbd = get_dim(ncid, bands) ;
  ad->nbgp = get_dim(ncid, nbqua) ;
  ad->nbnu = get_dim(ncid, wname) ;
  return res ;
}
 

res_T
set_atm_ncdf_fields(int ncid, struct atm_data * ad) {
  INIT_RES ;
  INIT_MSG ;
  int i ;
  size_t ind[1] = {1} ;
  size_t count[1] ;
  count[0] = ad->nbly ;

  res = get_tab(ncid, tisfn, &(ad->totISF)) ;

  (ad->vertical_levels) = (double*)mem_alloc(ad->nblv*sizeof(*ad->vertical_levels));
  res = get_tab(ncid, vname, ad->vertical_levels) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s in %s\n", vname, ad->atm_file);
    goto error;
  }

  ad->zmin = ad->vertical_levels[0];
  ad->zmax = ad->vertical_levels[ad->nblv-1];

  (ad->vertical_layers) = (double*)mem_alloc(ad->nbly*sizeof(*ad->vertical_layers)) ;
  res = get_tab(ncid, lname, ad->vertical_layers) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s in %s\n", lname, ad->atm_file);
    goto error;
  }

  (ad->z_tab) = (double*)mem_alloc(ad->nbly*sizeof(*ad->z_tab)) ;
  /* we don't want the 0 level => get_tab_from 1 
   * ztab[i] is the position of the upper interface of layer i */
  res = get_tab_from(ncid, zname, ad->z_tab, ind, count) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", zname) ;
  goto error ;
  }

  (ad->gp_nb) = (double*)mem_alloc(ad->nbbd*sizeof(*ad->gp_nb)) ;
  res = get_tab(ncid, gpnbn, ad->gp_nb) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", gpnbn) ;
    goto error ;
  }

  (ad->nu_tab) = (double*)mem_alloc(ad->nbnu*sizeof(*ad->nu_tab)) ;
  res = get_tab(ncid, wname, ad->nu_tab) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", wname) ;
    goto error ;
  }

  (ad->ncISF) = (double*)mem_alloc(ad->nbbd*sizeof(*ad->ncISF)) ;
  res = get_tab(ncid, isfna, ad->ncISF) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", isfna) ;
    goto error ;
  }

  (ad->wg) = (double*)mem_alloc(ad->nbbd*ad->nbgp*sizeof(*ad->wg)) ;
  res = get_tab(ncid, wgnam, ad->wg) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", wgnam) ;
    goto error ;
  }

  (ad->ka) = (double*)mem_alloc(ad->nbbd*ad->nbgp*ad->nbly*sizeof(*ad->ka)) ;
  res = get_tab(ncid, kanam, ad->ka) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", kanam) ;
    goto error ;
  }

  (ad->ks) = (double*)mem_alloc(ad->nbbd*ad->nbgp*ad->nbly*sizeof(*ad->ks)) ;
  res = get_tab(ncid, ksnam, ad->ks) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", ksnam) ;
    goto error ;
  }

  (ad->ssa) = (double*)mem_alloc(ad->nbbd*ad->nbgp*ad->nbly*sizeof(*ad->ssa)) ;
  res = get_tab(ncid, ssana, ad->ssa) ;
  if (res != RES_OK) {
    sprintf(err_message, " ! ERROR : Could not read %s\n", ssana) ;
    goto error ;
  }

error : 
    fprintf(stderr, err_message) ;
    goto exit ;
exit :
    if (res!=RES_OK) {
      if (clean_atm(ad) != RES_OK) {
        fprintf(stderr, "Could not clean atmospheric data\n");
      };
    }
  return res;
}

res_T
init_atm_data(struct logger* logger, struct atm_data * ad, char * atm_file, double zmin, double zmax, size_t nth) {
  INIT_RES ;
  INIT_MSG ; 
  size_t i;

  ad->atm_file = atm_file ;

  ad->nblv=0 ;
  ad->nbly=0 ;
  ad->zmin = 0. ;
  ad->zmax = 0. ;
  ad->z_tab=NULL ;
  ad->nu_tab=NULL;
  ad->ncISF=NULL;
  ad->gp_nb=NULL;
  ad->wg=NULL;
  ad->ka=NULL;
  ad->ks=NULL;
  ad->ssa=NULL;
  for (i=0;i<nth;i++) {
    ad->ka_g[i]=NULL;
    ad->ks_g[i]=NULL;
    ad->ssa_g[i]=NULL;
  }


  if (atm_file!=NULL) {
    int ncid ; 

    TRY(  get_ncid(atm_file, &ncid),
	  " ! ERROR : Atmospheric profiles file does not exist or is not netCDF\n");

    TRY(  set_atm_ncdf_dims(ncid, ad),
	  " ! ERROR : Dim could not be found in atmospheric profiles file\n");

    TRY(  set_atm_ncdf_fields(ncid, ad),
	  " ! ERROR : Field could not be found in atmospheric profiles file\n");

    res = put_ncid(ncid);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR, 
        "%s: could not release netCDF id\n", FUNC_NAME);
      goto error;
    }

  } else {
    ad->zmin = zmin ;
    ad->zmax = zmax ;
  }

error :
    fprintf(stderr, err_message) ;
    goto exit ;
exit : 
    NULL ;
  return res ;
}

res_T
set_atm_gp(struct context * ctx, struct photon * ptn) {
  INIT_RES ;
  int index ;
  const int ith=omp_get_thread_num();

  ASSERT(ctx->atm_dat.atm_file!=NULL); 

  index = ptn->ibd*ctx->atm_dat.nbgp*ctx->atm_dat.nbly + ptn->igp*ctx->atm_dat.nbly;

  /* profiles of ka, ks and ssa in gas, for the sampled g point */
  ctx->atm_dat.ka_g[ith]  = &(ctx->atm_dat.ka[index]);
  ctx->atm_dat.ks_g[ith]  = &(ctx->atm_dat.ks[index]);
  ctx->atm_dat.ssa_g[ith] = &(ctx->atm_dat.ssa[index]);

  return res ;
}
