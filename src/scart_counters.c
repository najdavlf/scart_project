#include <rsys/mem_allocator.h>
#include "scart_context.h"

void 
compute_mean_std(struct counter * cnt, double npaths) {
  if (cnt->size>0) {
    int i;
    for (i=0; i<cnt->size; i++) {
      cnt->w.t[i] /= npaths;
      cnt->w2.t[i] /= npaths;
      cnt->w2.t[i] = sqrt(1./npaths * (cnt->w2.t[i] - cnt->w.t[i]*cnt->w.t[i]));
    } 
  } else {
    cnt->w.s /= npaths;
    cnt->w2.s /= npaths;
    cnt->w2.s = sqrt(1./npaths * (cnt->w2.s - cnt->w.s*cnt->w.s));
  }
}

void
accum(struct counter * cnt, int ind, double w) {
  if (ind<0) {
    cnt->w.s += w;
    cnt->w2.s += w*w;
  } else {
    if (ind < cnt->size) {
      cnt->w.t[ind] += w;
      cnt->w2.t[ind] += w*w;
    } else {
      printf("error: ind %lu greater than cnt size %lu\n", ind, cnt->size);
      FATAL("stop");
    }
  }
}

void
init_counter(struct context * ctx, struct counter ** cnt, size_t size) {
  int i = 0;
  struct counter * tmp;
  tmp = MEM_CALLOC(&ctx->allocator, 1, sizeof(*tmp));
  tmp->size = size;
  if (size>0) {
    tmp->w.t = MEM_CALLOC(&ctx->allocator, tmp->size, sizeof(*tmp->w.t));
    tmp->w2.t = MEM_CALLOC(&ctx->allocator, tmp->size, sizeof(*tmp->w2.t));
    FOR_EACH(i, 0, tmp->size) {
      tmp->w.t[i]=0;
      tmp->w2.t[i]=0;
    }
  } else {
    tmp->w.s = 0.;
    tmp->w2.s = 0.;
  }
  *cnt = tmp;
}

void
clean_counter(struct context * ctx, struct counter * cnt){
  MEM_RM(&ctx->allocator, cnt->w.t);
  MEM_RM(&ctx->allocator, cnt->w2.t);
  MEM_RM(&ctx->allocator, cnt);
}
