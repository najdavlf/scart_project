#include "main.h"
#include "const.h"
#include "scart_context.h"
#include "netCDF.h"
#include "utils.h"
#include <omp.h>
#include <rsys/logger.h>
#include <star/ssp.h>

/* helper functions */

double
get_cop_ssa(struct cop_data * cd,
            double reff, /* effective radius in km */
            double veff, /* effective variance in km */
            int ilambda /* wvlgth index in cop LUT */) {

  double ssa;
  (void)veff; /* unused */

  if (cd->cop_file != NULL) {
    (void) reff; 
    ssa = cd->ssa_tab[ilambda];
  } else {
    ssa = cd->ssa;
  }
  return ssa;
}

double
get_cop_g(struct cop_data * cd,
          double reff, /* effective radius in km */
          double veff, /* effective variance in km */
          int ilambda /* wvlgth index in cop LUT */) {

  double g;
  (void)veff; /* unused */

  if (cd->cop_file != NULL) {
    (void) reff; 
    g = cd->g_tab[ilambda];
  } else {
    g = cd->g;
  }
  return g;
}

double
get_cop_se(struct cop_data * cd,
           double reff, /* effective radius in km */
           double veff, /* effective variance in km */
           int ilambda /* wvlgth index in cop LUT */) {

  double se;
  (void)veff; /* unused */

  if (cd->cop_file != NULL) {
    (void) reff; 
    se = 1000*cd->mse_tab[ilambda];
  } else {
    se = 3./2./(reff*RHO_W) ; /* km-1 */
  }
  return se;
}

double
get_cop_sa(struct cop_data * cd,
           double reff, /* effective radius in km */
           double veff, /* effective variance in km */
           int ilambda /* wvlgth index in cop LUT */) {

  double sa, se, ssa;
  (void)veff; /* unused */

  if (cd->cop_file != NULL) {
    (void) reff; (void)se; (void)ssa;
    sa = 1000*cd->msa_tab[ilambda];
  } else {
    se = get_cop_se(cd, reff, veff, ilambda);
    ssa = get_cop_ssa(cd, reff, veff, ilambda);
    sa = se*(1. - ssa);
  }
  return sa;
}

double
get_cop_ss(struct cop_data * cd,
           double reff, /* effective radius in km */
           double veff, /* effective variance in km */
           int ilambda /* wvlgth index in cop LUT */) {

  double ss, se, ssa;
  (void)veff; /* unused */

  if (cd->cop_file != NULL) {
    (void) reff; (void)se; (void)ssa;
    ss = 1000*cd->mss_tab[ilambda];
  } else {
    se = get_cop_se(cd, reff, veff, ilambda);
    ssa = get_cop_ssa(cd, reff, veff, ilambda);
    ss = se*ssa;
  }
  return ss;
}
 
res_T clean_cop(struct cop_data * cd) {
  int i;
  if (cd->mu_tab   != NULL) mem_rm(cd->mu_tab);
  if (cd->nu_tab   != NULL) mem_rm(cd->nu_tab);
  if (cd->cp_tab   != NULL) mem_rm(cd->cp_tab);
  if (cd->g_tab    != NULL) mem_rm(cd->g_tab); 
  if (cd->ssa_tab  != NULL) mem_rm(cd->ssa_tab);
  if (cd->mse_tab  != NULL) mem_rm(cd->mse_tab);
  if (cd->msa_tab  != NULL) mem_rm(cd->msa_tab);
  if (cd->mss_tab  != NULL) mem_rm(cd->mss_tab);
  if (cd->pdf_tab  != NULL) mem_rm(cd->pdf_tab);
  if (cd->icp_tab  != NULL) mem_rm(cd->icp_tab);
  return RES_OK;
}

res_T 
set_cop_ncdf_dims(int ncid,struct cop_data * cd) {
  INIT_RES ;
  cd->nbmu = get_dim(ncid,muname) ;
  cd->nbnu = get_dim(ncid,nuname) ;
  cd->nbcp = get_dim(ncid,cpname) ;
  return res ;
}

res_T
set_cop_ncdf_fields(int ncid, 
	            struct cop_data * cd,
              size_t nth) {
  res_T res = RES_OK;
  size_t i; /* nth=omp_get_num_procs() ;*/

  (cd->mu_tab) = (double*)mem_alloc(cd->nbmu*sizeof(*(cd->mu_tab))) ;
  res = get_tab(ncid, muname, cd->mu_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", muname) ;
    goto error ;
  } 

  (cd->nu_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*(cd->nu_tab))) ;
  res = get_tab(ncid, nuname, cd->nu_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", nuname) ;
    goto error ;
  }

  (cd->cp_tab) = (double*)mem_alloc(cd->nbcp*sizeof(*cd->cp_tab)) ;
  res = get_tab(ncid, cpname, cd->cp_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", cpname) ;
    goto error ;
  }

  (cd->ssa_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*cd->ssa_tab)) ;
  res = get_tab(ncid, ssaname, cd->ssa_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", ssaname) ;
    goto error ;
  }

  (cd->g_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*cd->g_tab)) ;
  res = get_tab(ncid, gname, cd->g_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", gname) ;
    goto error ;
  }

  (cd->mse_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*cd->mse_tab)) ;
  res = get_tab(ncid, mename, cd->mse_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", mename) ;
    goto error ;
  }

  (cd->mss_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*cd->mss_tab)) ;
  res = get_tab(ncid, msname, cd->mss_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", msname) ;
    goto error ;
  }

  (cd->msa_tab) = (double*)mem_alloc(cd->nbnu*sizeof(*cd->msa_tab)) ;
  res = get_tab(ncid, maname, cd->msa_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", maname) ;
    goto error ;
  }

  (cd->pdf_tab) = (double*)mem_alloc(cd->nbnu*cd->nbmu*sizeof(*cd->pdf_tab)) ;
  res = get_tab(ncid, pdfname, cd->pdf_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", pdfname) ;
    goto error ;
  }

  (cd->icp_tab) = (double*)mem_alloc(cd->nbnu*cd->nbcp*sizeof(*cd->icp_tab)) ;
  res = get_tab(ncid, icpname, cd->icp_tab) ;
  if (res != RES_OK) {
    printf("Could not read %s\n", icpname) ;
    goto error ;
  }
  
exit:
  return res;
error: 
  goto exit;
}

res_T 
init_cop_data(struct logger* logger, 
    struct cop_data * cd, 
    char * cop_file, 
    double g, double ssa, int ice, 
    size_t nth) {
  res_T res = RES_OK;
  size_t i; /* nth=omp_get_num_procs() ;*/

  cd->nbmu = 0 ;
  cd->nbnu = 0 ;
  cd->nbcp = 0 ;
  cd->rmod = 0. ;
  cd->smod = 0. ;
  cd->r2 = 0. ;
  cd->r3 = 0. ;
  cd->reff = 0.;
  cd->ssa  = 0.;
  cd->g    = 0.;
  cd->mu_tab = NULL ;
  cd->nu_tab = NULL ;
  cd->cp_tab = NULL ;
  cd->g_tab = NULL ;
  cd->ssa_tab = NULL ;
  cd->mse_tab = NULL ;
  cd->mss_tab = NULL ;
  cd->msa_tab = NULL ;
  cd->pdf_tab = NULL ;
  cd->icp_tab = NULL ;
  for (i=0;i<nth;i++) {
    cd->g_nu[i] = 0;
    cd->ssa_nu[i] = 0;
    cd->mse_nu[i] = 0;
    cd->pdf_nu[i] = NULL;
    cd->icp_nu[i] = NULL;
  }
  cd->cop_file = cop_file ;
  cd->ice = ice;

  if (cop_file!=NULL) {
    int ncid ;
    res = get_ncid(cop_file, &ncid);
    if (res != RES_OK) { 
      logger_print(logger, LOG_ERROR, 
	      ":%s: cop file does not exist or is not netCDF\n", FUNC_NAME);
      goto error;
    }

    res = set_cop_ncdf_dims(ncid, cd);
    if (res != RES_OK) {
      logger_print(logger, LOG_ERROR,
	      "%s: dim could not be found in Mie file\n", FUNC_NAME);
      goto error;
    }

    res = set_cop_ncdf_fields(ncid, cd, nth);
    if (res != RES_OK) {
      logger_print(logger, LOG_ERROR,
	      "%s: field could not be found in Mie file\n", FUNC_NAME); 
      goto error;
    }

    res = put_ncid(ncid);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR, 
        "%s: could not release netCDF id\n", FUNC_NAME);
      goto error;
    }
  } else {
    cd->g = g;
    cd->ssa = ssa;
  }

exit : 
  return res ;
error :
  goto exit ;
}

res_T
set_cop_nu(struct context * ctx, struct cop_data * cd, struct photon * ptn) {
  res_T res = RES_OK;
  const int ith = omp_get_thread_num();
  const double reff = cd->ice*ctx->les_dat.ice_reff + (1-cd->ice)*ctx->les_dat.liq_reff;
  const double veff = cd->ice*ctx->les_dat.ice_veff + (1-cd->ice)*ctx->les_dat.liq_veff;
  const int ilambda = cd->ice*ptn->ice_inu + (1-cd->ice)*ptn->liq_inu;
  const int ilambda0 = cd->ice*ptn->ice_inu0 + (1-cd->ice)*ptn->liq_inu0;
  const int ilambda1 = cd->ice*ptn->ice_inu1 + (1-cd->ice)*ptn->liq_inu1;
  size_t ind;

  /* max (mse(nu0), mse(nu1)) for null collisions */
  cd->mse_nu[ith] = fmax(get_cop_se(cd, reff, veff, ilambda0), get_cop_se(cd, reff, veff, ilambda1));

  return res ;
}
