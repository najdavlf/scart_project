#include <netcdf.h>
#include "main.h"

res_T
get_ncid(char * file, int *  ncid) {
  INIT_RES ;
  if (file!=NULL) {
    res = nc_open(file, NC_NOWRITE, ncid) ;
  } else {
    *ncid = 0 ;
    res = RES_BAD_ARG ;
  }
  return res;
}

res_T
put_ncid(int ncid) {
  res_T res = RES_OK;
  res = nc_close(ncid);
  return res;
}

size_t
get_dim(int ncid, char * dim_name) {
  int dim_id ;
  size_t dim ;

  nc_inq_dimid(ncid, dim_name, &dim_id) ;
  nc_inq_dimlen(ncid, dim_id, &dim) ;

  return dim ;
}

double
get_delta(int ncid, char * var_name) {
  int var_id ;
  double tmp, delta ;
  size_t zeros[1] = {0} ;
  size_t ones[1] = {1} ; 
  
  nc_inq_varid(ncid, var_name, &var_id) ;
  nc_get_var1_double(ncid, var_id, zeros, &tmp) ;
  nc_get_var1_double(ncid, var_id, ones,  &delta) ;
  delta -= tmp ;

  return delta ;
}

double
get_delta3d(int ncid, char * var_name) {
  int var_id ;
  double tmp, delta ;
  size_t zeros[3] = {0,0,0} ;
  size_t ones[3] = {1,0,0} ; 
  
  nc_inq_varid(ncid, var_name, &var_id) ;
  nc_get_var1_double(ncid, var_id, zeros, &tmp) ;
  nc_get_var1_double(ncid, var_id, ones,  &delta) ;
  delta -= tmp ;

  return delta ;
}

double 
get_val(int ncid, char * var_name, size_t ind[]) {
  int var_id ;
  double val ;

  nc_inq_varid(ncid,var_name, &var_id) ;
  nc_get_var1_double(ncid, var_id, ind, &val) ;

  return val ;
}

res_T
get_tab(int ncid, char * var_name, double * tab) {
  INIT_RES ;
  int ncerr;
  int var_id ;
  
  if (nc_inq_varid(ncid,var_name, &var_id)!=0) {
    printf("err inq varid\n");res=RES_BAD_OP ;
  } else {
    ncerr = nc_get_var_double(ncid, var_id, tab);
    if (ncerr!=0) {
      res = RES_BAD_OP ; printf("err get var\n");
    }
  }

  return res ;
}

res_T
get_tab_from(int ncid, char * var_name, double * tab, size_t ind[], size_t count[]) {
  INIT_RES ;
  int var_id ;
  if ((nc_inq_varid(ncid,var_name, &var_id)!=0)
  | (nc_get_vara_double(ncid, var_id, ind, count, tab))) {
  res = RES_BAD_OP ;
  }

  return res ;
}
