#include "main.h"
#include "conf.h"
#include "scart_context.h"
#include "write_res.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rsys/mem_allocator.h>
#include <omp.h>

static void
print_out(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[1m\x1b[32m>\x1b[0m %s", msg);
}

static void
print_err(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[31merror:\x1b[0m %s", msg);
}

static void
print_warn(const char* msg, void* ctx)
{
  (void)ctx; /* Avoid the "unused variable" warning */
  fprintf(stderr, "\x1b[33mwarning:\x1b[0m %s", msg);
}

static void
is_init(struct context *ctx)
{
  ctx->is_init_allocator=0;
  ctx->is_init_logger=0;
  ctx->is_init_s3d=0;
  ctx->is_init_sun_dat=0;
  ctx->is_init_grd_dat=0;
  ctx->is_init_col_dat=0;
  ctx->is_init_scn_dat=0;
  ctx->is_init_atm_dat=0;
  ctx->is_init_liq_dat=0;
  ctx->is_init_ice_dat=0;
  ctx->is_init_les_dat=0;
  ctx->is_init_ctx=0;
  ctx->is_init_conf=0;
  ctx->is_solved=0;
}

res_T
integrate(struct context * ctx,
          size_t nbpaths,
          double tolerance)
{
  res_T res = RES_OK;
  size_t ith, nthreads;
  int64_t ipath, npaths;
  double d_npaths = (double)nbpaths;
  double * tmp;
  struct ssp_rng_proxy* rng_proxy = NULL;
  struct ssp_rng** rngs = NULL;
  size_t i;
  struct results * results;
  struct results * m_res;
  const size_t nbbds = ctx->write_spectral? ctx->atm_dat.nbbd : 1;
  const size_t nbflx  = nbbds*ctx->scn_dat.nbx*ctx->scn_dat.nby*ctx->scn_dat.surfaces_number; 
  const size_t nbres  = nbbds*(ctx->scn_dat.nbx*ctx->scn_dat.nby*ctx->atm_dat.nblv);
  const size_t nbabs  = nbbds*(ctx->atm_dat.nbly+2);
  const size_t nbnet  = nbbds*(ctx->atm_dat.nblv);
  const size_t nbmsi  = nbbds*(ctx->scn_dat.nbx*ctx->scn_dat.nby*ctx->do_msi);
  const size_t maxsy  = ctx->max_reflections_symb;
  const size_t nbsya  = nbabs*maxsy;
  const size_t nbsyf  = nbnet*maxsy;
  const size_t nbsya2 = nbabs*(maxsy*maxsy);
  const size_t nbsyf2 = nbnet*(maxsy*maxsy);

  nthreads = ctx->nthreads; /*(size_t)omp_get_num_procs();*/

  ctx->nbbds = nbbds;
  ctx->nbabs = nbabs;
  ctx->nbnet = nbnet;
  ctx->nbmsi = nbmsi;
  ctx->nbflx = nbflx;
  ctx->nbsyf = nbsyf;
  ctx->nbsya = nbsya;
  ctx->nbsyf2 = nbsyf2;
  ctx->nbsya2 = nbsya2;
  ctx->nbres = nbres;
 
  ctx->results = (struct results*)mem_alloc(sizeof(*results)*nthreads);
  results = ctx->results;
  
  for (ith=0;ith<nthreads;ith++) {
    init_counter(ctx, &results[ith].net, nbnet);
    init_counter(ctx, &results[ith].abs, nbabs);
    init_counter(ctx, &results[ith].fup, nbflx);
    init_counter(ctx, &results[ith].fdn, nbflx);
    if (ctx->direct_diffuse) {
      init_counter(ctx, &results[ith].dir, nbflx);
      init_counter(ctx, &results[ith].difdn, nbflx);
      init_counter(ctx, &results[ith].difup, nbflx);
    }
    if (ctx->do_msi) {
      init_counter(ctx, &results[ith].msi, nbmsi);
    }
    if (maxsy) {
      init_counter(ctx, &results[ith].fup_alb, nbsyf);
      init_counter(ctx, &results[ith].fdn_alb, nbsyf);
      init_counter(ctx, &results[ith].abs_alb, nbsya);
      init_counter(ctx, &results[ith].fup_alb_cov, nbsyf2);
      init_counter(ctx, &results[ith].fdn_alb_cov, nbsyf2);
      init_counter(ctx, &results[ith].abs_alb_cov, nbsya2);
    }
    init_counter(ctx, &results[ith].path_len, 0);
    init_counter(ctx, &results[ith].albedo, 0);
    init_counter(ctx, &results[ith].transm, 0);
    init_counter(ctx, &results[ith].path_time, 0);
    init_counter(ctx, &results[ith].real_time, 0);
    init_counter(ctx, &results[ith].failures, 0);
  }

  res = ssp_rng_proxy_create
    (&ctx->allocator, &ssp_rng_mt19937_64, nthreads, &rng_proxy);
  if(res != RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: could not create the proxy RNG\n", FUNC_NAME);
    goto error;
  }
  rngs = MEM_CALLOC(&ctx->allocator, nthreads, sizeof(*rngs));
  if(!rngs) {
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: could not allocate the array of per thread RNG\n", FUNC_NAME);
    res = RES_MEM_ERR;
    goto error;
  }

  FOR_EACH(ith, 0, nthreads) {
    res = ssp_rng_proxy_create_rng(rng_proxy, ith, &rngs[ith]);
    if(res != RES_OK) {
      logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not create the RNG of the thread %lu\n",
        FUNC_NAME, (unsigned long)ith);
      goto error;
    }
  }

  npaths = (int64_t)nbpaths;
  omp_set_num_threads((int)nthreads);
  #pragma omp parallel for schedule(static)
  for(ipath=0; ipath < npaths; ++ipath) {
    const int ithread = omp_get_thread_num();
    compute_path(ctx, rngs[ithread], ithread, (size_t)ipath, results[ithread]);
  }

  m_res = &results[0];
  #define REDUCE(Var) {                                       \
    FOR_EACH(i, 0, m_res->Var->size) {                      \
      FOR_EACH(ith, 1, nthreads) {                            \
        m_res->Var->w.t[i] += results[ith].Var->w.t[i];   \
        m_res->Var->w2.t[i] += results[ith].Var->w2.t[i]; \
      }                                                       \
    }                                                         \
  } (void)0

  REDUCE(net);
  REDUCE(abs);
  REDUCE(fdn);
  REDUCE(fup);
  if (ctx->direct_diffuse) {
    REDUCE(dir);
    REDUCE(difdn);
    REDUCE(difup);
  }
  if (ctx->do_msi) {
    REDUCE(msi);
  }
  if (maxsy) {
    REDUCE(fup_alb);
    REDUCE(fdn_alb);
    REDUCE(abs_alb);
    REDUCE(fup_alb_cov);
    REDUCE(fdn_alb_cov);
    REDUCE(abs_alb_cov);
  }
  #undef REDUCE

  #define REDUCE(Var) {                                \
    FOR_EACH(ith, 1, nthreads) {                       \
      m_res->Var->w.s += results[ith].Var->w.s;    \
      m_res->Var->w2.s += results[ith].Var->w2.s;  \
    }                                                  \
  } (void)0

  REDUCE(path_len);
  REDUCE(albedo);
  REDUCE(transm);
  REDUCE(path_time);
  REDUCE(real_time);
  REDUCE(failures);
  #undef REDUCE

  compute_mean_std(m_res->net, d_npaths); 
  compute_mean_std(m_res->abs, d_npaths); 
  compute_mean_std(m_res->fdn, d_npaths); 
  compute_mean_std(m_res->fup, d_npaths); 
  if (ctx->direct_diffuse) {
    compute_mean_std(m_res->dir, d_npaths);
    compute_mean_std(m_res->difdn, d_npaths);
    compute_mean_std(m_res->difup, d_npaths);
  }
  if (ctx->do_msi) {
    compute_mean_std(m_res->msi, d_npaths);
  }
  if (maxsy) {
    compute_mean_std(m_res->fup_alb, d_npaths);
    compute_mean_std(m_res->fdn_alb, d_npaths);
    compute_mean_std(m_res->abs_alb, d_npaths);
    compute_mean_std(m_res->fup_alb_cov, d_npaths);
    compute_mean_std(m_res->fdn_alb_cov, d_npaths);
    compute_mean_std(m_res->abs_alb_cov, d_npaths);
  }
  compute_mean_std(m_res->path_len, d_npaths);
  compute_mean_std(m_res->albedo, d_npaths);
  compute_mean_std(m_res->transm, d_npaths);
  compute_mean_std(m_res->path_time, d_npaths);
  compute_mean_std(m_res->real_time, d_npaths);

exit:
  if(rng_proxy) ssp_rng_proxy_ref_put(rng_proxy);
  if(rngs!=NULL) {
    for (ith=0; ith<nthreads;ith++) {
      if(rngs[ith]!=NULL) {
        ssp_rng_ref_put(rngs[ith]);
      }
    }
    MEM_RM(&ctx->allocator, rngs);
  }
  return res;
error:
  goto exit;
}

int
main(int argc, char ** argv) {
  res_T res = RES_OK;
  time_t t1, t2 ;
  struct config conf ;
  double tolerance = 0.001;
  int err=0 ;
  struct context ctx ;
  struct s3d_device * s3d ;

  is_init(&ctx);

  res = mem_init_regular_allocator(&(ctx.allocator));
  if(res != RES_OK) {
    fprintf(stderr, "%s: could not initialise the memory allocator\n", FUNC_NAME);
    goto error;
  }
  ctx.is_init_allocator=1;

  res = logger_init(&(ctx.allocator), &(ctx.logger));
  if(res != RES_OK) {
    fprintf(stderr, "%s: could not initialise the logger\n", FUNC_NAME);
    goto error;
  }
  ctx.is_init_logger=1;
  logger_set_stream(&ctx.logger, LOG_OUTPUT, print_out, NULL);
  logger_set_stream(&ctx.logger, LOG_ERROR, print_err, NULL);
  logger_set_stream(&ctx.logger, LOG_WARNING, print_warn, NULL);

  logger_print(&ctx.logger,LOG_OUTPUT,
  "\n*----------------------------------------------------------------------------------------------------*\
   \n*--------------- SOLVING ATMOSPHERIC RADIATIVE TRANSFER WITH PATH TRACING MONTE CARLO ---------------*\
   \n*----------------------------------------------------------------------------------------------------*\n");

  /* read commands */
  logger_print(&ctx.logger, LOG_OUTPUT, "Reading commands...\n");
  time(&t1);
  res = read_commands(argc, argv, &conf, &ctx),
  time(&t2);
  ctx.conf=&conf;
  ctx.is_init_conf=1;
  if (res!=RES_OK) {
    if (res==RES_WARNING) {
      logger_print(&ctx.logger,LOG_WARNING,
      "%s: no simulation will be performed\n", FUNC_NAME);
    } else {
      logger_print(&ctx.logger,LOG_ERROR,
      "%s: error while reading commands\n", FUNC_NAME);
    }
    goto error;
  }
  logger_print(&ctx.logger, LOG_OUTPUT, "... OK (%f s)\n", difftime(t2,t1));

  /* initialize data */
  logger_print(&ctx.logger, LOG_OUTPUT, "Initializing context...\n");
  time(&t1);
  res = init_context(&ctx, &s3d, &conf); /* including les, mie, atm ... data */
  time(&t2);
  ctx.is_init_ctx = 1;
  if (res!=RES_OK) {
    logger_print(&ctx.logger,LOG_ERROR, 
        "%s: error during initialization of context\n", FUNC_NAME);
    goto error;
  }
  logger_print(&ctx.logger, LOG_OUTPUT, "... OK (%f s)\n", difftime(t2,t1));

  /* compute MC estimators */
  logger_print(&ctx.logger, LOG_OUTPUT, "Solving...\n");
  time(&t1);
  res = integrate(&ctx, conf.nbpaths, tolerance);
  time(&t2);
  ctx.is_solved=1;
  if (res!=RES_OK) {
    logger_print(&ctx.logger,LOG_ERROR,
	  "%s: error during MC integration\n",FUNC_NAME) ;
    goto error;
  }
  logger_print(&ctx.logger, LOG_OUTPUT, "... OK (%f s)\n", difftime(t2,t1));

  /* write results to file */
  logger_print(&ctx.logger, LOG_OUTPUT, "Writing results...\n");
  time(&t1);
  res = write_res(&ctx, ctx.results[0], conf.nbpaths, tolerance, conf.write_file);
  time(&t2);
  if (res!=RES_OK) {
    logger_print(&ctx.logger,LOG_ERROR,
	  "%s: error while writing results\n",FUNC_NAME);
    goto error;
  }
  logger_print(&ctx.logger, LOG_OUTPUT, "... OK (%f s)\n", difftime(t2,t1));

exit :
  logger_print(&ctx.logger, LOG_OUTPUT, "Cleaning memory...\n");
  time(&t1);
  res = clean_context(&ctx,s3d);
  time(&t2);
  if (res!=RES_OK) {
    logger_print(&ctx.logger,LOG_ERROR,
	  "%s: error while trying to clean memory\n",FUNC_NAME);
  }
  logger_release(&ctx.logger);
  if(ctx.is_init_allocator>0) {
    const size_t memsz = MEM_ALLOCATED_SIZE(&ctx.allocator);
    if(memsz!=0) { /* Check memory leaks */
      fprintf(stderr, "%s: allocator memory leaks: %lu Bytes\n",
        FUNC_NAME, (unsigned long)memsz);
    }
    mem_shutdown_regular_allocator(&ctx.allocator);
  }
  const size_t memsz = mem_allocated_size() ;
  if (memsz!=0) {
      fprintf(stderr, "%s: classical memory leaks: %lu Bytes\n",
        FUNC_NAME, (unsigned long)memsz);
  }
  printf("> ... OK (%f s)\n", difftime(t2,t1));
  printf("\n*------------------------------------------------ BYE -----------------------------------------------*\n");
  return err ;
error :
    if (res!=RES_OK) err = 1 ;
    goto exit ;
}

