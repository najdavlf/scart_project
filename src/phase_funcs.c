#include <rsys/double3.h>
#include <star/ssp.h>
#include "scart_context.h"
#include "utils.h"
#include "phase_funcs.h"
#include <omp.h>

/* "Two stream phase function"
 * with probability (g+1)/2,     sample = forward , cos theta = 1 
 * with probability 1 - (g+1)/2, sample = backward, cos theta = -1
 * ça fonctionne si le flux est déjà diffus mais pour la première diffusion 
 * (passer de direct à diffus) il faut avant d'appeler l'échantillonnage de 
 * la fonction de phase mettre à la main incoming à 57 ou -57 degrés en fonction 
 * de si la direction incidente avant cette premiere diffusion est vers le 
 * bas ou vers le haut : je ferai ça à l'extérieur des fonctions de phase, 
 * au moment de traiter les "true collisions" dans handle_volume... 
 */

double
ssp_ran_two_stream_local_pdf(const double g, const double sample[3])
{
  double epsilon_g, epsilon_mu;
  ASSERT(sample && d3_is_normalized(sample));
  /* probability of sample */
  if (sample[2] > 0) { /* forward */
    return (1+g)/2. ;
  }
  else { /* backward */
    return (1-g)/2. ;
  }
}

/* Henyey-Greenstein sampling of an unit sphere for an incident direction 
 * that is implicitly the Z axis. 
 * The PDF of the generated sample is stored in sample[3] */
static INLINE double*
ssp_ran_two_stream_local(struct ssp_rng* rng, const double g, double sample[4])
{
  double phi, R, cos_theta, sin_theta;
  ASSERT(rng && sample);
  phi = 2 * PI * ssp_rng_canonical(rng);
  R = ssp_rng_canonical(rng);
  /* cos theta = 1  avec proba (1+g)/2 (vers l'avant)
   * cos theta = -1 avec proba (1-g)/2 (vers l'arriere)
   */
  if ( R < (1+g)/2.) {
    cos_theta = 1 ;
  } 
  else {
    cos_theta = -1 ;
  }
  sin_theta = cos2sin(cos_theta);
  sample[0] = (cos(phi) * sin_theta);
  sample[1] = (sin(phi) * sin_theta);
  sample[2] = cos_theta;
  sample[3] = ssp_ran_two_stream_local_pdf(g,sample);
  return sample;
}

/* Return the probability distribution for a Henyey-Greenstein random
 * variate with an explicit incident direction that is `up' */
double
ssp_ran_two_stream_pdf(struct ssp_rng* rng,
                       const double up[3],
                       const double sample[3],
                       const void*context)
{
  const int ith = omp_get_thread_num();
  struct cop_data * ctx = (struct cop_data*)context;
  const double g = ctx->g_nu[ith];
  double epsilon_g, epsilon_mu;
  ASSERT(up && sample && d3_is_normalized(up) && d3_is_normalized(sample));
  if (d3_dot(sample,up) > 0) { /* forward */
    return (1+g)/2. ;
  }
  else { /* backward */
    return (1-g)/2. ;
  }
}


double * 
ssp_ran_two_stream(struct ssp_rng* rng,
                   const double up[3],
                   double sample[4],
                   const void*context)
{
  double sample_local[4];
  double basis[9];
  const int ith = omp_get_thread_num();
  struct cop_data * ctx = (struct cop_data*)context;
  const double g = ctx->g_nu[ith];
 
  ASSERT(rng && up && sample && d3_is_normalized(up));
  ssp_ran_two_stream_local(rng, g, sample_local);
  sample[3]=sample_local[3];
  return d33_muld3(sample, d33_basis(basis, up), sample_local);
}

/* HG local */
double
ssp_ran_sphere_myhg_local_pdf(const double g,
                              const double sample[3])
{
  double epsilon_g, epsilon_mu;
  /*printf("sample %g %g %g %g\n",sample[0],sample[1],sample[2],sample[3]);*/
  ASSERT(sample && d3_is_normalized(sample));
  if (g>0) {
    epsilon_g = 1-g;
    epsilon_mu = 1-sample[2];
  } else {
    epsilon_g = 1+g;
    epsilon_mu = 1+sample[2];
  }
  return (1./(4.*PI)
     *epsilon_g*(2-epsilon_g)
     *pow(epsilon_g*epsilon_g+2*epsilon_mu*(1-epsilon_g),-1.5));
}

/* Henyey-Greenstein sampling of an unit sphere for an incident direction 
 * that is implicitly the Z axis. 
 * The PDF of the generated sample is stored in sample[3] */
static INLINE double*
ssp_ran_sphere_myhg_local(struct ssp_rng* rng,
                          const double g,
                          double sample[4])
{
  double phi, R, cos_theta, sin_theta;
  ASSERT(rng && sample);
  phi = 2 * PI * ssp_rng_canonical(rng);
  R = ssp_rng_canonical(rng);
  cos_theta = 2*R*(1+g)*(1+g)*(g*(R-1)+1)
    /((1-g*(1-2*R))*(1-g*(1-2*R)))-1;
  sin_theta = cos2sin(cos_theta);
  sample[0] = (cos(phi) * sin_theta);
  sample[1] = (sin(phi) * sin_theta);
  sample[2] = cos_theta;
  sample[3] = ssp_ran_sphere_myhg_local_pdf(g,sample);
  return sample;
}

/* Return the probability distribution for a Henyey-Greenstein random
 * variate with an explicit incident direction that is `up' */
double
ssp_ran_sphere_myhg_pdf(struct ssp_rng* rng,
                        const double up[3],
                        const double sample[3],
                        const void*context)
{
  const int ith = omp_get_thread_num();
  struct cop_data * ctx = (struct cop_data*)context;
  const double g = ctx->g_nu[ith];
  double epsilon_g, epsilon_mu;
  ASSERT(up && sample && d3_is_normalized(up) && d3_is_normalized(sample));
  if (g>0) {
    epsilon_g = 1-g;
    epsilon_mu = 1-d3_dot(sample,up);
  } else {
    epsilon_g = 1+g;
    epsilon_mu = 1+d3_dot(sample,up);
  }
  return (1./(4.*PI)
     *epsilon_g*(2-epsilon_g)
     *pow(epsilon_g*epsilon_g+2*epsilon_mu*(1-epsilon_g),-1.5));
}


double * 
ssp_ran_sphere_myhg(struct ssp_rng* rng,
                    const double up[3],
                    double sample[4],
                    const void*context)
{
  const int ith = omp_get_thread_num();
  double sample_local[4];
  double basis[9];
  struct cop_data * ctx = (struct cop_data*)context;
  const double g = ctx->g_nu[ith];
 
  ASSERT(rng && up && sample && d3_is_normalized(up));
  ssp_ran_sphere_myhg_local(rng, g, sample_local);
  sample[3]=sample_local[3];
  return d33_muld3(sample, d33_basis(basis, up), sample_local);
}


/* Return the probability distribution for a Mie random
 * variate with an implicit incident direction in Z */
double
ssp_ran_sphere_mie_local_pdf(struct ssp_rng* rng,
                             const double sample[3],
                             void*context)
{
  struct cop_data * ctx = (struct cop_data*)context ;
  double mu ;
  ASSERT(sample && d3_is_normalized(sample));
  mu = sample[2] ;
  int i = find_index_rev(ctx->mu_tab, ctx->nbmu, mu) ;
  return rand_interp_lin(rng, ctx->mu_tab[i], ctx->mu_tab[i+1],
		  ctx->pdf_nu[omp_get_thread_num()][i], ctx->pdf_nu[omp_get_thread_num()][i+1], mu) ;
}

/* Mie sampling of an unit sphere for an incident direction 
 * that is implicitly the Z axis. 
 * The PDF of the generated sample is stored in sample[3] */
double*
ssp_ran_sphere_mie_local(struct ssp_rng* rng,
                         double sample[4],
                         void*context)
{
  ASSERT(rng && sample);
  struct cop_data * ctx = (struct cop_data*)context ;
  double phi = 2. * PI * ssp_rng_canonical(rng);
  double R = ssp_rng_canonical(rng);           /* cumulative pdf */
  int i = (int)(R*((double)ctx->nbcp-1)) ;     /* index of R in cp_tab */
  int ith = omp_get_thread_num();
  /* cos_theta = icp(R)  */
  
  double cos_theta = rand_interp_lin(rng, ctx->cp_tab[i], ctx->cp_tab[i+1],
	                                   ctx->icp_nu[ith][i], ctx->icp_nu[ith][i+1],
                                     R);
  double sin_theta = cos2sin(cos_theta);
  sample[0] = (cos(phi) * sin_theta);
  sample[1] = (sin(phi) * sin_theta);
  sample[2] = cos_theta;
  if ((!d3_is_normalized(sample)) && (fabs(d3_len(sample) - 1) <= 1e-6)) {
    sample[0] = sample[0]/d3_len(sample);
    sample[1] = sample[1]/d3_len(sample);
    sample[2] = sample[2]/d3_len(sample);
  } else if (!d3_is_normalized(sample)) {
    printf("sample not normalized ! nbcp %lu, R %g, i %i, len %g icp [i] = %g,\
           icp [i+1] %g, costheta = %g\n", 
           ctx->nbcp, R, i, d3_len(sample), ctx->icp_nu[ith][i], 
           ctx->icp_nu[ith][i+1], cos_theta);
  }
  sample[3] = ssp_ran_sphere_mie_local_pdf(rng, sample,ctx);
  return sample;
}

/* Return the probability distribution for a Mie random
 * variate with an explicit incident direction that is `up' */
double
ssp_ran_sphere_mie_pdf(struct ssp_rng* rng, 
                       const double up[3], 
                       const double sample[3],
                       const void*context)
{
  struct cop_data * ctx = (struct cop_data*)context ;
  double mu ;
  int i, ith=omp_get_thread_num();
  ASSERT(up && sample && d3_is_normalized(up) && d3_is_normalized(sample));
  mu = d3_dot(sample,up);
  i = find_index_rev(ctx->mu_tab, ctx->nbmu, mu);
  return rand_interp_lin(rng, ctx->mu_tab[i], ctx->mu_tab[i+1],
		                     ctx->pdf_nu[ith][i], ctx->pdf_nu[ith][i+1], 
                         mu);
}

/* Mie sampling of an unit sphere for an incident direction
 * that is `up'. 
 * The PDF of the generated sample is stored in sample[3] */
double *
ssp_ran_sphere_mie(struct ssp_rng* rng, 
                   const double up[3],
                   double sample[4], 
                   const void*context)
{
  struct cop_data * ctx = (struct cop_data*)context ;
  double sample_local[4];
  double basis[9];
  ASSERT(rng && up && sample && d3_is_normalized(up));
  ssp_ran_sphere_mie_local(rng, sample_local,ctx);
  sample[3]=sample_local[3];
  return d33_muld3(sample, d33_basis(basis, up), sample_local);
}


/* Return the probability distribution for a Rayleigh random
 * variate with an implicit incident direction in Z */
double
ssp_ran_sphere_ray_local_pdf(const double sample[3])
{
  double mu ;
  ASSERT(sample && d3_is_normalized(sample));
  mu = sample[2] ;  
  return (3./8.)*(1.+mu*mu);
}

/* Rayleigh sampling of an unit sphere for an incident direction 
 * that is implicitly the Z axis. 
 * The PDF of the generated sample is stored in sample[3] */
double*
ssp_ran_sphere_ray_local(struct ssp_rng* rng,
                         double sample[4])
{
  ASSERT(rng && sample);
  double phi = 2 * PI * ssp_rng_canonical(rng);
  double R = ssp_rng_canonical(rng);
  double A = pow(16.*R*R - 16.*R + 5, 1./2.) ;
  double B = -4.*R + 2.;
  double cos_theta = pow(A+B, -1./3.) - pow(A+B, 1./3.);
  double sin_theta = cos2sin(cos_theta);
  sample[0] = (cos(phi) * sin_theta);
  sample[1] = (sin(phi) * sin_theta);
  sample[2] = cos_theta;
  sample[3] = ssp_ran_sphere_ray_local_pdf(sample);
  return sample;
}

/* Return the probability distribution for a Rayleigh random
 * variate with an explicit incident direction that is `up' */
double
ssp_ran_sphere_ray_pdf(struct ssp_rng* rng,
                       const double up[3], 
                       const double sample[3],
                       const void*context)
{
  double mu;
  ASSERT(up && sample && d3_is_normalized(up) && d3_is_normalized(sample));
  mu = d3_dot(sample,up);
  return (3./8.)*(1.+mu*mu);
}

double* 
ssp_ran_sphere_ray(struct ssp_rng* rng,
                   const double up[3],
                   double sample[4],
                   const void*context)
{
  double sample_local[4];
  double basis[9];
  ASSERT(rng && up && sample && d3_is_normalized(up));
  ssp_ran_sphere_ray_local(rng, sample_local);
  sample[3]=sample_local[3];
  return d33_muld3(sample, d33_basis(basis, up), sample_local);
}
