# Star Cloudy Atmosphere Radiative Transfer
Copyright (C) 2017-2023 CNRS, Université Paul Sabatier
Contact najda.villefranque@gmail.com

## Description
This code solves 3D radiative transfer in cloudy atmospheres on Earth with Monte Carlo techniques. 

## Install

This program relies on the [CMake](http://www.cmake.org) and several packages
from the [star-engine](https://gitlab.com/meso-star/star-engine/) environment
to build.  It also depends on the
[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) C libraries.

First ensure that CMake and the NetCDF libs are installed on your system. Then
install the star-engine version 0.8.1:

```
~ $ git clone https://gitlab.com/meso-star/star-engine.git
~ $ cd star-engine && git checkout 0.8.1
~/star-engine $ mkdir build && cd build 
~/star-engine/build $ cmake ../cmake/
~/star-engine/build $ make
```

Finally generate the project from the `cmake/CMakeLists.txt` file by appending
to the `CMAKE_PREFIX_PATH` variable the install directories of its
dependencies.

```
~/scart_project $ mkdir build && cd build 
~/scart_project/build $ cmake ../cmake/ \
   -DCMAKE_PREFIX_PATH=~/star-engine/local/
~/scart_project/build $ make
```

The resulting project can be edited, built, tested and installed as any CMake
project. Refer to the [CMake](https://cmake.org/documentation) for further
informations on CMake.

## Usage

Make sure the location of the star-engine libraries and scart program are
registered against the current shell (e.g. by setting LD_LIBRARY_PATH and
PATH). Then run 

```
~ $ scart config.nam
```

`config.nam` is a user configuration ascii file in the form of:

```
key=value
```

## Help
All optional arguments available and their default values can be consulted with 

```
~ $ scart -h
```

```
*----------------------------------------------------------------------------------------------------*   
*--------------- SOLVING ATMOSPHERIC RADIATIVE TRANSFER WITH PATH TRACING MONTE CARLO ---------------*   
*----------------------------------------------------------------------------------------------------*
> Reading commands...
> Usage: ./build/scart -h|config.nam

> Optional key arguments in config.nam (description) and default values : 
	 nbpaths                    (number of trajectories to simulate)                                    100
	 nbly                       (number of vertical points where fluxes will be measured)               2
	 nbx                        (number of longitudinal points for fluxes computation                   1
	 nby                        (number of latitudinal points for fluxes computation                    1
	 liq_op_file                (name of netCDF data file for Mie look up tables (liquid))              (null)
	 ice_op_file                (name of netCDF data file for Mie look up tables (ice))                 (null)
	 atm_file                   (name of netCDF data file for atmospheric profiles)                     (null)
	 les_file                   (name of netCDF data file for 3D atmospheric fields)                    (null)
	 sun_file                   (name of netCDF data file for sun position and DNI)                     (null)
	 write                      (name of netCDF write file for results)                                 (null)
	 cloud                      (effect of liquid clouds)                                               1
	 atmosphere                 (effect of atmospheric molecules)                                       0
	 grd_alb                    (ground albedo)                                                         0
	 liq_g_assym                (assymetry parameter for HG phase function (liquid))                    0.86
	 liq_ssa                    (single scattering albedo (liquid))                                     1
	 ice_g_assym                (assymetry parameter for HG phase function (ice))                       0.86
	 ice_ssa                    (single scattering albedo (ice))                                        1
	 theta                      (solar zenith angle, in degrees)                                        0
	 phi                        (solar azimuth angle, in degrees)                                       0
	 latitude                   (latitude in degrees if sun_file is provided)                           -999
	 day                        (day in number if sun_file is provided)                                 -1
	 hour                       (hour in number if sun_file is provided)                                -1
	 cut_nose                   (opening angle in degrees that will be cut off the pf)                  0
	 monochromatic              (monochromatic wavelength in nm)                                        800
	 print                      (print mode (0: minimum, 1: debug, 2: path, 3: angle at boa, 
                                        4: horizontal distances, 5: directions before scattering))    0
	 use_mie_pf                 (use Mie phase function (0: use Henyey Greenstein))                     1
	 do_two_stream              (perform two-stream approximation (up / dn propagation)                 0
	 do_direct_diffuse          (compute distinct direct and diffuse fluxes)                            0
	 do_delta_scaling           (use delta-Eddington approximation)                                     0
	 do_russian_reflection      (use russian roulette to reflect path at the ground)                    0
	 do_spectral_integration    (integrate over spectrum if atm_file is provided)                       0
	 do_write_spectral          (write spectral fluxes if atm_file is provided)                         0
	 do_time_integration        (integrate over time if sun_file is provided)                           0
	 do_latitudinal_integration (integrate over latitudes if sun_file is provided)                      0
	 do_direct_in_solid_angle   (direct is defined as in a given solid angle around sun dir)            0
	 do_remove_periodicity      (do not repeat cloud domain)                                            0
	 do_4VS                     (remove abs, set lambertian illumination)                               0
	 do_msi                     (compute nadir radiances at TOA)                                        0
	 theta_diffus_down          (angle of downward diffusion for 2-stream approximation, in degrees)    122.482
	 theta_diffus_up            (angle of upward diffusion for 2-stream approximation, in degrees)      57.5184
	 direct_solid_angle         (solid angle around sun dir, in degrees, if do_direct_in_solid_angle)   0
	 zmin                       (minimum height in kilometers to measure fluxes)                        -1
	 zmax                       (maximum height in kilometers to measure fluxes)                        -1
	 ica                        (number of columns n for independent subdomain mode 
                                     (subdomain = nxn columns))                                       0
	 map_ica                    (0: integrate fluxes horizontally on nbx x nby pixels 
                                     1: integrate fluxes horizontally on ica domains)                 0
	 z_reg                      (vertical grid is regular)                                              1
	 max_reflections_symb       (accounted for in symbolic MC)                                          0
warning: main: no simulation will be performed
> Cleaning memory...
> ... OK (0.000000 s)

*------------------------------------------------ BYE -----------------------------------------------*
```
