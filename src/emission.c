#include "main.h"
#include "scart_context.h"
#include "utils.h"
#include "const.h"
#include <omp.h>
#include <star/ssp.h>
#include <star/s3d.h>

res_T 
sample_nu(struct ssp_rng * rng, struct context * ctx, struct photon *p) {
  res_T res = RES_OK;
  double eps;
  int inu, i=0;

  if (ctx->spectral_integration!=0) {
    if (ctx->atm_dat.atm_file!=NULL) {
      /* Sample narrow band from incoming solar flux at TOA */
      eps = ssp_rng_canonical(rng) ; 
      /* FIND INDEX_AFTER_VALUE */
      while (eps > ctx->atm_dat.ncISF[i]) { 
        i++ ;
      }
      /* force sampling one band */
      /*i=0; */
      p->ibd = i ;

      /* sample nu uniformly in the narrow band (to compute rayleigh & mie properties) */
      p->nu = ctx->atm_dat.nu_tab[i]+(ctx->atm_dat.nu_tab[i+1]-ctx->atm_dat.nu_tab[i])*ssp_rng_canonical(rng) ;
    }
    else {
      /* if spectral integration is required, then atmospherical data is needed */
      res = RES_IO_ERR ;
    }
  } else {
    p->nu = ctx->mono_nu ;
    if (ctx->atm_dat.atm_file!=NULL) {
      /* find to which narrow band the monochromatic nu belongs */
      /*printf("emit with atm\n");*/
      p->ibd = find_index(ctx->atm_dat.nu_tab, ctx->atm_dat.nbnu, p->nu) ;
    } else {
      p->ibd = 0 ;
    }
  }
  if (ctx->liq_op_dat.cop_file!=NULL) {
      /* find nu index in mie nu tab */
      inu = find_index(ctx->liq_op_dat.nu_tab, ctx->liq_op_dat.nbnu, p->nu);
      p->liq_inu0 = inu;
      if (inu+1 < ctx->liq_op_dat.nbnu) {
        p->liq_inu1 = inu+1;
      } else { /* beyond last point in wvl tab : use last point data */
        p->liq_inu1 = inu; 
      }
  } else {
      p->liq_inu0 = 0;
      p->liq_inu1 = 0;
  }
  if (ctx->ice_op_dat.cop_file!=NULL) {
      /* find nu index in mie nu tab */
      inu = find_index(ctx->ice_op_dat.nu_tab, ctx->ice_op_dat.nbnu, p->nu);
      p->ice_inu0 = inu;
      if (inu+1 < ctx->ice_op_dat.nbnu) {
        p->ice_inu1 = inu+1;
      } else {
        p->ice_inu1 = inu;
      }
  } else {
      p->ice_inu0 = 0;
      p->ice_inu1 = 0;
  }
  return res ;
}

res_T 
sample_gp(struct ssp_rng * rng, struct context * ctx, struct photon *p) {
  res_T res = RES_OK;
  double eps ;
  double cumul=0.;
  int i=0;
  if (ctx->atm_dat.atm_file!=NULL) {
    eps = ssp_rng_canonical(rng); 
    while (eps > cumul && (i+1<ctx->atm_dat.nbgp)) { 
      if (0) printf("i %i eps %f cumul %f\n", i, eps, cumul);
      cumul+=ctx->atm_dat.wg[p->ibd*ctx->atm_dat.nbgp + i] ;
      i++ ;
    }
    i--;

    if (i>=ctx->atm_dat.nbgp || i>=ctx->atm_dat.gp_nb[p->ibd]) {
      if (0) 
        printf("i %i bad op ??? nbgp %i eps %f cumul %f \n",
            i, ctx->atm_dat.gp_nb[p->ibd], eps, cumul);
      res=RES_BAD_OP;
    } else {
      p->igp = i ;
      if (0) printf("ok gp = %i\n", i);
    }
  }
  /*
    if ((p->igp == ctx->atm_dat.gp_nb[p->ibd]-1))
    printf("sampled last gp %i of band %i\n",p->igp, p->ibd);
  */
  return res ;
}

res_T
sample_entry(struct ssp_rng * rng, struct  context * ctx, double org[]) {
  res_T res = RES_OK;

  org[0] = ctx->les_dat.xmin + (ctx->les_dat.xmax-ctx->les_dat.xmin)*ssp_rng_canonical(rng) ;
  org[1] = ctx->les_dat.ymin + (ctx->les_dat.ymax-ctx->les_dat.ymin)*ssp_rng_canonical(rng) ;
  org[2] = ctx->ztoa*1.001; /* emitting higher than TOA to allow counter intersection */
  return res ;
}

res_T
set_direction(struct context * ctx, double dir[]){
  INIT_RES ;

  d3_set(dir, ctx->sun_dat.sun_dir[omp_get_thread_num()]);

  return res ;
}

res_T
set_weight(struct context * ctx, double *  w) {
  INIT_RES ;

  *w=ctx->sun_dat.w_dni[omp_get_thread_num()] ;
  return res ;
}

res_T
emission(struct ssp_rng * rng, struct context * ctx, struct ray * r) {
  res_T res = RES_OK;
  const double down[3] = {0., 0., -1.};
  double sample[3];

  res = sample_nu(rng,ctx,&(r->ph)); /* nm */
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
      "%s: could not sample wavelength\n", FUNC_NAME);
    goto error;
  }

  res = sample_gp(rng,ctx,&(r->ph)); 
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
      "%s: could not sample quadrature point\n", FUNC_NAME);
    goto error;
  }

  res = sample_entry(rng,ctx,r->org);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
      "%s: could not sample entry position\n", FUNC_NAME);
    goto error;
  }

  res = set_direction(ctx, r->dir);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
      "%s: could not set particle direction\n", FUNC_NAME);
    goto error;
  }
  if (ctx->do_4VS) {
    ssp_ran_hemisphere_cos(rng, down, sample, NULL);
    r->dir[0] = sample[0]; r->dir[1] = sample[1]; r->dir[2] = sample[2];
  }

  res = set_weight(ctx, &(r->ph.w));
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
      "%s: could not set particle weight\n", FUNC_NAME);
    goto error;
  }

exit : 
  return res ;
error : 
  goto exit ;
}
