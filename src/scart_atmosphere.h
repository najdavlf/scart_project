#ifndef ATMOSPHERE_H
#define ATMOSPHERE_H

/******* Exposed functions to handle atmospheric data *******/

/* what is in a structure "atmosphere_data" */
struct atm_data {
  char * atm_file ;  /* Name of netCDF data file for atmospheric profiles */
  size_t nblv ;      /* number of levels interfacing the layers, from ground to TOA */
  size_t nbly ;      /* number of layers where radiative properties are defined */
  size_t nbbd ;      /* number of narrow bands */
  size_t nbgp ;      /* max number of quadrature points per band */
  size_t nbnu ;      /* nb of wavelengths = number of narrow bands +1 */
  double zmin,zmax ; /* Vertical boundaries of atmospheric profile (km) */
  double *vertical_levels;    /* Levels interfacing layers */
  double *vertical_layers;    /* Center of layers where 1D profiles are defined (upper interface of layer) */
  double *z_tab ;    /* Array of levels where 1D profiles are defined (upper interface of layer) */
  double *nu_tab ;   /* Array of wavelengths (narrow bands bounds) */
  double totISF ;    /* Total Incoming Solar Flux at TOA */
  double *gp_nb;     /* number of g points per band */
  double *wg;        /* g points weights */
  double *ka;        /* gas absorption coefficient */
  double *ks;        /* rayleigh scattering coefficient */
  double *ssa;       /* gas single scattering albedo */
  double  *ncISF;    /* Normalized Cumulative Incoming Solar Flux */
  double **ka_g, **ks_g, **ssa_g ; /* PRIV once g point has been sampled */
} ;

/* fill the atmosphere_data structure */
res_T
init_atm_data(struct logger* logger, struct atm_data * ad, char * atm_file, double zmin, double zmax, size_t nth) ;

/* clean up memory */
res_T 
clean_atm(struct atm_data * ad);

/* access data */
double get_atm_ka(struct atm_data * ad, double height, int iband, int igpoint);
double get_atm_ks(struct atm_data * ad, double height, int iband, int igpoint);
double get_atm_ke(struct atm_data * ad, double height, int iband, int igpoint);

#endif
