#ifndef SUN_H
#define SUN_H

struct sun_data {
  char * sun_file ;       /* Name of netCDF file containing sun data  */ 
  int do_time_int, do_lat_int ;
  double **sun_dir ;       /* PRIV Sun cartesian direction */
  float *w_dni ;          /* PRIV DNI Weight of current path */
  size_t nblat, nbdate  ; /* Number of latitudes and dates */
  size_t nbhour, nbday;   /* Number of hours and days */
  double *sza, *azim ;     /* Sun zenith and azimuth angles */
                          /* Of dimensions nblat, nbdate */
  double *lat, *date ;     /* Latitude in degrees Nth and hourly dates in secondes */
                          /* Of dim nblat and nbdate */
  double *hour, *day ;     /* Hourly hours and daily days to select a date in date */
                          /* Of dim nbhours and nbdays */
  double *cumLatTable ;    /* Cumulative DNI as a function of latitude => sample a latitude */
                          /* Of dim nblat */
  double *cumLatDatTable ; /* Cumulative DNI as a function of date, and latitude => sample a date knowing lat */
                          /* Of dim nblat,nbdate */
  double *tab_dni ;        /* Depending on the method of integration, 
			     may be a scalar (totdni), a 1D latidunal year-average (latdni),
			     or a 2D local value (in date and latitude) (dni) */
} ;

res_T
sample_sun(struct ssp_rng * rng, 
           void * ctx);

res_T
init_sun_data(struct logger* logger,
              struct sun_data * sd,  
              char * sun_file, 
              double theta, 
              double phi, 
              int do_time_int, 
              int do_lat_int,
              size_t nthreads);

res_T 
clean_sun(struct sun_data * sd, size_t nth);

#endif
