#ifndef CONF_H
#define CONF_H

#include "const.h"
#include <string.h>

/* Struct config to read configuration file or command line arguments */
struct config  {
  char*write_file ;
  size_t nbpaths  ;
  size_t nbly,nbx,nby ;
  double zmin,zmax ;
  char*liq_op_file ;
  char*ice_op_file ;
  char*atm_file ;
  char*les_file ;
  char*sun_file ;
  int cloud, atmosphere ;
  double ground_albedo;
  double liq_g_assym;
  double liq_ssa ;
  double ice_g_assym;
  double ice_ssa ;
  double theta_sun, phi_sun ;
  double latitude, day, hour ;
  int z_reg ;
  int print ;
  int use_mie_pf ;
  int do_two_stream ;
  int do_direct_diffuse ;
  int do_delta_scaling ;
  int do_russian_reflection ;
  int do_spectral_integration ;
  int do_write_spectral;
  int do_time_integration ;
  int do_latitudinal_integration ;
  int do_direct_in_solid_angle ;
  int do_remove_periodicity ;
  int do_4VS;
  int do_msi;
  int do_ad; 
  double theta_diffus_down, theta_diffus_up;
  double direct_solid_angle ;
  double monochromatic ;
  double cut_nose ;
  double ad_coords[2][2];
  size_t ica ;
  size_t map_ica ;
  int max_reflections_symb;
} ;

#define CONF_INIT__ {                                                             \
        NULL,                  /*write file*/                                     \
        100 ,                  /*nbpaths*/                                        \
        2,1,1,                 /* nbly, nby, nbx */                               \
        -1.,-1.,               /* zmin zmax */                                    \
        NULL,NULL,NULL,NULL,NULL,  /* cop liq, cop ice, atm, LES files */         \
        1,0,              /* Active cloud, active atm */                          \
        0.,               /* ground alb */                                        \
        LIQ_G_AS,         /* assymetry factor */                                  \
        LIQ_SSA,          /* ssa */                                               \
        ICE_G_AS,         /* assymetry factor */                                  \
        ICE_SSA,          /* ssa */                                               \
        0,0,              /* theta sun, phi sun */                                \
        -999,-1,-1,       /* latitude, day, hour */                               \
        1,                /* is z regular ?*/                                     \
        0,                /* print mode */                                        \
        1,                /* Mie phase func */                                    \
        0,                /* two stream approx */                                 \
        0,0,0,            /* direct diffuse, delta scaling, russian reflection */ \
	      0,0,0,            /* spectral, time, lat integrations */                  \
	      0,                /* do write spectral fluxes */                          \
	      0,                /* do direct in solid angle */                          \
	      0,                /* do remove periodicity */                             \
	      0,                /* do 4VS                */                             \
	      0,                /* do msi                */                             \
	      0,                /* do ad                 */                             \
	      122.481636591,57.518363409,/* theta diffus down and up for 2-stream app */\
	      0.,               /* direct in solid angle */                             \
        CONST_MONO_NU,    /* monochromatic wavelength */                          \
        0.,               /* cut nose */                                          \
        { { -FLT_MAX, FLT_MAX}, {-FLT_MAX, FLT_MAX} }, /* average coordinates */  \
        0,0,              /* ica, ica map */                                      \
	      0                 /* max reflections accounted for in symbolic MC */      \
}

static const struct config CONF_INIT = CONF_INIT__;

#endif
