#include "main.h"
#include "const.h"
#include "scart_context.h"
#include "handle_volumic_collision.h"
#include <star/ssp.h>
#include <star/s3d.h>
#include <omp.h>

res_T
get_les_index(struct context * ctx, 
              const double x, const double y, const double z, 
              size_t* ix, size_t* iy, size_t*iz) 
{
  res_T res = RES_OK;
  int ith = omp_get_thread_num();
  size_t iztmp;

  if (ctx->les_dat.les_file!=NULL) {  /* LES netCDF file */
    *ix = get_x_ind(ctx,x,ctx->ica);
    *iy = get_y_ind(ctx,y,ctx->ica);
    *iz = get_z_ind(ctx,z);
    iztmp = *iz ;
    if (ctx->print==1) { 
      fprintf(ctx->debug[ith], "fetch les data at index %lu %lu %lu\n", *ix, *iy, *iz);
      fprintf(ctx->debug[ith], "height of index %i-1, %i and %i+1 : %f %f %f \n", 
          iztmp, iztmp, iztmp, ctx->les_dat.z_tab[iztmp-1], ctx->les_dat.z_tab[iztmp],
          ctx->les_dat.z_tab[iztmp+1]);
      /*
       * for (iztmp=0; iztmp<ctx->les_dat.nbz; iztmp++)
       * fprintf(ctx->debug[ith], "z_tab[%3i] = %10f\n", iztmp, ctx->les_dat.z_tab[iztmp]);
      */
    }
  } else {
    res = RES_BAD_ARG;
  }

  return res ;
}

res_T
get_volume_properties(struct ssp_rng* rng,
                      struct context * ctx,
                      struct event * ev,
                      struct ray *r,
                      struct collision * col) 
{
  res_T res = RES_OK;
  int index, inu;
  const double x=ev->location[0];
  const double y=ev->location[1];
  const double z=ev->location[2];
  struct cop_data * liq_dat = &(ctx->liq_op_dat);
  struct cop_data * ice_dat = &(ctx->ice_op_dat);
  struct atm_data * ad = &(ctx->atm_dat);
  struct les_data * ld = &(ctx->les_dat);
  double ql, qi;
  size_t ix, iy, iz;
  int ith = omp_get_thread_num();

  res = reset_collision(col, ctx->col_dat.nbcolliders);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not reset collision to zero\n", FUNC_NAME);
    goto error;
  }

  if ((z < ctx->zboa)) {
    /* if under the ground */
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: below ground with z %g, zboa %g, ztoa %g \n",
        FUNC_NAME, z, ctx->zboa, ctx->ztoa);
    goto error ;
  }

  if (z <= ctx->ztoa) {

    /* first add cloud optical properties if relevant */
    if ((z> ld->zmin) && (z < ld->zmax) && ctx->col_dat.cloud) {

      res = get_les_index(ctx, x, y, z, &ix, &iy, &iz);
      if (res != RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not get LES index\n",FUNC_NAME);
        goto error ;
      }

      ql = get_les_ql(ld, ix, iy, iz);
      inu = 0;
      if (ctx->liq_op_dat.cop_file != NULL) {
        /* randomly set to inu or inu+1 (= linear interpolation of cop) */
        inu = (int)rand_interp_lin(rng, 
            ctx->liq_op_dat.nu_tab[r->ph.liq_inu0], 
            ctx->liq_op_dat.nu_tab[r->ph.liq_inu1],
            (double)r->ph.liq_inu0, (double)r->ph.liq_inu1,
            r->ph.nu);
        ctx->liq_op_dat.pdf_nu[ith] = &ctx->liq_op_dat.pdf_tab[inu*ctx->liq_op_dat.nbmu];
        ctx->liq_op_dat.icp_nu[ith] = &ctx->liq_op_dat.icp_tab[inu*ctx->liq_op_dat.nbcp];
      }
      col->ks[DROPLET] = ql*get_cop_ss(liq_dat, ld->liq_reff, ld->liq_veff, inu);
      col->ka[DROPLET] = ql*get_cop_sa(liq_dat, ld->liq_reff, ld->liq_veff, inu);
      col->g[DROPLET] =  get_cop_g(liq_dat, ld->liq_reff, ld->liq_veff, inu);
      liq_dat->g_nu[ith] = col->g[DROPLET];
      r->ph.liq_inu = inu;
      /*printf("g %f se %f ssa %f\n", 
          col->g[DROPLET], 
          get_cop_se(liq_dat, ld->liq_reff, ld->liq_veff, inu),
          get_cop_ssa(liq_dat, ld->liq_reff, ld->liq_veff, inu));
      */

      qi = get_les_qi(ld, ix, iy, iz);
      inu = 0;
      if (ctx->ice_op_dat.cop_file != NULL) {
        /* randomly set to inu or inu+1 (= linear interpolation of cop) */
        inu = (int)rand_interp_lin(rng, 
            ctx->ice_op_dat.nu_tab[r->ph.ice_inu0],
            ctx->ice_op_dat.nu_tab[r->ph.ice_inu1],
            (double)r->ph.ice_inu0, (double)r->ph.ice_inu1,
            r->ph.nu);
        ctx->ice_op_dat.pdf_nu[ith] = &ctx->liq_op_dat.pdf_tab[inu*ctx->liq_op_dat.nbmu];
        ctx->ice_op_dat.icp_nu[ith] = &ctx->ice_op_dat.icp_tab[inu*ctx->ice_op_dat.nbcp];
      }
      col->ks[ICE] = qi*get_cop_ss(ice_dat, ld->ice_reff, ld->ice_veff, inu);
      col->ka[ICE] = qi*get_cop_sa(ice_dat, ld->ice_reff, ld->ice_veff, inu);
      col->g[ICE] = get_cop_g(ice_dat, ld->ice_reff, ld->ice_veff, inu);
      ice_dat->g_nu[ith] = col->g[ICE];
      r->ph.ice_inu = inu;

      if (ctx->print==1) {
        fprintf(ctx->debug[ith], 
            "liquid ql %f, ks %f, ka %f, pf g %f\n", 
            ql, col->ks[DROPLET], col->ka[DROPLET], col->g[DROPLET]);
        fprintf(ctx->debug[ith], 
            "ice qi %f, ks %f, ka %f, pf g %f\n", 
            qi, col->ks[ICE], col->ka[ICE], col->g[ICE]);
      }

      if (ctx->delta_scaling>0) {
	      res = delta_eddington(&(liq_dat->g_nu[ith]), &(col->ks[DROPLET]), ctx->delta_scaling);
	      res = delta_eddington(&(ice_dat->g_nu[ith]), &(col->ks[ICE]), ctx->delta_scaling);
        if (ctx->print==1) { 
          fprintf(ctx->debug[ith],
              "after delta scaling: liquid ks %g, pf g %g\n",
              col->ks[DROPLET], col->g[DROPLET]);
          fprintf(ctx->debug[ith],
              "after delta scaling: ice ks %g, pf g %g\n",
              col->ks[ICE], col->g[ICE]);
        }
      }
    }

    /* then add atmospheric optical properties if relevant */
    if (ctx->col_dat.atmosphere>0) {
      if (ad->atm_file!=NULL) { /* either from atmospheric data file */
        col->ks[2*ctx->col_dat.cloud] = get_atm_ks(ad, z, r->ph.ibd, r->ph.igp);
        col->ka[2*ctx->col_dat.cloud] = get_atm_ka(ad, z, r->ph.ibd, r->ph.igp);
        col->g[2*ctx->col_dat.cloud] = 0;
        if (ctx->print==1) { 
          fprintf(ctx->debug[ith],
              "gas ks %f, ka %f, pf g %f\n",
              col->ks[2*ctx->col_dat.cloud], col->ka[2*ctx->col_dat.cloud], col->g[2*ctx->col_dat.cloud]);
        }
      } else {
	      res = RES_BAD_ARG;
	      goto error;
      }
    }
  }

  if (ctx->do_4VS) {
    for (index=0;index<ctx->col_dat.nbcolliders;index++) {
      col->ka[index] = 0;
    }
  }

  for (index=0;index<ctx->col_dat.nbcolliders;index++) {
    col->ks_tot += col->ks[index];
    col->ka_tot += col->ka[index];
  }
  col->ke_tot = col->ks_tot + col->ka_tot ;

exit:
  return res ;
error:
  goto exit;
}

res_T
handle_true_collision(struct ssp_rng * rng,
                      struct context * ctx,
                      struct event * ev,
                      struct ray * r,
                      struct collision * col,
                      struct results * results) 
{
  res_T res = RES_OK;
  const size_t ispec = (size_t)(r->ph.ibd*ctx->write_spectral); /* 0 or ibd */
  const size_t nbabs = ctx->nbabs/(ctx->nbbds);
  const size_t nbsya = ctx->nbsya/(ctx->nbbds);
  int ith = omp_get_thread_num();
  int do_collision;
  size_t z_ind;
  int ind_sym;
  double r0;
  double ke;

  if (ctx->print==1) {
    fprintf(ctx->debug[ith],
        "total ka %g and ks %g in band %i and gpoint %i\n", 
        col->ka_tot, col->ks_tot,r->ph.ibd,r->ph.igp) ;
    fprintf(ctx->debug[ith],
        "save %g %g %g %i %i\n",
        ev->location[2], col->ka_tot, col->ks_tot,r->ph.ibd,r->ph.igp) ;
  }

  res = sample_collider(rng,ctx,col);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not sample collider\n",FUNC_NAME);
    goto error ;
  }

  res = handle_periodicity(ctx, col, r, ev, &do_collision);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not handle periodicity\n",FUNC_NAME);
    goto error ;
  }

  if (do_collision) {
    r0 = ssp_rng_canonical(rng);
    ke = col->ks[col->ind_collider]+col->ka[col->ind_collider];

    if (r0 < col->ks[col->ind_collider]/ke) { /* Scattering */
      res = scatter(rng,ctx,col,r,&(ev->has_scattered));
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not scatter\n",FUNC_NAME);
        goto error ;
      }

      if (ctx->print==1) {
        fprintf(ctx->debug[ith], 
            "the particule will be scattered by %i\n",
            col->ind_collider);
      }
    } else { /* Absorption */
      if (results != NULL) {
        /* in z; 0 is bottom 1st layer, 1 is bottom 2nd layer, .. nblv is
         * bottom space */
        z_ind = find_index(ctx->atm_dat.vertical_levels, ctx->atm_dat.nblv, 
                           ev->location[2]);
        ind_sym = (1+(int)z_ind)*(ctx->max_reflections_symb)+r->ph.is_reflected;
        /* in abs; 0 is below ground, 1 is first layer, .. nblv-1 is last
         * layer, nblv is space */
        ASSERT(z_ind<ctx->atm_dat.nblv);
        results->tmp_abs[1+z_ind]+=r->ph.w;
        if (r->ph.is_reflected < ctx->max_reflections_symb) {
          results->tmp_abs_alb[ind_sym] += r->ph.w;
          /*printf("abs symb %i %i %i %f\n", z_ind, r->ph.is_reflected, 
              (1+z_ind)*(ctx->max_reflections_symb)+r->ph.is_reflected, 
              results->abs_alb[(1+z_ind)*(ctx->max_reflections_symb)+r->ph.is_reflected]
           );
           */
        }
      }

      r->ph.w = 0. ;
      r->over=1 ;

      if (ctx->print==1) {
        fprintf(ctx->debug[ith],
            "the particule will be absorbed by %i\n",
            col->ind_collider) ;
      }
    }
  } else {
    (void)ke; (void)r0;
  }

exit:
  return res;
error :
  goto exit;
}

res_T
handle_volumic_collision(
    struct ssp_rng* rng,
    struct context* ctx,
    struct ray* r,
    struct event* ev,
    struct results* results)
{
  res_T res = RES_OK;
  int ith = omp_get_thread_num() ;
  struct collision col = ev->collision;

  res = get_volume_properties(rng, ctx, ev, r, &col);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not get volume properties\n",FUNC_NAME);
    goto error;
  }

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "with total ke %g\n", col.ks_tot+col.ka_tot);
  }

  if (col.ke_tot>ctx->ke_hat[ith]) {
    if (ctx->print==1)
    fprintf(ctx->debug[ith], 
        "OMG k %f > khat %f\nat %g %g %g with \n",
        col.ke_tot, ctx->ke_hat[ith],
        ev->location[0], ev->location[1], ev->location[2]);
    fprintf(ctx->debug[ith], 
        "k/khat = %f khat - k = %f \n",
        col.ke_tot/ctx->ke_hat[ith], 
        ctx->ke_hat[ith]-col.ke_tot); 
    fprintf(ctx->debug[ith], 
        "tot ka %f ks %f\nliquid ka %f ks %f\nice ka %f ks %f\ngas ka %f ks %f\n",
        col.ka_tot, col.ks_tot,
        col.ka[DROPLET], col.ks[DROPLET],
        col.ka[ICE], col.ks[ICE],
        col.ka[2], col.ks[2]);
    fprintf(ctx->debug[ith], "ray dir %f %f %f\n", SPLIT3(r->dir));
    fprintf(ctx->debug[ith], "ibd igp %i %i\n", r->ph.ibd, r->ph.igp);
    res=RES_BAD_OP;
    goto error ;
  } else {
    if (ssp_rng_canonical(rng) < col.ke_tot/ctx->ke_hat[ith]) { /* true collision */
      if (ctx->print==1) { 
        fprintf(ctx->debug[ith],
            "This is a true col with ke_hat %g\n",
            ctx->ke_hat[ith]) ;
      } else if (ctx->print==2) {
        fprintf(ctx->debug[ith],
            "%g\t%g\t%g\n",
            ev->location[0], ev->location[1], ev->location[2]) ;
        fflush(ctx->debug[ith]) ;
      }

      res = handle_true_collision(rng, ctx, ev, r, &col, results);
      if (res!=RES_OK){
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not handle true collision\n",FUNC_NAME);
        goto error;
      }
      if (ev->has_scattered) {
        ev->type_event = VOL_SCAT;
      } else {
        ev->type_event = VOL_ABS;
      }
    } else {
      if (ctx->print==1) { 
        fprintf(ctx->debug[ith],
            "This is a null col with ke_hat %g\n",
            ctx->ke_hat[ith]); 
      }
      ev->type_event = VOL_NULL;
    }
  }

  ev->filter = 0 ;

exit:
  if (ctx->print) fflush(ctx->debug[ith]);
  return res;
error:
  goto exit;
}
