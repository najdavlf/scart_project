#ifndef PATH_H
#define PATH_H

#include "const.h"
res_T 
reset_collision(struct collision *c, int nbcol) {
  res_T res = RES_OK;
  int index;
  for (index=0;index<nbcol;index++) {
    c->ks[index] = 0. ;
    c->ka[index] = 0. ;
    c->g[index] = 0. ;
  }
  c->ka_tot = 0. ;
  c->ks_tot = 0. ;
  c->ke_tot = 0. ;
  c->ind_collider=0 ;
exit:
  return res;
error:
  goto exit;
}

res_T
init_collision(struct collision * c, int nbcol) {
  res_T res = RES_OK;
  int index ;
  c->ka = (double*)mem_alloc(nbcol*sizeof(*(c->ka))) ;
  c->ks = (double*)mem_alloc(nbcol*sizeof(*(c->ks))) ;
  c->g = (double*)mem_alloc(nbcol*sizeof(*(c->g))) ;
  res = reset_collision(c, nbcol);
  return res ;
}

res_T
clean_collision(struct collision * c) {
  if (c->ka != NULL) mem_rm(c->ka);
  if (c->ks != NULL) mem_rm(c->ks);
  if (c->g  != NULL) mem_rm(c->g );
  return RES_OK;
}

res_T
set_event_properties(struct ray * r, double ds, double dc, struct event * e) {
  res_T res = RES_OK;
  double eps=EPS;

  e->has_scattered=0; /* re initialize scattering */

  if (ds<dc+eps) {
    e->is_at_surface= 1  ; 
    e->distance     = ds ;
  } else {
    e->is_at_surface= 0  ; 
    e->distance     = dc ;
  }
  
  e->location[0] = r->org[0] + r->dir[0]*e->distance;
  e->location[1] = r->org[1] + r->dir[1]*e->distance;
  e->location[2] = r->org[2] + r->dir[2]*e->distance;

  /*d3_add(e->location, d3_mul(e->location, r->dir, e->distance), r->org);  location = org + distance*dir */

  if ((ds==0.)|(dc==0.)) res = RES_BAD_OP ;

  return res ;
}

res_T
update_range(struct ray *r, double dis) {
  INIT_RES ;
  r->range[0]=dis;
  if (r->range[0] == FLT_MAX)
	  res = RES_BAD_ARG ;
  return res ;
}

res_T
find_next_event(struct ssp_rng * rng, struct context * ctx, struct ray *r, struct event * ev) {
  res_T res = RES_OK;
  int i;
  int nbtry=0;
  struct scn_data sd = ctx->scn_dat ;
  double dist_surf, dist_coll;
  float forigin[3], fdirection[3], frange[2];
  struct s3d_hit hit_from = r->hit ;
  int ith=omp_get_thread_num();
  
  /* quand filtrer la derniere prim intersectee ?
   * Surface 
   *  - BOA : oui car je me déplace le long de mon rayon (incertitude de ma position d'arrivée)
   *  - TOA : oui car le range update ne suffit pas à exclure la primitive
   *  - VIRTUAL : oui car le range update ne suffit pas à exclure la primitive
   *  - BBOX : oui car le range update ne suffit pas à exclure la primitive
   *  Volume 
   *  - null : non car l'intersection est toujours valide
   *  - scatter : non car l'intersection est toujours valide */
  if (ev->filter==0) {
    /* ne pas filtrer => set hit_from to null */
    hit_from.prim = S3D_PRIMITIVE_NULL ;
  }

  if (ctx->print==1) {
      fprintf(ctx->debug[ith], "Tracing ray from origin %g %g %g with range %g %g\n",r->org[0],r->org[1],r->org[2],r->range[0],r->range[1]);
      fflush(ctx->debug[ith]) ;
  }
  
  for (i=0;i<3;i++) forigin[i] = (float)r->org[i];
  for (i=0;i<3;i++) fdirection[i] = (float)r->dir[i];
  for (i=0;i<2;i++) frange[i] = (float)r->range[i];

  res = s3d_scene_view_trace_ray(sd.scn_view, forigin, fdirection, frange, &hit_from, &(r->hit));
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not trace ray orig %f %f %f dir %f %f %f range %f %f\n",FUNC_NAME, 
             SPLIT3(forigin), SPLIT3(fdirection), SPLIT2(frange));
    goto error;
  }

  dist_surf = (double)r->hit.distance ;
  if (dist_surf < 0.) {
    if ((ctx->print==1)) fprintf(ctx->debug[ith], "hit is behind, sample next surface\n");
    res = s3d_scene_view_trace_ray(sd.scn_view, forigin, fdirection, frange, &(r->hit_from), &(r->hit));
    if (res!=RES_OK){
      logger_print(&ctx->logger, LOG_ERROR,
          "%s: could not trace ray\n",FUNC_NAME);
      goto error;
    }
    dist_surf = (double)r->hit.distance;
  }

  if (ctx->ke_hat[ith] > KE_HAT_NULL) {
    do {
      if ((ctx->print==1) && (nbtry>1)) fprintf(ctx->debug[ith], "too close %f %f ; resample dist coll\n", dist_coll, dist_surf);
      dist_coll = ssp_ran_exp(rng, ctx->ke_hat[ith]);
      nbtry++;
    } while (eq_eps(dist_coll, dist_surf, EPS));
    if ((ctx->print==1) && (nbtry>1)) fprintf(ctx->debug[ith], "ended up with dist coll %f after %i tries\n", dist_coll, nbtry);
  } else {
    dist_coll = DBL_MAX;
  }

  /*printf("distance to next surf %g,\ndistance to next coll %g\n",dist_surf, dist_coll);*/

  if (ctx->print==1) {
      fprintf(ctx->debug[ith], "tirage distance next coll avec ke hat = %g\n",ctx->ke_hat[ith]) ;
      fprintf(ctx->debug[ith], "distance to next surf %g,\ndistance to next coll %g\n",dist_surf,dist_coll);
  }

  res = set_event_properties(r,dist_surf,dist_coll,ev);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not get set event properties\n",FUNC_NAME);
    goto error;
  }

  if ((ctx->print==1) && (eq_eps(dist_surf, dist_coll, EPS))) {
    fprintf(ctx->debug[ith], "ALERT EQ EPS SURF %g COLL %g ; SURF WINS ? %i\n", dist_surf, dist_coll, ev->is_at_surface);
  }

  if (ev->is_at_surface) { /* keep previous hit in memory */
    r->hit_from = r->hit;
  }

  if ((r->org[2] <= ctx->ztoa)||(ev->location[2] <= ctx->ztoa)) {
    r->path_len += ev->distance;
  } /* else { printf("org %f ev %f < %g \n", r->org[2], ev->location[2], ctx->ztoa);}*/

  /*res = update_range(r, ev->distance);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not update range\n",FUNC_NAME);
    goto error;
  }*/
  
  if (ctx->print==1) {
      fprintf(ctx->debug[ith], "new range from origin : %g\n", r->range[0]);
      fflush(ctx->debug[ith]);
  }

exit:
  return res;
error:
  goto exit;
}

res_T
handle_surfacic_intersection(struct ssp_rng * rng,struct context * ctx, struct ray * r, struct event * ev, struct results * res) ;

res_T
handle_volumic_collision(struct ssp_rng * rng,struct context * ctx, struct ray * r, struct event *  ev, struct results * res) ;

#endif
