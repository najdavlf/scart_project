#include "main.h"
#include "utils.h"
#include "scart_les.h"
#include "const.h"
#include "netCDF.h"
#include <omp.h>
#include <stdlib.h>
#include <string.h>
#include <rsys/mem_allocator.h>
#include <rsys/logger.h>

/* Helper functions */

double 
get_les_rho(struct les_data * ld, size_t ix, size_t iy, size_t iz){
  double rho;
  size_t index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby;
  rho = ld->rho_tab[index];
  return rho;
}

double 
get_les_ql(struct les_data * ld, size_t ix, size_t iy, size_t iz){
  double ql;
  size_t index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby;
  ql = ld->ql_tab[index];
  return ql;
}

double 
get_les_qi(struct les_data * ld, size_t ix, size_t iy, size_t iz){
  double qi;
  size_t index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby;
  qi = ld->qi_tab[index];
  return qi;
}

res_T
set_les_rho(struct les_data * ld, double * pr_tab, double * th_tab){
  res_T res = RES_OK;
  size_t ix, iy, iz;
  size_t index;
  double pr, temp;

  for (iz=0; iz<ld->nbz; iz++){
    for (iy=0; iy<ld->nby; iy++){
      for (ix=0; ix<ld->nbx; ix++){
        index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby ;
        if (ld->use_default_rho) {
          (void)pr; (void)temp; /* unused */
          ld->rho_tab[index] = RHO_A; /* default value */
        } else {
          pr = pr_tab[index];
          temp = th_tab[index];
          ld->rho_tab[index] = rho(pr, T(temp, pr));
        }
      }
    }
  }
exit:
  return res;
error:
  goto exit;
}

res_T
set_les_ql(struct les_data *ld, double * rho_tab, double * rc_tab) {
  res_T res = RES_OK;
  size_t ix, iy, iz;
  size_t index;
  for (iz=0; iz<ld->nbz; iz++){
    for (iy=0; iy<ld->nby; iy++){
      for (ix=0; ix<ld->nbx; ix++){
        index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby ;
        ld->ql_tab[index] = rho_tab[index]*rc_tab[index];
      }
    }
  }
exit:
  return res;
error:
  goto exit;
}

res_T
set_les_qi(struct les_data *ld, double * rho_tab, double * ri_tab) {
  res_T res = RES_OK;
  size_t ix, iy, iz;
  size_t index;
  for (iz=0; iz<ld->nbz; iz++){
    for (iy=0; iy<ld->nby; iy++){
      for (ix=0; ix<ld->nbx; ix++){
        index = ix + iy*ld->nbx + iz*ld->nbx*ld->nby ;
        ld->qi_tab[index] = rho_tab[index]*ri_tab[index];
      }
    }
  }
exit:
  return res;
error:
  goto exit;
}


res_T 
clean_les(struct les_data * ld) {
  res_T res = RES_OK;
  if (ld->lev_tab  != NULL) mem_rm(ld->lev_tab); 
  if (ld->z_tab  != NULL) mem_rm(ld->z_tab); 
  if (ld->ql_tab != NULL) mem_rm(ld->ql_tab);
  if (ld->qi_tab != NULL) mem_rm(ld->qi_tab);
  if (ld->rho_tab != NULL) mem_rm(ld->rho_tab);
  if (ld->LWC_max != NULL) mem_rm(ld->LWC_max);
  if (ld->IWC_max != NULL) mem_rm(ld->IWC_max);
  if (ld->rho_max != NULL) mem_rm(ld->rho_max);
  return res;
}

res_T 
set_les_ncdf_dims(int ncid, struct les_data * ld) {
  INIT_RES ;
  size_t i1D0[1] = {0} ;
  size_t i1Dmax[1] ;
  size_t i3D0[3] = {0,0,0} ;
  size_t i3Dmax[3] = {0,0,0} ;
  ld->nbx  = get_dim(ncid, xname) ;
  ld->nby  = get_dim(ncid, yname) ;
  ld->nbz  = get_dim(ncid, zname) ;
  ld->dx   = get_delta(ncid,xname) ;
  ld->dy   = get_delta(ncid,yname) ;
  i1Dmax[0] = ld->nbx-1 ;
  ld->xmin = get_val(ncid, xname, i1D0) - ld->dx/2. ;
  ld->xmax = get_val(ncid, xname, i1Dmax) + ld->dx/2. ;
  i1Dmax[0] = ld->nby-1 ;
  ld->ymin = get_val(ncid, yname, i1D0) - ld->dy/2. ;
  ld->ymax = get_val(ncid, yname, i1Dmax) + ld->dy/2. ;
  if (ld->z_reg>0) {
    int var_id ;
    if (nc_inq_varid(ncid,zname, &var_id)!=0) {
      ld->dz   = get_delta3d(ncid,zvarname) ;
      i3Dmax[0] = ld->nbz-1 ;
      ld->zmin = get_val(ncid, zvarname, i3D0) - ld->dz/2. ;
      ld->zmax = get_val(ncid, zvarname, i3Dmax) + ld->dz/2. ;
    } else {
      ld->dz   = get_delta(ncid,zname) ;
      i1Dmax[0] = ld->nbz-1 ;
      ld->zmin = get_val(ncid, zname, i1D0) - ld->dz/2. ;
      ld->zmax = get_val(ncid, zname, i1Dmax) + ld->dz/2. ;
    }
  } else {
    ld->dz   = get_delta(ncid,zname) ;
    i1Dmax[0] = ld->nbz-1 ;
    ld->zmin = get_val(ncid, zname, i1D0) - ld->dz/2. ;
    ld->zmax = get_val(ncid, zname, i1Dmax) + ld->dz/2. ;
  }
  return res ;
}
 

res_T
set_les_ncdf_fields(int ncid, int atm,
                    struct les_data * ld) 
{
  res_T res = RES_OK;
  const size_t size = ld->nbx*ld->nby*ld->nbz ;
  size_t i, iz;
  /* local arrays */
  double * th_tab = NULL;
  double * pr_tab = NULL;
  double * rc_tab = NULL;
  double * ri_tab = NULL;

  th_tab = (double*)mem_alloc(size*sizeof(*th_tab));
  pr_tab = (double*)mem_alloc(size*sizeof(*pr_tab));
  rc_tab = (double*)mem_alloc(size*sizeof(*rc_tab));
  ri_tab = (double*)mem_alloc(size*sizeof(*ri_tab));
  
  ld->z_tab = (double*)mem_alloc(ld->nbz*sizeof(*ld->z_tab));
  res = get_tab(ncid, zname, ld->z_tab) ;
  if (res != RES_OK) {
    printf("%s: could not read %s\n", FUNC_NAME, zname);
    goto error ;
  }

  ld->lev_tab = (double*)mem_alloc((1+ld->nbz)*sizeof(*ld->z_tab));
  ld->lev_tab[0] = 0;
  for (iz=0; iz<ld->nbz-1;iz++) {
    if (ld->z_reg) {
      ld->lev_tab[iz+1] = ld->z_tab[iz] + ld->dz/2.;
    } else {
      ld->lev_tab[iz+1] = ld->z_tab[iz] + (ld->z_tab[iz+1]-ld->z_tab[iz])/2.;
    /*
     * printf("lev %i %f\n", iz, ld->lev_tab[iz]);
     * printf("z   %i             %f\n", iz, ld->z_tab[iz]);
     */
    }
  }
  /* last interface above LES */
  ld->lev_tab[ld->nbz] = ld->z_tab[ld->nbz-1] + (ld->z_tab[ld->nbz-1]-ld->lev_tab[ld->nbz-1]);

  res = get_tab(ncid, thname, th_tab);
  if (res != RES_OK) {
    res=RES_OK;
    printf("warning: no temperature field; will take default rho %g to compute lwc from rc \n", RHO_A);
    if (atm==1) {
      printf("warning: no temperature field; will take default rho %g to compute ks in gas (homogeneous) \n", RHO_A);
    }
    ld->use_default_rho=1;
  }

  res = get_tab(ncid, prname, pr_tab);
  if (res != RES_OK) {
    res=RES_OK;
    printf("warning: no pressure field; will take default rho %g to compute lwc from rc \n", RHO_A);
    if (atm==1) {
      printf("warning: no pressure field; will take default rho %g to compute ks in gas (homogeneous) \n", RHO_A);
    }
    ld->use_default_rho=1;
  }

  res = get_tab(ncid, rcname, rc_tab);
  if (res != RES_OK) {
    res=RES_OK;
    printf("warning: no liquid mixing ratio field %s in %s\n", rcname, ld->les_file);
    for (i=0;i<size;i++) rc_tab[i] = 0.;
  }

  res = get_tab(ncid, riname, ri_tab);
  if (res != RES_OK) {
    res=RES_OK;
    printf("warning: no ice mixing ratio field %s in %s\n", riname, ld->les_file);
    for (i=0;i<size;i++) ri_tab[i] = 0.;
  } 

  (ld->rho_tab) = (double*)mem_alloc(size*sizeof(*ld->rho_tab));
  res = set_les_rho(ld, pr_tab, th_tab);

  (ld->ql_tab) = (double*)mem_alloc(size*sizeof(*ld->ql_tab));
  res = set_les_ql(ld, ld->rho_tab, rc_tab);

  (ld->qi_tab) = (double*)mem_alloc(size*sizeof(*ld->qi_tab));
  res = set_les_qi(ld, ld->rho_tab, ri_tab);

  (ld->LWC_max) = (double*)mem_alloc(ld->nbz*sizeof(*ld->LWC_max));
  (ld->IWC_max) = (double*)mem_alloc(ld->nbz*sizeof(*ld->IWC_max));
  (ld->rho_max) = (double*)mem_alloc(ld->nbz*sizeof(*ld->rho_max));

exit :
    if (th_tab!=NULL) mem_rm(th_tab);
    if (pr_tab!=NULL) mem_rm(pr_tab);
    if (rc_tab!=NULL) mem_rm(rc_tab);
    if (ri_tab!=NULL) mem_rm(ri_tab);
    return res ;
error : 
    goto exit ;
}

res_T
set_les_WCmax(struct les_data * ld,  int cloud) {
  /* Called at the beggining of the simulation */
  res_T res = RES_OK;
  const size_t size = ld->nbx*ld->nby;
  int iz, ind;

  if (ld->les_file!=NULL) {
    for (iz=0;iz<ld->nbz;iz++) {
      ld->rho_max[iz] = get_max(&(ld->rho_tab[iz*size]),size, &ind);
      if (cloud>0) {
        ld->LWC_max[iz] = get_max(&(ld->ql_tab[iz*size]), size, &ind);
        ld->IWC_max[iz] = get_max(&(ld->qi_tab[iz*size]), size, &ind);
      }
    }
  } else {
    res = RES_BAD_ARG ;
    goto error;
  }

exit: 
  return res ;
error:
  goto exit;
}

res_T 
set_les_bot_and_top(struct les_data*ld) {
  res_T res = RES_OK;
  int tmp;
  size_t lay=0, ixy=0;
  double * qmax=NULL;
  double * qlay=NULL;

  qmax = (double*)mem_alloc(ld->nbz*sizeof(*qmax));
  qlay = (double*)mem_alloc(ld->nbx*ld->nby*sizeof(*qlay));

  if (ld->les_file!=NULL) {
    for (lay=0;lay<ld->nbz;lay++) {
      for (ixy=0;ixy<ld->nbx*ld->nby;ixy++) {
        qlay[ixy] = ld->ql_tab[lay*ld->nbx*ld->nby + ixy] + 
                    ld->qi_tab[lay*ld->nbx*ld->nby + ixy];
      } 
      qmax[lay] = get_max(qlay,ld->nbx*ld->nby,&tmp);
    }
    lay = 0 ;
    /* la position de l'interface est lev_bot*dz en bas et (lev_top+1)*dz en haut */
    /* lev_bot la première couche où il ya du nuage */
    /* couche 1 (indice 0) : interface 0 ; couche 2 (indice 1) : interface dz */
    /* lev_top la dernière couche où il ya du nuage */
    /* couche 1 (indice 0) : interface dz ; couche 10 (indice 9) : interface 10dz */
    
    while (ld->lev_bot==-1 && lay<ld->nbz) {
      if (qmax[lay] > 0.) {
        ld->lev_bot=(int)lay ;
      }
      lay++ ;
    }
    lay=ld->nbz-1;
    while(lay>0 && ld->lev_top==-1) {
      if (qmax[lay] > 0.) {
        ld->lev_top=(int)lay ;
      }
      lay-- ; 
    }
    if (ld->lev_bot>-1) {/*printf("ok lev_bot plus grand que -1\n");*/
      ld->nbly++ ;
    }
    if (ld->lev_top>-1) {/*printf("ok lev_top plus grand que -1\n");*/
      ld->nbly++ ;
    }
  }
  else {
    res=RES_BAD_ARG;
    goto error;
  }

  if (ld->z_reg>0) {
    ld->pos_bot = ld->dz*((double)ld->lev_bot)+ld->zmin ;
    ld->pos_top = ld->dz*((double)ld->lev_top+1)+ld->zmin ;
  }
  else {
    double dzirreg ;
    if (ld->lev_bot>0) {
      ld->pos_bot = .5*(ld->z_tab[ld->lev_bot-1]+ld->z_tab[ld->lev_bot]);
    }  else if (ld->lev_bot==0) {
      ld->pos_bot = ld->zmin;
    } 
    if (ld->lev_top<0) {
      ld->pos_top = ld->zmax;
    } else if (ld->lev_top<ld->nbz-2) {
      ld->pos_top = .5*(ld->z_tab[ld->lev_top]+ld->z_tab[ld->lev_top+1]);
    } else {
      ld->pos_top = ld->zmax;
    }
  }

  if (ld->pos_bot<ld->zmin) ld->pos_bot=ld->zmin ;
  if (ld->pos_top>ld->zmax) ld->pos_top=ld->zmax ;

  printf("bbox clouds lbot %i ltop %i\n", ld->lev_bot, ld->lev_top);
  printf("bbox clouds zbot %f ztop %f\n", ld->pos_bot, ld->pos_top);

  exit: 
    if (qmax!=NULL) mem_rm(qmax);
    if (qlay!=NULL) mem_rm(qlay);
    return res ;
  error:
    goto exit ;
}

res_T 
init_les_data(struct logger* logger, struct les_data * ld,  
    int cloud, int atmosphere, 
    char * les_file, 
    int z_reg,
    size_t nth) 
{
  res_T res = RES_OK;
  /*const int nth=omp_get_num_procs();*/
  size_t i, iz;
  int ncid;

  ld->les_file = les_file ;

  /* this is in const.h */
  ld->liq_reff = LIQ_REFF; /* 10 mum in km */
  ld->liq_veff = LIQ_VEFF; /* no unit ???? */
  ld->ice_reff = ICE_REFF; /* 100 mum in km */
  ld->ice_veff = ICE_VEFF; /* no unit ???? */

  /* Init to NULL to later check if was allocated */
  ld->lev_tab = NULL;
  ld->z_tab = NULL;
  ld->ql_tab = NULL;
  ld->qi_tab = NULL;
  ld->rho_tab = NULL;
  /* to compute hat k */
  ld->LWC_max=NULL;
  ld->IWC_max=NULL;
  ld->rho_max=NULL;

  /* will use RHO_A if pressure or temperature is missing */
  ld->use_default_rho=0;

  ld->z_reg = z_reg; /* is vertical grid regular? */

  ld->nbly = 0;    /* number of les bbox interfaces */
  ld->lev_bot=-1;  /* index of first layer with cloud */
  ld->lev_top=-1;  /* index of last layer with cloud  */

  if (les_file!=NULL) {
    res = get_ncid(les_file, &ncid);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR,
          "%s: LES file does not exist or is not netCDF\n", FUNC_NAME);
      goto error;
    }

    res = set_les_ncdf_dims(ncid, ld);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR,
          "%s: dims could not be found in LES file\n", FUNC_NAME);
      goto error;
    }

    res = set_les_ncdf_fields(ncid, atmosphere, ld);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR,
          "%s: field could not be found in LES file\n", FUNC_NAME);
      goto error;
    }

    res = put_ncid(ncid);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR, 
        "%s: could not release netCDF id\n", FUNC_NAME);
      goto error;
    }
  }  else { 
    (void)ncid;
    res = RES_BAD_ARG;
    logger_print(logger, LOG_ERROR, 
      "%s: no LES file was provided\n", FUNC_NAME);
    goto error;
  }
  
  /* will compute LWCmax only if cloud == 1 */
  res = set_les_WCmax(ld, cloud);
  if (res!=RES_OK) {
    logger_print(logger, LOG_ERROR,
      "%s: could not compute max LWC or IWC\n", FUNC_NAME);
    goto error;
  }
  /*
  for (iz=0;iz<ld->nbz;iz++) {
    printf("%i %f %f\n", iz, ld->LWC_max[iz], ld->IWC_max[iz]);
  }
  */

  /* 
  logger_print(logger, LOG_OUTPUT,
    "%s: LWC max %g, IWC max %g\n", FUNC_NAME, ld->LWC_max, ld->IWC_max);
  */

  /* si WCmax > 0, on peut trouver les niveaux verticaux encadrant la couche nuageuse (où WC>0) */
  /*
  if (ld->IWC_max + ld->LWC_max > 0.) {
    res = set_les_bot_and_top(ld);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR,
          "%s: could not set LES bottom and top cloud layer\n", FUNC_NAME);
      goto error;
    }
  }
  */

exit : 
  return res ;
error :
  goto exit ;
}
