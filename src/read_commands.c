#include "main.h"
#include "parser.h"
#include "scart_context.h"
#include "conf.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <rsys/logger.h>

res_T
parse_coordinates(char*arg, double*coords)
{
  res_T res = RES_OK;
  size_t len;
  res = cstr_to_list_double(arg, ',', coords, &len, 2);
  return res;
}

res_T 
read_namelist( char*namelist, struct config * conf) {
    config_option * co ;
    config_option * tmp_co;
    if ((co = read_config_file(namelist)) == NULL) {
        perror("read_config_file()");
        return RES_BAD_ARG;
    }
    while(1) {
      /* commence à la dernière ligne lue */
      printf("  %s = %s\n", co->key, co->value);
      if (!strcmp(co->key,"nbpaths"))   conf->nbpaths  = (size_t)atoi(co->value) ;
      else if (!strcmp(co->key,"nbly"))      conf->nbly += (size_t)atol(co->value) ;
      else if (!strcmp(co->key,"nbx"))       conf->nbx = (size_t)atol(co->value) ;
      else if (!strcmp(co->key,"nby"))       conf->nby = (size_t)atol(co->value) ;
      else if (!strcmp(co->key,"mie_file")||!strcmp(co->key,"liq_op_file"))  {
        conf->liq_op_file   = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->liq_op_file, co->value)   ; 
      }
      else if (!strcmp(co->key,"ice_op_file"))  {
        conf->ice_op_file   = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->ice_op_file, co->value)   ; 
      }
      else if (!strcmp(co->key,"atm_file"))  { conf->atm_file   = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->atm_file, co->value)   ; }
      else if (!strcmp(co->key,"les_file"))  { conf->les_file   = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->les_file, co->value)   ; }
      else if (!strcmp(co->key,"sun_file"))  { conf->sun_file   = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->sun_file, co->value)   ; }
      else if (!strcmp(co->key,"write"))     { conf->write_file = (char*)mem_alloc((1+strlen(co->value))*sizeof(char)) ; strcpy(conf->write_file, co->value) ; }
      else if (!strcmp(co->key,"cloud"))     conf->cloud = atoi(co->value) ;
      else if (!strcmp(co->key,"atmosphere"))conf->atmosphere = atoi(co->value) ;
      else if (!strcmp(co->key,"grd_alb"))   conf->ground_albedo = atof(co->value) ;
      else if (!strcmp(co->key,"g_assym")||!strcmp(co->key,"liq_g_assym"))  conf->liq_g_assym = atof(co->value) ;
      else if (!strcmp(co->key,"ssa")||!strcmp(co->key,"liq_ssa"))          conf->liq_ssa = atof(co->value) ;
      else if (!strcmp(co->key,"ice_g_assym"))   conf->ice_g_assym = atof(co->value) ;
      else if (!strcmp(co->key,"ice_ssa"))       conf->ice_ssa = atof(co->value) ;
      else if (!strcmp(co->key,"theta"))     conf->theta_sun = atof(co->value) ;
      else if (!strcmp(co->key,"phi"))       conf->phi_sun = atof(co->value) ;
      else if (!strcmp(co->key,"latitude"))     conf->latitude = atof(co->value) ; 
      else if (!strcmp(co->key,"day"))       conf->day = atof(co->value) ;
      else if (!strcmp(co->key,"hour"))       conf->hour = atof(co->value) ;
      else if (!strcmp(co->key,"cut_nose"))  conf->cut_nose = atof(co->value) ;
      else if (!strcmp(co->key,"monochromatic")) conf->monochromatic = atof(co->value) ;
      else if (!strcmp(co->key,"print"))     conf->print = atoi(co->value);
      else if (!strcmp(co->key,"use_mie_pf")) conf->use_mie_pf = atoi(co->value) ;
      else if (!strcmp(co->key,"do_two_stream")) conf->do_two_stream = atoi(co->value) ;
      else if (!strcmp(co->key,"do_direct_diffuse")) conf->do_direct_diffuse = atoi(co->value) ;
      else if (!strcmp(co->key,"do_delta_scaling")) conf->do_delta_scaling = atoi(co->value) ;
      else if (!strcmp(co->key,"do_russian_reflection")) conf->do_russian_reflection = atoi(co->value) ;
      else if (!strcmp(co->key,"do_spectral_integration")) conf->do_spectral_integration = atoi(co->value) ;
      else if (!strcmp(co->key,"do_write_spectral")) conf->do_write_spectral = atoi(co->value) ;
      else if (!strcmp(co->key,"do_time_integration")) conf->do_time_integration = atoi(co->value) ;
      else if (!strcmp(co->key,"do_latitudinal_integration")) conf->do_latitudinal_integration = atoi(co->value) ;
      else if (!strcmp(co->key,"do_direct_in_solid_angle")) conf->do_direct_in_solid_angle = atoi(co->value) ;
      else if (!strcmp(co->key,"do_remove_periodicity")) conf->do_remove_periodicity = atoi(co->value) ;
      else if (!strcmp(co->key,"do_4VS")) conf->do_4VS = atoi(co->value);
      else if (!strcmp(co->key,"do_msi")) conf->do_msi = atoi(co->value);
      else if (!strcmp(co->key,"do_ad")) conf->do_ad = atoi(co->value);
      else if (!strcmp(co->key,"ad_xcoords")) parse_coordinates(co->value, conf->ad_coords[0]);
      else if (!strcmp(co->key,"ad_ycoords")) parse_coordinates(co->value, conf->ad_coords[1]);
      else if (!strcmp(co->key,"theta_diffus_down")) conf->theta_diffus_down = atof(co->value);
      else if (!strcmp(co->key,"theta_diffus_up")) conf->theta_diffus_up = atof(co->value);
      else if (!strcmp(co->key,"direct_solid_angle")) conf->direct_solid_angle = atof(co->value);
      else if (!strcmp(co->key,"zmin")) conf->zmin = atof(co->value);
      else if (!strcmp(co->key,"zmax")) conf->zmax = atof(co->value);
      else if (!strcmp(co->key,"z_reg")) conf->z_reg = atoi(co->value);
      else if (!strcmp(co->key,"ica")) conf->ica = (size_t)atoi(co->value);
      else if (!strcmp(co->key,"map_ica")) conf->map_ica = (size_t)atoi(co->value);
      else if (!strcmp(co->key,"max_reflections_symb")) conf->max_reflections_symb= atoi(co->value);
      else printf("Unkown key %s\n",co->key);
      if (co->prev != NULL) {
        /* s'il reste des lignes avant la courante 
         * adresse de la structure devient 
         * l'adresse de la structure précédente  */
        tmp_co = co ;
        co = co->prev;
        mem_rm(tmp_co) ;
      } else { /* si c'est la dernière, on dégage  */
        mem_rm(co) ; 
        break;
      }
    }
  return RES_OK ;
}

res_T 
print_usage(struct config * conf, struct context * ctx)
{ 
  INIT_RES ;
  logger_print(&(ctx->logger),LOG_OUTPUT,"Optional key arguments in config.nam \
(description) and default values : \n\t %s%lu%s %s%lu%s %s%lu%s %s%lu%s \
%s%s%s %s%s%s %s%s%s %s%s%s %s%s%s %s%s%s %s%i%s %s%i%s %s%g%s %s%g%s \
%s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s \
%s%i%s %s%i%s %s%i%s %s%i%s %s%i%s %s%i%s %s%i%s %s%i%s %s%i%s %s%i%s \
%s%i%s %s%i%s %s%i%s %s%i%s %s%g%s %s%g%s %s%g%s %s%g%s %s%g%s %s%lu%s \
%s%lu%s %s%i%s %s%i\n",\
      "nbpaths                    (number of trajectories to simulate)                                    ", conf->nbpaths,  "\n\t",
      "nbly                       (number of vertical points where fluxes will be measured)               ", conf->nbly, "\n\t",
      "nbx                        (number of longitudinal points for fluxes computation                   ", conf->nbx, "\n\t",
      "nby                        (number of latitudinal points for fluxes computation                    ", conf->nby, "\n\t",
      "liq_op_file                (name of netCDF data file for Mie look up tables (liquid))              ", conf->liq_op_file, "\n\t",
      "ice_op_file                (name of netCDF data file for Mie look up tables (ice))                 ", conf->ice_op_file, "\n\t",
      "atm_file                   (name of netCDF data file for atmospheric profiles)                     ", conf->atm_file, "\n\t",
      "les_file                   (name of netCDF data file for 3D atmospheric fields)                    ", conf->les_file, "\n\t",
      "sun_file                   (name of netCDF data file for sun position and DNI)                     ", conf->sun_file, "\n\t",
      "write                      (name of netCDF write file for results)                                 ", conf->write_file, "\n\t",
      "cloud                      (effect of liquid clouds)                                               ", conf->cloud, "\n\t",
      "atmosphere                 (effect of atmospheric molecules)                                       ", conf->atmosphere, "\n\t",
      "grd_alb                    (ground albedo)                                                         ", conf->ground_albedo, "\n\t",
      "liq_g_assym                (assymetry parameter for HG phase function (liquid))                    ", conf->liq_g_assym, "\n\t",
      "liq_ssa                    (single scattering albedo (liquid))                                     ", conf->liq_ssa, "\n\t",
      "ice_g_assym                (assymetry parameter for HG phase function (ice))                       ", conf->ice_g_assym, "\n\t",
      "ice_ssa                    (single scattering albedo (ice))                                        ", conf->ice_ssa, "\n\t",
      "theta                      (solar zenith angle, in degrees)                                        ", conf->theta_sun, "\n\t",
      "phi                        (solar azimuth angle, in degrees)                                       ", conf->phi_sun,"\n\t",
      "latitude                   (latitude in degrees if sun_file is provided)                           ", conf->latitude,"\n\t",
      "day                        (day in number if sun_file is provided)                                 ", conf->day,"\n\t",
      "hour                       (hour in number if sun_file is provided)                                ", conf->hour,"\n\t",
      "cut_nose                   (opening angle in degrees that will be cut off the pf)                  ", conf->cut_nose,"\n\t",
      "monochromatic              (monochromatic wavelength in nm)                                        ", conf->monochromatic,"\n\t",
      "print                      (print mode (0: minimum, 1: debug, 2: path, 3: angle at boa, \n\                        
                                        4: horizontal distances, 5: directions before scattering))          ", conf->print,"\n\t",
      "use_mie_pf                 (use Mie phase function (0: use Henyey Greenstein))                     ", conf->use_mie_pf,"\n\t",
      "do_two_stream              (perform two-stream approximation (up / dn propagation)                 ", conf->do_two_stream,"\n\t",
      "do_direct_diffuse          (compute distinct direct and diffuse fluxes)                            ", conf->do_direct_diffuse,"\n\t",
      "do_delta_scaling           (use delta-Eddington approximation)                                     ", conf->do_delta_scaling,"\n\t",
      "do_russian_reflection      (use russian roulette to reflect path at the ground)                    ", conf->do_russian_reflection,"\n\t",
      "do_spectral_integration    (integrate over spectrum if atm_file is provided)                       ", conf->do_spectral_integration,"\n\t",
      "do_write_spectral          (write spectral fluxes if atm_file is provided)                         ", conf->do_write_spectral,"\n\t",
      "do_time_integration        (integrate over time if sun_file is provided)                           ", conf->do_time_integration,"\n\t",
      "do_latitudinal_integration (integrate over latitudes if sun_file is provided)                      ", conf->do_latitudinal_integration,"\n\t",
      "do_direct_in_solid_angle   (direct is defined as in a given solid angle around sun dir)            ", conf->do_direct_in_solid_angle,"\n\t",
      "do_remove_periodicity      (do not repeat cloud domain)                                            ", conf->do_remove_periodicity,"\n\t",
      "do_4VS                     (remove abs, set lambertian illumination)                               ", conf->do_4VS,"\n\t",
      "do_msi                     (compute nadir radiances at TOA)                                        ", conf->do_msi,"\n\t",
      "theta_diffus_down          (angle of downward diffusion for 2-stream approximation, in degrees)    ",conf->theta_diffus_down,"\n\t",
      "theta_diffus_up            (angle of upward diffusion for 2-stream approximation, in degrees)      ",conf->theta_diffus_up,"\n\t",
      "direct_solid_angle         (solid angle around sun dir, in degrees, if do_direct_in_solid_angle)   ",conf->direct_solid_angle,"\n\t",
      "zmin                       (minimum height in kilometers to measure fluxes)                        ", conf->zmin,"\n\t",
      "zmax                       (maximum height in kilometers to measure fluxes)                        ", conf->zmax,"\n\t",
      "ica                        (number of columns n for independent subdomain mode \n\
                                     (subdomain = nxn columns))                                             ", conf->ica,"\n\t",
      "map_ica                    (0: integrate fluxes horizontally on nbx x nby pixels \n\
                                     1: integrate fluxes horizontally on ica domains)                       ", conf->map_ica,"\n\t",
      "z_reg                      (vertical grid is regular)                                              ", conf->z_reg, "\n\t",
      "max_reflections_symb       (accounted for in symbolic MC)                                          ", conf->max_reflections_symb);
  res=RES_WARNING; /* label for warnings */
  return res ;
}


res_T 
check_config(struct config * conf, struct context * ctx)
{ 
  INIT_RES ;

  if (conf->les_file==NULL) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: no input netCDF file found\n",FUNC_NAME);
    goto error;
  }

  if ((conf->use_mie_pf)&((!conf->liq_op_file)||(!conf->ice_op_file))) {
    logger_print(&ctx->logger, LOG_WARNING,
        "%s: will not use Mie phase function where not provided\n",FUNC_NAME);
  }
  if ((conf->liq_op_file)) {
    logger_print(&ctx->logger, LOG_WARNING,
        "%s: will use Mie optical properties for liquid water since Mie data file was given\n\
         %s: values for liq_g_assym and liq_ssa will not be used even if provided\n",FUNC_NAME,FUNC_NAME);
  }
  if ((conf->ice_op_file)) {
    logger_print(&ctx->logger, LOG_WARNING,
        "%s: will use Mie optical properties for ice water since Mie data file was given\n\
         %s: values for ice_g_assym and ice_ssa will not be used even if provided\n",FUNC_NAME,FUNC_NAME);
  }

  if ((conf->do_delta_scaling)&(conf->use_mie_pf)) {
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: cannot use Mie phase function with delta scaling\n",FUNC_NAME);
    goto error;
  }

  if(conf->nbpaths <= 0) {
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: invalid number of paths %lu\n", FUNC_NAME,conf->nbpaths);
    goto error;
  }

  if ((conf->ground_albedo < 0) | (conf->ground_albedo > 1)) {
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: ground albedo must be in [0,1]\n",FUNC_NAME);
    goto error ;
  }

  if((conf->liq_g_assym<-1)||(conf->liq_g_assym>1)||(conf->ice_g_assym<-1)||(conf->ice_g_assym>1)){
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: g must be in [-1, 1]\n",FUNC_NAME);
    goto error ;
  }

  if ((conf->liq_ssa < 0)||(conf->liq_ssa > 1)||(conf->ice_ssa < 0)||(conf->ice_ssa > 1)) {
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: single scattering albedo must be in [0,1]\n",FUNC_NAME);
    goto error ;
  }

  if ((conf->do_latitudinal_integration==1)&(conf->do_time_integration==0)) {
    /* Chose time and integrate over latitudes (not implemented) */
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: cannot integrate over latitudes only.\n",FUNC_NAME);
    goto error ;
  } else if ((conf->do_latitudinal_integration==0)&(conf->do_time_integration==1)) {
    /* Chose lat and integrate over time */
    if ((conf->latitude<-90)|(conf->latitude>90)) { 
      res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: wrong latitude %g\n", FUNC_NAME, conf->latitude);
      goto error ;
    } else { 
      conf->theta_sun=-1 ; conf->phi_sun=-1 ; }
  } else if ((conf->do_latitudinal_integration==0)&(conf->do_time_integration==0)) {
    /* No integration in space or time */
    if ((conf->theta_sun<0)|(conf->theta_sun>90)) {
      res=RES_BAD_ARG ;
      logger_print(&ctx->logger, LOG_ERROR,
      "%s: wrong value for theta %g (must be in [0,90])\n", FUNC_NAME, conf->theta_sun);
      goto error ;
    }
  } else if ((conf->do_latitudinal_integration==1)&(conf->do_time_integration==1)) {
    /* Integration in space AND time */
    conf->theta_sun=-1 ; conf->phi_sun=-1 ;
  } 

  if ((conf->latitude>-90)&(conf->latitude<90)) {
    if (conf->sun_file==NULL) {
      res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: sun file missing to solve radiation at latitude %g\n",FUNC_NAME,conf->latitude);
      goto error ;
    }
    if ((!conf->do_time_integration) & ((conf->hour < 0) | (conf->day < 0))) {
      res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: need to either integrate over time or chose hour and day\n",FUNC_NAME);
      goto error ;
    }
    conf->phi_sun=-1 ; 
    conf->theta_sun=-1 ; 
  }

  if ((conf->do_time_integration+conf->do_latitudinal_integration>0)&(conf->sun_file==NULL)) {
    res = RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: sun file missing to integrate over space and/or time\n",FUNC_NAME);
    goto error ;
  }

  if ((conf->map_ica>0) && (conf->ica==0)){
     logger_print(&ctx->logger, LOG_WARNING,
    "%s: map_ica is set to %lu but ica is set to %lu. ICA will not be performed and map_ica will have no effect\n",FUNC_NAME,conf->map_ica,conf->ica);
          conf->map_ica=0;
  }

  if ((conf->atm_file==NULL)){
    res=RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
	  "%s: atmospherical data must be provided through the atm_file key argument\n",FUNC_NAME);
	  goto error ;
  }

  if ((conf->do_spectral_integration>0) && (conf->atm_file==NULL)){
    res=RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
	  "%s: if spectral integration is required, then atmospherical data must be provided through the atm_file key argument\n",FUNC_NAME);
	  goto error ;
  }

  if ((conf->do_write_spectral>0) && (conf->atm_file==NULL)){
    res=RES_BAD_ARG ;
    logger_print(&ctx->logger, LOG_ERROR,
	  "%s: if spectral fluxes are to be output, then atmospherical data must be provided through the atm_file key argument\n",FUNC_NAME);
	  goto error ;
  }

  if (conf->max_reflections_symb < 0) {
    res=RES_BAD_ARG;
    logger_print(&ctx->logger, LOG_ERROR,
	  "%s: maximum number of terms in MCS cannot be less than 0 (%i in conf)\n", FUNC_NAME, conf->max_reflections_symb);
	  goto error ;
  }

error :
    goto exit ;
exit :
    NULL ;
  return res ;
}


res_T
read_commands(const int argc, char ** argv, struct config * conf, struct context * ctx)
{
  INIT_RES ;
  int nb_max_arg = 2 ;

  if (argc==1) {
    logger_print(&ctx->logger,LOG_ERROR,
        "%s: missing config file \nTry %s -h to get more info\n",FUNC_NAME,argv[0]) ;
    res = RES_BAD_ARG ;
    goto error ;
  } else if (argc > nb_max_arg) {
    logger_print(&ctx->logger,LOG_ERROR,
        "%s: too many arguments on command line\n",FUNC_NAME) ;
    res = RES_BAD_ARG ;
    goto error ;
  } else {
    *conf = CONF_INIT;
  }

  if ((argc==2)&&(!strcmp(argv[1],"-h"))) {
    /* Print optional commands description */
    logger_print(&ctx->logger, LOG_OUTPUT, "Usage: %s -h|config.nam\n\n", argv[0]);
    res=print_usage(conf, ctx) ;
    goto exit ;
  } else if (argc==2) {
    /* Set parameters values from namelist file */
    res = read_namelist(argv[1], conf);
    if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
      "%s: error while reading namelist\n",FUNC_NAME);
    goto error;
    }
  }

  res = check_config(conf, ctx);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: error in parameter value\n",FUNC_NAME) ;
    goto error;
  }

error : 
    goto exit ;
exit : 
  return res ;
}
