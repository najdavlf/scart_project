#include "main.h"
#include "scart_context.h"
#include "const.h"
#include "realization.h"
#include "utils.h"
#include <star/ssp.h>
#include <star/s3d.h>
#include <time.h>
#include <omp.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

res_T
compute_path(struct context * ctx, 
             struct ssp_rng* rng,
             const int ithread,
             const size_t ipath,
             struct results results)
{
  res_T res = RES_OK;
  const size_t maxsy = ctx->max_reflections_symb;
  const size_t nbbds = ctx->nbbds;
  const size_t nbabs = ctx->nbabs/nbbds;
  const size_t nbnet = ctx->nbnet/nbbds;
  const size_t nbflx = ctx->nbflx/nbbds;
  const size_t nbdir = nbflx*ctx->direct_diffuse;
  const size_t nbsyf = nbnet*maxsy;
  const size_t nbsya = nbabs*maxsy;
  const size_t nbx = ctx->scn_dat.nbx;
  const size_t nby = ctx->scn_dat.nby;

  const size_t mod=1e5; /* print data every mod realizations */

  double tmp_fup[nbflx];
  double tmp_fdn[nbflx];
  double tmp_net[nbnet];
  double tmp_abs[nbabs];
  double tmp_dir[nbdir];
  double tmp_difdn[nbdir];
  double tmp_difup[nbdir];
  double tmp_fup_alb[nbsyf];
  double tmp_fdn_alb[nbsyf];
  double tmp_abs_alb[nbsya];

  struct ray r ; 
  int i,j,k,iz,izi,izj;
  const int ith = ithread;
  const size_t nth = (size_t)omp_get_num_procs() ;
  static size_t real_num_all = 0;
  char file_debug[70];

  const time_t abstime = time(NULL);
  const struct tm ltime=*localtime(&abstime);
  clock_t c_beg_path, c_end_path, c_beg_real, c_end_real;
  time_t t_beg_path, t_end_path, t_beg_real, t_end_real;
  int ibd;
  int nrefs;

  c_beg_real = clock();
  t_beg_real = time(NULL);

  for (i=0; i<nth; i++) {
    ctx->first[i] = 1;
    ctx->real_num[i] = (ctx->nbpaths)/nth*i;
  }

  real_num_all++;
  if (real_num_all % mod == 0) 
   logger_print(&ctx->logger,LOG_OUTPUT,
      "%lu : %02i/%02i - %02i:%02i:%02i \n",(real_num_all), ltime.tm_mday, ltime.tm_mon+1, ltime.tm_hour, ltime.tm_min, ltime.tm_sec);

  ctx->real_num[ith]++;

  if (ctx->print==0) {
    ctx->debug[ith] = NULL; /* fopen(NULL, "w") ;*/
  }
  else if (ctx->print==1) {
      sprintf(file_debug, "debug%02i.txt", ith+1) ;
      ctx->debug[ith] = fopen(file_debug, "w") ;
      fprintf(ctx->debug[ith], "I am %i\n",ith);
      fprintf(ctx->debug[ith],"les zmin,zmax %g %g\natm zmin,zmax %g %g\nscn zmin,zmax %g %g\nall zboa,ztoa %g %g\n",
      ctx->les_dat.zmin,ctx->les_dat.zmax,
      ctx->atm_dat.zmin,ctx->atm_dat.zmax,
      ctx->scn_dat.zmin,ctx->scn_dat.zmax,
      ctx->zboa,ctx->ztoa);
      fflush(ctx->debug[ith]) ;
  } else if (ctx->print==2) {
    sprintf(file_debug, "path%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ;
    fprintf(ctx->debug[ith],"\n"); 
  } else if (ctx->print==3) {
    sprintf(file_debug, "angle%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else if (ctx->print==4) {
    sprintf(file_debug, "horizontal_sza%4g_proc%02i_dscale%01i.txt", ctx->the[ith], ith+1, ctx->delta_scaling) ;
    ctx->debug[ith] = fopen(file_debug, "a") ;
    if (ctx->first[ith]==1) {
    fprintf(ctx->debug[ith],"// SZA [°] %g, wavelength %gnm, LES %s, gas %i, mie %i, asym %g, ground albedo %g, nbpaths %lu\n",ctx->the[ith], ctx->mono_nu, ctx->les_dat.les_file, ctx->col_dat.atmosphere, ctx->liq_op_dat.cop_file!=NULL, ctx->liq_op_dat.g, ctx->grd_dat.albedo, ctx->nbpaths) ;
    fprintf(ctx->debug[ith], "# path\t# scattering\t# level\tuz\tz [km]\tx [km]\ty [km]\t# x0\t# xL\t# y0\t# yL\tw [-]\n");
    ctx->first[ith]=0 ;
    }
  } else if (ctx->print==5) {
    sprintf(file_debug, "directions%02i.dat", ith+1) ;
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else if (ctx->print==6) {
    sprintf(file_debug, "inds%02i.txt", ith+1);
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else if (ctx->print==7) {
    sprintf(file_debug, "khats%02i.txt", ith+1);
    ctx->debug[ith] = fopen(file_debug, "a") ; 
  } else {
    ctx->debug[ith] = fopen(NULL, "w") ;
    printf("Invalid print %i",ctx->print); ctx->print = 0;
  }

  r = RAY_NULL;
  r.ph.id = real_num_all ;

  res = sample_sun(rng,ctx);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: could not sample sun\n",FUNC_NAME);
    goto error;
  }

  res = emission(rng,ctx,&r);
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: could not emit particle\n",FUNC_NAME);
    goto error;
  }

  /* In ICA mode, saving origin horizontal location */
  if (ctx->ica) {
    size_t ix = (size_t)get_x_ind(ctx,r.org[0],0);
    size_t iy = (size_t)get_y_ind(ctx,r.org[1],0);
    ctx->I0[ith] = (ix/ctx->ica)*ctx->ica ; 
    ctx->J0[ith] = (iy/ctx->ica)*ctx->ica ; 
    if (ctx->print==1){
      fprintf(ctx->debug[ith],"Emitted at position %g %g\n",r.org[0],r.org[1]);
      fprintf(ctx->debug[ith],"This is raw indexes %lu %lu\n", ix,iy) ;
      fprintf(ctx->debug[ith],"Upper left cell of ICA subdomain %lu %lu\n", ctx->I0[ith],ctx->J0[ith]);
    }
  }

  res = set_cop_nu(ctx, &ctx->liq_op_dat, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set Mie data for wavelength %f\n",FUNC_NAME, r.ph.nu);
    goto error;
  }

  res = set_cop_nu(ctx, &ctx->ice_op_dat, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set Mie data for wavelength %f\n",FUNC_NAME, r.ph.nu);
    goto error;
  }

  res = set_atm_gp(ctx, &(r.ph));
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not set atm data for iband %lu, gpoint %lu\n",FUNC_NAME, r.ph.ibd, r.ph.igp);
    goto error;
  }

  ctx->ke_hat[ith] = KE_HAT_NULL;

  if (ctx->print==1) {
    fprintf(ctx->debug[ith], "\n%i emitted at %g %g %g\n", r.ph.id, r.org[0], r.org[1], r.org[2]) ;
    fprintf(ctx->debug[ith], "with lambda %g in band %i and qd pt %i\n",r.ph.nu,r.ph.ibd, r.ph.igp);
    fprintf(ctx->debug[ith], "my g is %g and ssa %g\n",
          ctx->liq_op_dat.g, ctx->liq_op_dat.ssa);
  } else if (ctx->print==2) {
    fprintf(ctx->debug[ith], "%g\t%g\t%g\n", r.org[0], r.org[1], r.org[2]) ;
  }

  memset(tmp_net, 0., sizeof(*tmp_net)*nbnet);
  memset(tmp_fup, 0., sizeof(*tmp_fup)*nbflx);
  memset(tmp_fdn, 0., sizeof(*tmp_fdn)*nbflx);
  memset(tmp_abs, 0., sizeof(*tmp_abs)*nbabs);
  if (ctx->direct_diffuse) {
    memset(tmp_dir, 0., sizeof(*tmp_dir)*nbflx);
    memset(tmp_difdn, 0., sizeof(*tmp_difdn)*nbflx);
    memset(tmp_difup, 0., sizeof(*tmp_difup)*nbflx);
  }
  if (maxsy>0) {
    memset(tmp_fup_alb, 0., sizeof(*tmp_fup_alb)*nbsyf);
    memset(tmp_fdn_alb, 0., sizeof(*tmp_fdn_alb)*nbsyf);
    memset(tmp_abs_alb, 0., sizeof(*tmp_abs_alb)*nbsya);
  }

  results.tmp_net = tmp_net;
  results.tmp_fup = tmp_fup;
  results.tmp_fdn = tmp_fdn;
  results.tmp_abs = tmp_abs;
  if (ctx->direct_diffuse) {
    results.tmp_dir = tmp_dir;
    results.tmp_difdn = tmp_difdn;
    results.tmp_difup = tmp_difup;
  }
  if (maxsy>0) {
    results.tmp_fup_alb = tmp_fup_alb;
    results.tmp_fdn_alb = tmp_fdn_alb;
    results.tmp_abs_alb = tmp_abs_alb;
  }

  c_beg_path = clock();
  t_beg_path = time(NULL);
  res = path(rng, ctx, &r, &results);
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: could not compute path %lu\n",FUNC_NAME, real_num_all);
    goto error;
  }
  c_end_path = clock();
  t_end_path = time(NULL);

  accum(results.path_time, -1, 1000*difftime(t_end_path, t_beg_path));
  /*accum(results.path_time, -1, (c_end_path-c_beg_path)*1000/CLOCKS_PER_SEC);*/

  nrefs = r.ph.is_reflected < maxsy? r.ph.is_reflected+1 : maxsy;
  ibd = ctx->write_spectral*r.ph.ibd;
  for (iz=0; iz<nbnet; iz++) { /* for each level */
    for (i=0; i<nbx; i++) {
      for (j=0; j<nby; j++) {
        k = iz*nbx*nby + j*nbx + i;
        accum(results.fup, ibd*nbflx + k, results.tmp_fup[k]);
        accum(results.fdn, ibd*nbflx + k, results.tmp_fdn[k]);
        if (ctx->direct_diffuse) {
          accum(results.dir, ibd*nbflx + k, results.tmp_dir[k]);
          accum(results.difdn, ibd*nbflx + k, results.tmp_difdn[k]);
          accum(results.difup, ibd*nbflx + k, results.tmp_difup[k]);
        }
      }
    }
    accum(results.net, ibd*nbnet + iz, results.tmp_net[iz]);
    if (maxsy>0) {
      for (i=0; i<nrefs; i++) { /* for each nb ref */
        izi = iz*maxsy + i;
        accum(results.fdn_alb, ibd*nbsyf + izi, results.tmp_fdn_alb[izi]);
        accum(results.fup_alb, ibd*nbsyf + izi, results.tmp_fup_alb[izi]);
        for (j=0;j<nrefs;j++) {
          izj = iz*maxsy + j;
          k = ibd*(nbnet*maxsy*maxsy) + iz*(maxsy*maxsy) + i*maxsy + j;
          accum(results.fdn_alb_cov, k,
              results.tmp_fdn_alb[izi]*results.tmp_fdn_alb[izj]);
          accum(results.fup_alb_cov, k,
              results.tmp_fup_alb[izi]*results.tmp_fup_alb[izj]);
          /*if (izi==izj) printf("iz %3i i %2i cov_fdn %f fdni %f fdnj %f \n",
              iz, i, results.fdn_alb_cov[k], results.fdn_alb[izi], results.fdn_alb[izj]);*/
        }
      }
    }
  }
  for (iz=0; iz<nbabs; iz++) { /* for each level */
    accum(results.abs, iz, results.tmp_abs[iz]);
    if (maxsy>0) {
      for (i=0; i<nrefs; i++) {
        izi = iz*maxsy + i;
        accum(results.abs_alb, ibd*nbsya + izi, results.tmp_abs_alb[izi]);
        for (j=0;j<nrefs;j++) {
          izj = iz*maxsy + j;
          k = ibd*(nbabs*maxsy*maxsy) + iz*(maxsy*maxsy) + i*maxsy + j;
          accum(results.abs_alb_cov, k,
              results.tmp_abs_alb[izi]*results.tmp_abs_alb[izj]);
        }
      }
    }
  }

  accum(results.transm, -1, results.tmp_transm);
  accum(results.albedo, -1, results.tmp_albedo);

exit:
  c_end_real = clock();
  t_end_real = time(NULL);
  accum(results.real_time, -1, 1000*difftime(t_end_real, t_beg_real));
  /*accum(results.real_time, -1, (c_end_real-c_beg_real)*1000/CLOCKS_PER_SEC);*/
  if (ctx->print){fflush(ctx->debug[ith]); fclose(ctx->debug[ith]); ctx->debug[ith]=NULL;}
  return res;
error : 
  accum(results.failures, -1, 1);
  if ((ctx->print==1)) {
    char file_bugged[70]; 
    sprintf(file_bugged,"bug-%09lu.out", real_num_all); 
    rename(file_debug,file_bugged); 
  }
  goto exit;
}
