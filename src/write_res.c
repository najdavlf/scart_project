#include "main.h"
#include "conf.h"
#include "scart_context.h"
#include "netCDF.h"
#include <time.h>
#include <string.h>
#include <netcdf.h>
#include <rsys/logger.h>

#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); res = RES_BAD_OP; goto error;}
#define WRITE_ATT(name, format, value) { \
  sprintf(tmp, format, value);           \
  res = write_att(ncid, name, tmp);      \
  if (res != RES_OK) {                   \
    printf("error: could not write attribute %s\n", name); \
    goto error;                                            \
  }                                      \
}
  
res_T
write_att(const int ncid, const char* name, const char* value) {
  res_T res = RES_OK;
  int retval; 
  if ((retval = nc_put_att_text(ncid, NC_GLOBAL, name, strlen(value), value))) ERR(retval);

error:
  return res;
};

res_T
write_description(struct context * ctx, const int ncid) {
  res_T res = RES_OK;
  char tmp[1000];
  time_t tm;
  time(&tm);

  WRITE_ATT("nbpaths",                    "%lu", ctx->nbpaths);
  WRITE_ATT("nblv",                       "%lu", ctx->atm_dat.nblv);
  WRITE_ATT("nbx",                        "%lu", ctx->scn_dat.nbx);
  WRITE_ATT("nby",                        "%lu", ctx->scn_dat.nby);
  WRITE_ATT("liq_op_file",                "%s ", ctx->liq_op_dat.cop_file);
  WRITE_ATT("ice_op_file",                "%s ", ctx->ice_op_dat.cop_file);
  WRITE_ATT("atm_file",                   "%s ", ctx->atm_dat.atm_file);
  WRITE_ATT("les_file",                   "%s ", ctx->les_dat.les_file);
  WRITE_ATT("sun_file",                   "%s ", ctx->sun_dat.sun_file);
  WRITE_ATT("cloud",                      "%i ", ctx->col_dat.cloud);
  WRITE_ATT("atmosphere",                 "%i ", ctx->col_dat.atmosphere);
  WRITE_ATT("grd_alb",                    "%f ", ctx->grd_dat.albedo);
  WRITE_ATT("liq_g_assym",                "%f ", ctx->liq_op_dat.g);
  WRITE_ATT("liq_ssa",                    "%f ", ctx->liq_op_dat.ssa);
  WRITE_ATT("ice_g_assym",                "%f ", ctx->ice_op_dat.g);
  WRITE_ATT("ice_ssa",                    "%f ", ctx->ice_op_dat.ssa);
  WRITE_ATT("theta",                      "%f ", ctx->the[0]);
  WRITE_ATT("phi",                        "%f ", ctx->phi[0]);
  WRITE_ATT("latitude",                   "%f ", ctx->latitude);
  WRITE_ATT("day",                        "%f ", ctx->day);
  WRITE_ATT("hour",                       "%f ", ctx->hour);
  WRITE_ATT("cut_nose",                   "%f ", ctx->cut_nose);
  WRITE_ATT("monochromatic",              "%f ", ctx->mono_nu);
  WRITE_ATT("print",                      "%i ", ctx->print);
  WRITE_ATT("use_mie_pf",                 "%i ", ctx->conf->use_mie_pf);
  WRITE_ATT("do_two_stream",              "%i ", ctx->two_stream);
  WRITE_ATT("do_direct_diffuse",          "%i ", ctx->direct_diffuse);
  WRITE_ATT("do_delta_scaling",           "%i ", ctx->delta_scaling);
  WRITE_ATT("do_russian_reflection",      "%i ", ctx->russian_ref);
  WRITE_ATT("do_spectral_integration",    "%i ", ctx->spectral_integration);
  WRITE_ATT("do_time_integration",        "%i ", ctx->sun_dat.do_time_int);
  WRITE_ATT("do_latitudinal_integration", "%i ", ctx->sun_dat.do_lat_int);
  WRITE_ATT("do_direct_in_solid_angle",   "%i ", ctx->do_direct_in_solid_angle);
  WRITE_ATT("direct_solid_angle",         "%f ", ctx->direct_solid_angle);
  WRITE_ATT("zmin",                       "%f ", ctx->scn_dat.zmin);
  WRITE_ATT("zmax",                       "%f ", ctx->scn_dat.zmax);
  WRITE_ATT("ica",                        "%i ", ctx->ica);
  WRITE_ATT("map_ica",                    "%i ", ctx->map_ica);
  WRITE_ATT("z_reg",                      "%i ", ctx->les_dat.z_reg);
  WRITE_ATT("do_ad",                      "%i ", ctx->do_ad);
  WRITE_ATT("datetime",                   "%s ", ctime(&tm));

error:
  return res;
}

res_T
write2ncdf(struct context * ctx, char * write_file, struct results results, size_t nbpaths) 
{
  res_T res = RES_OK;
  int retval;
  char info[10000];
  char wm2[] = "W.m-2";
  char wm22[] = "(W.m-2)2";
  char wm2sr[] = "W.m-2.sr-1";

  size_t i, j, k, ke, iz, izi, iw;

  const size_t nbbds = ctx->nbbds; /* number of spectral bands */
  const size_t nbres = ctx->nbres; /* up or  down, no LES BBOX */
  const size_t nbflx = ctx->nbflx; /* up or  down, with LES BBOX */
  const size_t nbres_h = nbres/nbbds;    /* levels, no LES BBOX */
  const size_t nbflx_h = nbflx/nbbds;    /* levels, with LES BBOX */
  const size_t nbabs = ctx->nbabs;
  const size_t nbnet = ctx->nbnet;
  const size_t nbmsi = ctx->nbmsi;
  const size_t maxsy = ctx->max_reflections_symb;
  const size_t nbsya = nbabs*maxsy;  /* length of symbolic vector */
  const size_t nbsyf = nbnet*maxsy;  /* length of symbolic vector */
  const size_t nbsya2 = nbabs*maxsy*maxsy;
  const size_t nbsyf2 = nbnet*maxsy*maxsy;

  int ncid;
  int x_dimid, y_dimid, z_dimid;
  int zabs_dimid, c_dimid;
  int w_dimid, wb_dimid; /* wavelengths mid and boundary */
  int alb_dimid, alb_dimid2;
  int x_varid, y_varid, z_varid, zabs_varid;
  int w_varid, wb_varid; /* wavelengths mid and boundary */

  int csza_varid;
  int msi_varid, net_varid, abs_varid, dn_varid, up_varid;
  int difdn_varid, difup_varid, dirdn_varid;
  int Smsi_varid, Snet_varid, Sabs_varid, Sdn_varid, Sup_varid;
  int Sdifdn_varid, Sdifup_varid, Sdirdn_varid;
  int fup_alb_varid, fdn_alb_varid, abs_alb_varid; /* symbolic albedo */
  int Sfup_alb_varid, Sfdn_alb_varid, Sabs_alb_varid; /* symbolic albedo */
  int sfup_alb_varid, sfdn_alb_varid, sabs_alb_varid; /* symbolic albedo */
  int Ssfup_alb_varid, Ssfdn_alb_varid, Ssabs_alb_varid; /* symbolic albedo */
  int alb_varid;

  int alb_tab[maxsy];
  double x_tab[ctx->scn_dat.nbx];
  double y_tab[ctx->scn_dat.nby];
  double z_tab[ctx->atm_dat.nblv];
  double w_tab[nbbds], wb_tab[nbbds+1];
  double zabs_tab[nbabs/nbbds];
  double theta=*(ctx->the);
  double fact;
  double dx[2];
  double dy[2];
  double dxy;

  
  if (ctx->do_ad) {
    x_tab[0] = ctx->les_dat.xmin>ctx->ad_coordinates[0][0]?ctx->les_dat.xmin:ctx->ad_coordinates[0][0];
    x_tab[1] = ctx->les_dat.xmax<ctx->ad_coordinates[0][1]?ctx->les_dat.xmax:ctx->ad_coordinates[0][1];
    y_tab[0] = ctx->les_dat.ymin>ctx->ad_coordinates[1][0]?ctx->les_dat.ymin:ctx->ad_coordinates[1][0];
    y_tab[1] = ctx->les_dat.ymax<ctx->ad_coordinates[1][1]?ctx->les_dat.ymax:ctx->ad_coordinates[1][1];

    dx[0] = (x_tab[0] - ctx->les_dat.xmin) + (ctx->les_dat.xmax - x_tab[1]);
    dx[1] = (x_tab[1] - x_tab[0]);
    dy[0] = (y_tab[0] - ctx->les_dat.ymin) + (ctx->les_dat.ymax - y_tab[1]);
    dy[1] = (y_tab[1] - y_tab[0]);
    dxy = (dx[0]+dx[1])*(dy[0]+dy[1]);

  } else {
    for (i=0;i<ctx->scn_dat.nbx;i++)  
      x_tab[i]=((float)i+.5)*(ctx->les_dat.xmax-ctx->les_dat.xmin)/ctx->scn_dat.nbx ;
    for (i=0;i<ctx->scn_dat.nby;i++)  
      y_tab[i]=((float)i+.5)*(ctx->les_dat.ymax-ctx->les_dat.ymin)/ctx->scn_dat.nby ;
  }
  for (i=0;i<ctx->atm_dat.nblv;i++) {
    z_tab[i]=ctx->scn_dat.z_surfaces[i]; 
    if (i>0) {
      zabs_tab[i]=(z_tab[i-1] + z_tab[i])/2.;
    }
  }
  zabs_tab[0] = -1;
  zabs_tab[nbabs/nbbds-1] = zabs_tab[nbabs/nbbds-2]+10.;
  if (nbbds>1) {
    for (iw=0;iw<nbbds+1;iw++) {
      wb_tab[iw] = ctx->atm_dat.nu_tab[iw];
      if (i>0) w_tab[iw-1] = (wb_tab[iw]+wb_tab[iw-1])/2.;
    }
  } else {
    w_tab[0] = ctx->mono_nu;
    wb_tab[0] = ctx->mono_nu;
    wb_tab[1] = ctx->mono_nu;
  }

  for (i=0;i<nbres;i++) { 
    /* ei = sum_n=0^nbpaths_in_i(ei^n) / nbpaths to 
       ei = sum_n=0^nbpaths_in_i(ei^n) / nbpaths_in_i
       where 
       nbpaths_in_i = nbpaths * area_in_i / area 
       if area = Ni * area_in_i (same area for all subdomains) then 
       nbpaths_in_i = nbpaths / Ni 
       1/nbpaths_in_i = Ni / nbpaths
       or 1/nbpaths_in_i = area / (area_in_i * nbpaths)
    */
    if (ctx->do_ad) {
      int ix = i%(ctx->scn_dat.nbx*ctx->scn_dat.nby)%ctx->scn_dat.nby; /* 0=0 1=1 2=0 3=1 */
      int iy = floor(i%(ctx->scn_dat.nbx*ctx->scn_dat.nby)/ctx->scn_dat.nbx); /* 0=0 1=0 2=1 3=1 */
      fact = dxy/(dx[ix]*dy[iy]);
    } else {
      fact = ctx->scn_dat.nbx*ctx->scn_dat.nby;
    }
    results.fdn->w.t[i]  = results.fdn->w.t[i] *fact;
    results.fup->w.t[i]  = results.fup->w.t[i] *fact;
    results.fdn->w2.t[i] = results.fdn->w2.t[i]*fact;
    results.fup->w2.t[i] = results.fup->w2.t[i]*fact;
    if (ctx->direct_diffuse) {
      results.dir->w.t[i]= results.dir->w.t[i]*fact;
      results.dir->w2.t[i]= results.dir->w2.t[i]*fact;
      results.difdn->w.t[i] = results.difdn->w.t[i]*fact;
      results.difup->w.t[i] = results.difup->w.t[i]*fact;
      results.difdn->w2.t[i] = results.difdn->w2.t[i]*fact;
      results.difup->w2.t[i] = results.difup->w2.t[i]*fact;
    }
  }
  if (ctx->do_ad) {
    fact = dxy/(dx[1]*dy[1]); /* inverse fraction of domain occupied by the AD */
    for (i=0;i<nbnet;i++) {
      results.net->w.t [i] = results.net->w.t [i] * fact;
      results.net->w2.t[i] = results.net->w2.t[i] * fact;
    }
    for (i=0;i<nbabs;i++) {
      results.abs->w.t [i] = results.abs->w.t [i] * fact;
      results.abs->w2.t[i] = results.abs->w2.t[i] * fact;
    }
  }
  if (ctx->do_ad && maxsy) {
    fact = dxy/(dx[1]*dy[1]); /* inverse fraction of domain occupied by the AD */
    for (i=0;i<nbsyf;i++) {
      results.fdn_alb->w.t [i] = results.fdn_alb->w.t [i] * fact;
      results.fdn_alb->w2.t[i] = results.fdn_alb->w2.t[i] * fact;
      results.fup_alb->w.t [i] = results.fup_alb->w.t [i] * fact;
      results.fup_alb->w2.t[i] = results.fup_alb->w2.t[i] * fact;
    }
    for (i=0;i<nbsyf2;i++) {
      results.fdn_alb_cov->w.t [i] = results.fdn_alb_cov->w.t [i] * fact;
      results.fdn_alb_cov->w2.t[i] = results.fdn_alb_cov->w2.t[i] * fact;
      results.fup_alb_cov->w.t [i] = results.fup_alb_cov->w.t [i] * fact;
      results.fup_alb_cov->w2.t[i] = results.fup_alb_cov->w2.t[i] * fact;
    }
    for (i=0;i<nbsya;i++) {
      results.abs_alb->w.t [i] = results.abs_alb->w.t [i] * fact;
      results.abs_alb->w2.t[i] = results.abs_alb->w2.t[i] * fact;
    }
    for (i=0;i<nbsya;i++) {
      results.abs_alb_cov->w.t [i] = results.abs_alb_cov->w.t [i] * fact;
      results.abs_alb_cov->w2.t[i] = results.abs_alb_cov->w2.t[i] * fact;
    }
  }

  for (i=0;i<maxsy;i++) {
    alb_tab[i] = i;
  }

  /* Create and define dimensions and variable */
  if ((retval = nc_create(write_file, NC_CLOBBER, &ncid))) ERR(retval);

  res=write_description(ctx,ncid);
  if (res != RES_OK) {
    goto error;
  }

  if ((retval = nc_def_dim(ncid, "W_E_direction",   ctx->scn_dat.nbx, &x_dimid))) ERR(retval);
  if ((retval = nc_def_dim(ncid, "S_N_direction",   ctx->scn_dat.nby, &y_dimid))) ERR(retval);
  if ((retval = nc_def_dim(ncid, "vertical_levels", ctx->atm_dat.nblv, &z_dimid))) ERR(retval);
  if ((retval = nc_def_dim(ncid, "vertical_layers", nbabs/nbbds, &zabs_dimid))) ERR(retval);
  if ((retval = nc_def_dim(ncid, "column",              1, &c_dimid))) ERR(retval);

  if (maxsy) {
    if ((retval = nc_def_dim(ncid, "nb_reflection",  maxsy, &alb_dimid))) ERR(retval);
    if ((retval = nc_def_dim(ncid, "nb_reflection2", maxsy, &alb_dimid2))) ERR(retval);
  }

  if ((retval = nc_def_dim(ncid, "wavelength_mid",  nbbds, &w_dimid))) ERR(retval);
  if ((retval = nc_def_dim(ncid, "wavelength_bds",  nbbds+1, &wb_dimid))) ERR(retval);

  if ((retval = nc_def_var(ncid, "cos_solar_zenith_angle", NC_DOUBLE, 1, &c_dimid, &csza_varid))) ERR(retval);

  if ((retval = nc_def_var(ncid, "wavelength_mid",  NC_DOUBLE, 1, &w_dimid,  &w_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "wavelength_bds",  NC_DOUBLE, 1, &wb_dimid, &wb_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "W_E_direction",   NC_DOUBLE, 1, &x_dimid, &x_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "S_N_direction",   NC_DOUBLE, 1, &y_dimid, &y_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "vertical_levels", NC_DOUBLE, 1, &z_dimid, &z_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "vertical_layers", NC_DOUBLE, 1, &zabs_dimid, &zabs_varid))) ERR(retval);

  if ((retval = nc_put_att_text(ncid, x_varid, "axis", strlen("X"),"X"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, y_varid, "axis", strlen("Y"),"Y"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, z_varid, "axis", strlen("Z"),"Z"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, zabs_varid, "axis", strlen("Z"),"Z"))) ERR(retval);

  if ((retval = nc_put_att_text(ncid, w_varid,  "units", strlen("nanometer"),"nanometer"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, wb_varid, "units", strlen("nanometer"),"nanometer"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, x_varid, "units", strlen("km"),"km"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, y_varid, "units", strlen("km"),"km"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, z_varid, "units", strlen("km"),"km"))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, zabs_varid, "units", strlen("km"),"km"))) ERR(retval);

  if (maxsy) {
    if ((retval = nc_def_var(ncid, "number_reflections", NC_INT, 1, &alb_dimid, &alb_varid))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, alb_varid, "units", strlen("-"),"-"))) ERR(retval);
  }
  
  int nbdims=3;
  int dimids2d[3] = {c_dimid, w_dimid, zabs_dimid};
  if ((retval = nc_def_var(ncid, "absorbed_sw",     NC_DOUBLE, nbdims, dimids2d, &abs_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "std_absorbed_sw", NC_DOUBLE, nbdims, dimids2d, &Sabs_varid))) ERR(retval);

  dimids2d[nbdims-1] = z_dimid;
  if ((retval = nc_def_var(ncid, "flux_net_sw",     NC_DOUBLE, nbdims, dimids2d, &net_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "std_flux_net_sw", NC_DOUBLE, nbdims, dimids2d, &Snet_varid))) ERR(retval);

  nbdims = nbdims + 2;
  int dimids4d[5] = {c_dimid, w_dimid, z_dimid, y_dimid, x_dimid};
  if ((retval = nc_def_var(ncid, "flux_dn_sw",     NC_DOUBLE, nbdims, dimids4d, &dn_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "flux_up_sw",     NC_DOUBLE, nbdims, dimids4d, &up_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "std_flux_dn_sw", NC_DOUBLE, nbdims, dimids4d, &Sdn_varid))) ERR(retval);
  if ((retval = nc_def_var(ncid, "std_flux_up_sw", NC_DOUBLE, nbdims, dimids4d, &Sup_varid))) ERR(retval);

  if ((retval = nc_put_att_text(ncid, abs_varid,  "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, Sabs_varid, "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, net_varid,  "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, Snet_varid, "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, dn_varid,  "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, up_varid,  "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, Sdn_varid, "units", strlen(wm2), wm2))) ERR(retval);
  if ((retval = nc_put_att_text(ncid, Sup_varid, "units", strlen(wm2), wm2))) ERR(retval);

  if (ctx->direct_diffuse) {
    if ((retval = nc_def_var(ncid, "flux_dn_direct_sw",      NC_DOUBLE, nbdims, dimids4d, &dirdn_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "flux_dn_diffuse_sw",     NC_DOUBLE, nbdims, dimids4d, &difdn_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "flux_up_diffuse_sw",     NC_DOUBLE, nbdims, dimids4d, &difup_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_flux_dn_direct_sw",  NC_DOUBLE, nbdims, dimids4d, &Sdirdn_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_flux_dn_diffuse_sw", NC_DOUBLE, nbdims, dimids4d, &Sdifdn_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_flux_up_diffuse_sw", NC_DOUBLE, nbdims, dimids4d, &Sdifup_varid))) ERR(retval);

    if ((retval = nc_put_att_text(ncid, dirdn_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, difdn_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, difup_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sdirdn_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sdifdn_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sdifup_varid, "units", strlen(wm2), wm2))) ERR(retval);
  }

  if (ctx->do_msi) {
    nbdims=4;
    int dimids3d[4] = {c_dimid, w_dimid, y_dimid, x_dimid};
    if ((retval = nc_def_var(ncid, "rad_msi_sw", NC_DOUBLE,     nbdims, dimids3d, &msi_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_rad_msi_sw", NC_DOUBLE, nbdims, dimids3d, &Smsi_varid))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, msi_varid, "units", strlen(wm2sr), wm2sr))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Smsi_varid, "units", strlen(wm2sr), wm2sr))) ERR(retval);
  }
  if (maxsy) {
    nbdims = 4;
    int dimids3d2[4] = {c_dimid, w_dimid, z_dimid, alb_dimid2};
    if ((retval = nc_def_var(ncid, "symalb_flux_dn_sw",     NC_DOUBLE, nbdims, dimids3d2, &fdn_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "symalb_flux_up_sw",      NC_DOUBLE, nbdims, dimids3d2, &fup_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_symalb_flux_dn_sw", NC_DOUBLE, nbdims, dimids3d2, &Sfdn_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_symalb_flux_up_sw",  NC_DOUBLE, nbdims, dimids3d2, &Sfup_alb_varid))) ERR(retval);

    dimids3d2[nbdims-2] = zabs_dimid;
    if ((retval = nc_def_var(ncid, "symalb_absorbed_sw",         NC_DOUBLE, nbdims, dimids3d2, &abs_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_symalb_absorbed_sw",     NC_DOUBLE, nbdims, dimids3d2, &Sabs_alb_varid))) ERR(retval);
    
    nbdims = 5;
    int dimids4d2[5] = {c_dimid, w_dimid, z_dimid, alb_dimid, alb_dimid2};
    if ((retval = nc_def_var(ncid, "cov_symalb_flux_dn_sw",     NC_DOUBLE, nbdims, dimids4d2, &sfdn_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "cov_symalb_flux_up_sw",      NC_DOUBLE, nbdims, dimids4d2, &sfup_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_cov_symalb_flux_dn_sw", NC_DOUBLE, nbdims, dimids4d2, &Ssfdn_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_cov_symalb_flux_up_sw",  NC_DOUBLE, nbdims, dimids4d2, &Ssfup_alb_varid))) ERR(retval);

    dimids4d2[nbdims-3] = zabs_dimid;
    if ((retval = nc_def_var(ncid, "cov_symalb_absorbed_sw",         NC_DOUBLE, nbdims, dimids4d2, &sabs_alb_varid))) ERR(retval);
    if ((retval = nc_def_var(ncid, "std_cov_symalb_absorbed_sw",     NC_DOUBLE, nbdims, dimids4d2, &Ssabs_alb_varid))) ERR(retval);

    if ((retval = nc_put_att_text(ncid, fdn_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, fup_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, abs_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sfdn_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sfup_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Sabs_alb_varid, "units", strlen(wm2), wm2))) ERR(retval);
    if ((retval = nc_put_att_text(ncid,  sfdn_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
    if ((retval = nc_put_att_text(ncid,  sfup_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
    if ((retval = nc_put_att_text(ncid,  sabs_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Ssfdn_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Ssfup_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
    if ((retval = nc_put_att_text(ncid, Ssabs_alb_varid, "units", strlen(wm22), wm22))) ERR(retval);
  }

  if ((retval = nc_enddef(ncid)))
     ERR(retval);
  /* END DEFINITION MODE */

  /* Fill values */
  double costheta=cos(theta/180*PI);
  if ((retval = nc_put_var_double(ncid, csza_varid, &costheta))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, w_varid, w_tab))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, wb_varid, wb_tab))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, x_varid, x_tab))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, y_varid, y_tab))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, z_varid, z_tab))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, zabs_varid, zabs_tab))) ERR(retval);

  if ((retval = nc_put_var_double(ncid, net_varid,  results.net->w.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, abs_varid,  results.abs->w.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, up_varid,   results.fup->w.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, dn_varid,   results.fdn->w.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, Snet_varid, results.net->w2.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, Sabs_varid, results.abs->w2.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, Sup_varid,  results.fup->w2.t))) ERR(retval);
  if ((retval = nc_put_var_double(ncid, Sdn_varid,  results.fdn->w2.t))) ERR(retval);
  if (ctx->direct_diffuse) {
    if ((retval = nc_put_var_double(ncid, dirdn_varid,  results.dir->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, difdn_varid,  results.difdn->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, difup_varid,  results.difup->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sdirdn_varid, results.dir->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sdifdn_varid, results.difdn->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sdifup_varid, results.difup->w2.t))) ERR(retval);
  }
  if (ctx->do_msi) {
    if ((retval = nc_put_var_double(ncid, msi_varid,  results.msi->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Smsi_varid, results.msi->w2.t))) ERR(retval);
  }
  if (maxsy) {
    if ((retval = nc_put_var_int(ncid, alb_varid, alb_tab))) ERR(retval) ;
    if ((retval = nc_put_var_double(ncid, fdn_alb_varid, results.fdn_alb->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, fup_alb_varid, results.fup_alb->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, abs_alb_varid, results.abs_alb->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sfdn_alb_varid, results.fdn_alb->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sfup_alb_varid, results.fup_alb->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Sabs_alb_varid, results.abs_alb->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid,  sfdn_alb_varid,  results.fdn_alb_cov->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid,  sfup_alb_varid,  results.fup_alb_cov->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid,  sabs_alb_varid,  results.abs_alb_cov->w.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Ssfdn_alb_varid, results.fdn_alb_cov->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Ssfup_alb_varid, results.fup_alb_cov->w2.t))) ERR(retval);
    if ((retval = nc_put_var_double(ncid, Ssabs_alb_varid, results.abs_alb_cov->w2.t))) ERR(retval);
  }
  exit:
    /* Close file. */
    if (nc_close(ncid)) printf("Error closing netCDF file\n");
    return res;
  error:
    goto exit;
}

res_T
write_res(struct context * ctx, 
          struct results results,
          size_t nbpaths, 
          double tolerance,
          char*write_file)
{
  res_T res = RES_OK;
  size_t NF=results.failures->w.s, max_failures=nbpaths*tolerance;

  if (NF > max_failures) {
    logger_print(&ctx->logger,LOG_WARNING,
        " Integration failed with %lu failures / %lu sampled paths (%f %%)\n\n",  NF, nbpaths,(float)NF/(float)nbpaths*100.);
  } else {
    logger_print(&ctx->logger,LOG_OUTPUT,
        " Integration succeeded with %lu failures / %lu sampled paths (%f %%)\n", NF, nbpaths,(float)NF/(float)nbpaths*100.);

    /* nbcount = 2 * nbpixels * nbsurfs + 2 */
    logger_print(&ctx->logger, LOG_OUTPUT,
        " Average time per path : %f +- %f ms\n", results.path_time->w.s, results.path_time->w2.s);
    logger_print(&ctx->logger, LOG_OUTPUT,
        " Average time per realization : %f +- %f ms \n", results.real_time->w.s, results.real_time->w2.s);
    logger_print(&ctx->logger, LOG_OUTPUT,
        " Surface down / TOA down : %f +- %f \n", results.transm->w.s, results.transm->w2.s);
    logger_print(&ctx->logger, LOG_OUTPUT,
        " TOA up / TOA down       : %f +- %f\n", results.albedo->w.s, results.albedo->w2.s);
    logger_print(&ctx->logger, LOG_OUTPUT,
        " <L> = %f +- %f\t(4V/S = %f)\n", results.path_len->w.s,  results.path_len->w2.s, 4*ctx->ztoa);
    if (write_file) {
      res = write2ncdf(ctx, write_file, results, nbpaths);
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not write to netcdf file %s\n",FUNC_NAME,write_file);
      }
    }
  }
  
exit:
  return res;
error : 
  goto exit;
}
