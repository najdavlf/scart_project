#ifndef HANDLE_VOLUMIC_COLLISION_H
#define HANDLE_VOLUMIC_COLLISION_H
#include "utils.h" 

res_T
sample_scatterer(struct ssp_rng * rng,
                 struct context * ctx,
                 struct collision * col) 
{
  res_T res = RES_OK;
  int i ;
  double eps = ssp_rng_canonical(rng) ;
  double cumratio[ctx->col_dat.nbcolliders+1] ;

  cumratio[0] = 0 ;
  for (i=0;i<ctx->col_dat.nbcolliders;i++){
    cumratio[i+1] = cumratio[i] + col->ks[i]/col->ks_tot ;
  }
  
  col->ind_collider = find_index(cumratio, ctx->col_dat.nbcolliders+1, eps) ; 

  return res ;
}

res_T
sample_collider(struct ssp_rng * rng,
                struct context * ctx,
                struct collision * col) 
{
  res_T res = RES_OK;
  int i ;
  double eps = ssp_rng_canonical(rng) ;
  double cumratio[ctx->col_dat.nbcolliders+1] ;

  cumratio[0] = 0 ;
  for (i=0;i<ctx->col_dat.nbcolliders;i++){
    cumratio[i+1] = cumratio[i] + (col->ks[i]+col->ka[i])/col->ke_tot ;
  }
  
  col->ind_collider = find_index(cumratio, ctx->col_dat.nbcolliders+1, eps) ; 

  return res ;
}

res_T
handle_periodicity(struct context * ctx,
                   struct collision * col, 
                   struct ray * r,
                   struct event *ev,
                   int * do_collision) {
  res_T res = RES_OK;
  /* remove_periodicity ? 
   * if cloud and first collision => register Ifirst, Jfirst 
   * if cloud and not first collision => check 
   *   if I,J == Ifirst,Jfirst => continue
   *   else => ignore event 
   * if not cloud => continue */
  int I,J;
  if (ctx->remove_periodicity) {
    I = get_mod_ind(ev->location[0],
        ctx->les_dat.xmax-ctx->les_dat.xmin,ctx->les_dat.nbx);
    J = get_mod_ind(ev->location[1],
        ctx->les_dat.ymax-ctx->les_dat.ymin,ctx->les_dat.nby);
    if ((col->ind_collider==0) && (!r->ph.is_cloudy)) { /* first cloud */
      r->ph.is_cloudy = 1;
      r->ph.Ifirst = I;
      r->ph.Jfirst = J;
      *do_collision = 1;
    } else if ((col->ind_collider==0) && (r->ph.is_cloudy)) { 
      if ((r->ph.Ifirst==I) && (r->ph.Jfirst==J)) {
        /* not first cloud, same domain */
        *do_collision = 1;
      } else {
        /* not first cloud, other domain */
        *do_collision = 0;
      }
    }
  } else {
    *do_collision = 1;
  }
error :
  return res ;
}

res_T
scatter(struct ssp_rng * rng,
        struct context * ctx,
        struct collision * col,
        struct ray * r,
        int*has_scattered) 
{
  res_T res = RES_OK;
  double sample[4] ; 
  double ndir[3] ;
  double odir[3] ;
  const int ith = omp_get_thread_num();
  const type_collider type_col = ctx->col_dat.types[col->ind_collider];
  const int nb_scatt = r->ph.is_scattered;
  struct cop_data * cd;

  switch (type_col) {
    case DROPLET:
      cd = &ctx->liq_op_dat;
      break;
    case ICE:
      cd = &ctx->ice_op_dat;
      break;
    case AIR:
      cd = NULL;
      break;
  }

  *has_scattered=0;

  /* old direction (incoming) */
  d3_set(odir, r->dir);
  d3_normalize(odir,odir);

  /* sample new direction */
  ctx->col_dat.colliders_pf[col->ind_collider](rng, odir, sample, cd);

  /* new direction (outgoing) */
  d3_set(ndir, sample);
  d3_normalize(ndir,ndir) ;

  if ((ctx->cut_nose > 0.) && (f3_dot(r->dir,ndir) > cos(ctx->cut_nose))) {
    /* this is a null collision */
  } else {
    /* if two-stream : ramener sur up / down directions */ 
    if (ctx->two_stream) {
      if (ctx->print==1) {
        fprintf(ctx->debug[ith],
            "d3_dot %g odir[2] %g ndir[2] %g \n",
            d3_dot(odir,ndir),odir[2],ndir[2]);
        fflush(ctx->debug[ith]);
      }
      res = do_two_stream_approx(odir,ndir,ctx->theta_diffus_down,ctx->theta_diffus_up);
    }

    d3_set(r->dir, ndir); 
    d3_normalize(r->dir,r->dir) ;

    if (ctx->print==5) {
      fprintf(ctx->debug[ith],
          "%u %i %i %g %g %g %g scattering\n",
          r->ph.id,r->ph.is_scattered,r->ph.is_reflected, SPLIT3(odir),r->ph.w);
      fflush(ctx->debug[ith]);
    }
    r->ph.is_scattered+=1; /* count all scattering events */
  }
  if (r->ph.is_scattered > nb_scatt) *has_scattered=1;
  return res ;
}

res_T
do_two_stream_approx(double odir[3], double ndir[3], double th_dn, double th_up) {
  INIT_RES;
  /* cos_theta_diffus = 0.5 ; */ /* This value is used in ecRad, and comes from Zdunkowski "PIFM" 
                            * Zdunkowski et al., 1980; Contributions to Atmospheric Physics 53, 147-66 */ 

  /* theta_diffus = 51.*PI/180. */ /* This value is from shonk and hogan 2013 */ ;
  /* theta_diffus = atan(PI/2.) */ /* This value is from schaefer et al. 2016 */ ;
  double cos_theta_diffus_dn=cos(th_dn),sin_theta_diffus_dn=sin(th_dn); 
  double cos_theta_diffus_up=cos(th_up),sin_theta_diffus_up=sin(th_up);

  double sin_theta_in = cos2sin(ndir[2]) ;
  double cos_phi_in,sin_phi_in ;
  if (fabs(sin_theta_in) < 1.e-6) {
    cos_phi_in=1 ; 
    sin_phi_in=0 ;
  } else {
   cos_phi_in = ndir[0] / sin_theta_in ;
   sin_phi_in = ndir[1] / sin_theta_in ;
  }

  double up[3] = {cos_phi_in*sin_theta_diffus_up, sin_phi_in*sin_theta_diffus_up, cos_theta_diffus_up};
  double dn[3] = {cos_phi_in*sin_theta_diffus_dn, sin_phi_in*sin_theta_diffus_dn, cos_theta_diffus_dn};
 
  /* old behaviour */
  /* odir.ndir > 0 => same dir ; < 0 => change dir */
  /* odir[2] > 0 => incoming upward ; < 0 => incoming downward */
  /*
  if ((d3_dot(odir,ndir)>0) && (odir[2] > 0)) {  continue vers le haut 
    d3_set(ndir,up);
  }
  else if ((d3_dot(odir,ndir)>0) && (odir[2] < 0)) { continue vers le bas 
    d3_set(ndir,dn);
  } 
  else if ((d3_dot(odir,ndir)<0) && (odir[2] > 0)) { diffusé vers le bas 
    d3_set(ndir,dn);
  }
  else if ((d3_dot(odir,ndir)<0) && (odir[2] < 0)) { diffusé vers le haut 
    d3_set(ndir,up);
  }
  else { 
    res = RES_BAD_OP ;
  }
  */

  /* new behaviour */
  /* ndir[2] > 0 => up */
  /* ndir[2] < 0 => dn */
  if (ndir[2] > 0 ) { /* vers le haut */
    d3_set(ndir,up) ;
  } 
  else if (ndir[2]<0) { /* vers le bas */
    d3_set(ndir,dn) ;
  }
  else {
    res=RES_BAD_OP ;
  }

  return res ;
}

res_T 
delta_eddington(double*g,double*ks,int mode) {
  res_T res = RES_OK;
  double vg = *g ;
  double vk = *ks ;
  double f=0 ;
  if (mode==1) {
    f = vg*vg ;   
  } else if (mode==2) {
    f = vg ; 
  } else if (mode==3) { 
    f = 0.6*vg*vg ; 
  }
  *ks = vk*(1.-f) ;
  *g  = (vg-f)/(1.-f) ;
  return res ;
}

#endif 
