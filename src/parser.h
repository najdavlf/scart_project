#ifndef PARSER_H
#define PARSER_H

#define CONFIG_ARG_MAX_BYTES 256
#include <rsys/logger.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
/*
typedef struct config_option config_option;
int remove_comments_and_blanks(config_option_t co) ;
int parse_line(config_option_t co) ;
*/

typedef struct config_option config_option ;
struct config_option {
    config_option * prev;
    char all[2*CONFIG_ARG_MAX_BYTES] ;
    char key[CONFIG_ARG_MAX_BYTES];
    char value[CONFIG_ARG_MAX_BYTES];
} ; 

int
remove_comments_and_blanks(config_option * co) {
  if ((co->all[0] == '#') || (co->all[0]=='/') || (co->all[0]=='\n')) {
    return 1;
  }
  else return 0 ; 
}
int 
parse_line(config_option * co) 
{
  char*config_line = &(co->all[0]) ;
  while(isspace(*config_line)) config_line++ ;
  if (((*config_line)=='#') || ((*config_line)=='/') || ((*config_line)=='\n')){
    return 1 ;
  } else {
    int i=0;
    while((!isspace(*config_line)) && ((*config_line!='='))){
      co->key[i] = *config_line ;
      i++ ; config_line++ ;
    } /* found key */
    while(isspace(*config_line) || (*config_line=='=')) config_line++ ;
    if (((*config_line)=='#') || ((*config_line)=='\n') ){
      return 1 ; /* value is commentend or missing */
    } else {
      i=0;
      while((!isspace(*config_line)) && ((*config_line!='\n')) && ((*config_line!='#')) ){
        co->value[i] = *config_line ;
        i++ ; config_line++ ;
      }
    }
    return 0;
  }
}

config_option * read_config_file(char* path) {
  FILE* fp;

  if ((fp = fopen(path, "r+")) == NULL) {
    perror("fopen()");
    return NULL;
  }
  
  config_option * last_co_addr = NULL;
  while(1) {
    config_option * co = NULL;
    if ((co = (config_option *)mem_calloc(1, sizeof(config_option))) == NULL)
      continue;
    co->prev = last_co_addr;
    fgets(co->all,2*CONFIG_ARG_MAX_BYTES,fp) ;	
    if (feof(fp)) {
      mem_rm(co);  break ;
    }
    if(remove_comments_and_blanks(co)) {
      mem_rm(co) ; continue ;
    } else {
      if (parse_line(co)) { 
        mem_rm(co) ; continue ;
      } else {
        last_co_addr = co;
      }
    }
  }
  fclose(fp);
  return last_co_addr;
}

#endif
