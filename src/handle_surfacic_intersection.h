#ifndef HANDLE_SURFACIC_INTERSECTION_H
#define HANDLE_SURFACIC_INTERSECTION_H

#include <star/ssp.h>
#include "omp.h"
                                                                                            
res_T 
reflect_spec(double norm[3], double dir[3])
{
  res_T res = RES_OK ;
  double vec[3];
  double tmp = -2.f * d3_dot(dir,norm) ;
  /*d3_mulf(vec,norm,tmp) ;*/
  vec[0] = norm[0]*tmp;
  vec[1] = norm[1]*tmp;
  vec[2] = norm[2]*tmp;
  d3_add(dir, dir, vec) ;
  d3_normalize(dir,dir) ;
  return res ;
}


res_T
reflect(struct ssp_rng * rng,
        struct context * ctx,
        struct ray * r,
        struct intersection * inte,
        int* has_reflected) 
{
  res_T res = RES_OK;
  const int nb_ref = r->ph.is_reflected;
  double eps = ssp_rng_canonical(rng);
  const double up[3] = {0.,0.,1.};
  double sample[3] ;
  double dir[3] ;

  *has_reflected=0;

  if (r->ph.w == 0) {
    res = RES_BAD_OP ;
    logger_print(&(ctx->logger),LOG_ERROR,
	  "%s: path weight is already %g\n",FUNC_NAME,r->ph.w);
  }

  if ((ctx->russian_ref>0) && (eps > inte->albedo)) {   /* not reflected */
    r->ph.w=0; 
    r->over=1;
    *has_reflected=0;
    goto exit;
  }

  /* reflection (total or partial) */
  eps = ssp_rng_canonical(rng) ;
  if (eps < inte->spec) {   /* specular */
    d3_normalize(dir,r->hit.normal) ;
    reflect_spec(dir,r->dir) ;
  } else {                  /* lambertian */
    ssp_ran_hemisphere_cos(rng,up,sample,NULL) ;
    r->dir[0] = sample[0] ; r->dir[1] = sample[1] ; r->dir[2] = sample[2] ;
  }
  if (ctx->russian_ref==0) /* not russian ref */
    r->ph.w *= inte->albedo;
  if (inte->albedo==0) {
    r->over=1;
    *has_reflected=0; 
  } else {
    *has_reflected=1;
    r->ph.is_reflected+=1; 
  }

exit:
  return res ;
}

int
is_direct(struct context * ctx, struct ray *r) 
{
  int is_dir=0;
  if (ctx->do_direct_in_solid_angle>0) {
    if (d3_dot(r->dir,ctx->sun_dat.sun_dir[omp_get_thread_num()])>cos(ctx->direct_solid_angle)) /* this is direct */
      is_dir=1 ;
  }
  else {
    if (r->ph.is_scattered+r->ph.is_reflected<1)
      is_dir=1 ;
  }
  return is_dir ;
}

#endif
