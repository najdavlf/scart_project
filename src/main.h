#ifndef MAIN_H
#define MAIN_H

#include <star/s3d.h>
#include <star/ssp.h>

/* Star Cloudy Atmosphere Radiative Transfer (SCART)
 * 3D Monte Carlo method to solve atmospheric radiative transfer
 * Villefranque, Fournier, Couvreux, Blanco, Eymet, Roger
 * 2017
 */
#define SIZE_MSG 100
#define INIT_MSG char err_message[SIZE_MSG] = {'\0'} 
#define INIT_RES  res_T res = RES_OK ;

#define TRY(Func,Msg)  res=Func ; if(res!=RES_OK){sprintf(err_message,Msg);goto error;}
#define TRY_log(Func,Act)  res=Func ; if(res!=RES_OK){Act;goto error;}

#define TIME(S,Func,Msg)  printf(S) ; time(&t1);  res=Func ; time(&t2); if(res!=RES_OK){sprintf(err_message,Msg);goto error;} else { printf("\t...OK (%g sec)\n", difftime(t2,t1)) ; }
#define TIME_log(S,Func,Act)  logger_print(&ctx.logger,LOG_OUTPUT,S) ; time(&t1);  res=Func ; time(&t2); if(res!=RES_OK){Act;goto error;} else { logger_print(&ctx.logger,LOG_OUTPUT,"\t...OK (%g sec)\n", difftime(t2,t1)) ; }

typedef long unsigned int size_t ;

typedef int res_T;
#define RES_OK 0
#define RES_BAD_ARG 1
#define RES_MEM_ERR 2
#define RES_IO_ERR 3
#define RES_UNKNOWN_ERR 4
#define RES_BAD_OP 5
#define RES_EOF 6 /* End Of File */
#define RES_WARNING 7

res_T
realization
  (void* value,
   struct ssp_rng* rng,
   const unsigned ithread,
   void* Ncontext) ;

#endif
