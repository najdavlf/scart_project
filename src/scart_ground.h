#ifndef GROUND_H
#define GROUND_H

typedef enum {
  LAMBERTIAN,
  SPECULAR,
  TYPE_BRDF_NUMBER__
} type_brdf;

struct scart_ground {
  double albedo;  /* reflectivity of the ground (uniform) */
  type_brdf brdf; /* type of reflection of the ground (uniform) */
};

#endif
