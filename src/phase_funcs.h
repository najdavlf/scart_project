#ifndef PHASE_FUNC_H
#define PHASE_FUNC_H

#include <star/ssp.h>
#include <star/s3d.h>
#include <rsys/float3.h>
#include <rsys/double3.h>


double * 
ssp_ran_sphere_myhg
  (struct ssp_rng* rng, const double up[3], double sample[4], const void*context);

double * 
ssp_ran_sphere_mie
  (struct ssp_rng* rng, const double up[3], double sample[4], const void*context);

double * 
ssp_ran_sphere_ray
  (struct ssp_rng* rng, const double up[3], double sample[4], const void*context);

double * 
ssp_ran_two_stream
  (struct ssp_rng* rng, const double up[3], double sample[4], const void*context);


double
ssp_ran_sphere_myhg_pdf
  (struct ssp_rng* rng, const double up[3], const double sample[3], const void*context);

double
ssp_ran_sphere_mie_pdf
  (struct ssp_rng* rng, const double up[3], const double sample[3], const void*context);

double
ssp_ran_sphere_ray_pdf
  (struct ssp_rng* rng, const double up[3], const double sample[3], const void*context);

double
ssp_ran_two_stream_pdf
  (struct ssp_rng* rng, const double up[3], const double sample[3], const void*context);

#endif
