#include <netcdf.h>

/* LES fields */
#define xname "W_E_direction"     /* x coordinate vector  (km) */
#define yname "S_N_direction"     /* y coordinate vector  (km) */
#define zname "vertical_levels"   /* z coordinate vector  (km) */
#define zvarname "VLEV"           /* if z is 3D */
#define thname "THT"              /* Potential temperature (K) */
#define prname "PABST"            /* Pressure (Pa) */
#define rcname "RCT"              /* Liquid water mixing ratio (kg/kg) */
#define riname "RIT"              /* Ice water mixing ratio (kg/kg) */

/* ATM fields */
#define bands "narrow_bands"     /* narrow bands dimension */
#define nbqua "g_points"         /* g points dimension */
#define vname "vertical_levels"  /* interfaces between layers z coordinate vector (km) */
#define lname "vertical_layers"  /* center of the layers z coordinate vector (km) */
#define wname "wavelengths"      /* wavelength at narrow band interfaces */
#define isfna "norm_cum_ISF"     /* normalized cumulative incoming solar flux in each band */
#define tisfn "tot_ISF"          /* total incoming solar flux */
#define gpnbn "g_points_nb"      /* number of g points in the band */
#define wgnam "wg_points"        /* weight of each g point in each band */
#define kanam "ka"               /* abs coefficient for g points and levels */
#define ksnam "ks"               /* sca coefficient for g points and levels */
#define ssana "ssa"              /* single scatt alb for g points and levels */

/* SUN fields */
#define latname "latitude"
#define datname "date"
#define hourname "hour"
#define dayname "day"
#define szaname "sun_zenith_angle"
#define aziname "sun_azimuth_angle"
#define ltcumcd "latitudinal_timecumulative_cosined_DNI"
#define dilcumcd "date_integrated_latcumulative_cosined_DNI"
#define lcdname "local_cosined_DNI"
#define latcdname "latitudinal_cosined_DNI"
#define tcdname "total_cosined_DNI"

/* MIE fields */
#define muname  "mu"      /* cosine theta */
#define nuname  "lambda"  /* wavelength (nm) */
#define cpname  "cp"      /* discretized [0,1] */
#define gname   "g"       /* assymetry parameter */
#define ssaname "ssa"     /* single scatt albedo */
#define pename  "ecs"     /* extinction cross sec */
#define psname  "scs"     /* scattering cross sec */
#define paname  "acs"     /* absorption cross sec */
#define mename  "mecs"    /* massic ext cs */
#define msname  "mscs"    /* massic sca cs */
#define maname  "macs"    /* massic abs cs */
#define qename  "eqf"     /* ext quality factor */
#define qsname  "sqf"     /* sca quality factor */
#define qaname  "aqf"     /* abs quality factor */
#define pdfname "pdf"     /* normalized phase func */
#define icpname "mu_icp"  /* cos theta for discretized inverse cumulative */
#define disname "name"    /* name of size distrib function */
#define rmodname "rmod"   /* mean parameter of size distrib */
#define smodname "smod"   /* std parameter of size distrib */ 
#define r2name  "r2"      /* 2nd order moment of size distrib */
#define r3name  "r3"      /* 3rd order moment of size distrib */
#define reffname "reff"   /* effective radius of size distrib = r3/r2 */

res_T get_ncid(char * file, int * ncid) ;

size_t get_dim(int ncid, char * dim_name) ;

double get_delta(int ncid, char * var_name) ;
double get_delta3d(int ncid, char * var_name) ;

double get_val(int ncid, char * var_name, size_t ind[]) ;

res_T get_tab(int ncid, char * var_name, double * tab) ;
res_T get_tab_from(int ncid, char * var_name, double * tab, size_t ind[], size_t count[]) ;
