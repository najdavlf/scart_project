#ifndef CONTEXT_H
#define CONTEXT_H

#include "scart_sun.h"
#include "scart_cop.h"
#include "scart_les.h"
#include "scart_scene.h"
#include "scart_ground.h"
#include "scart_atmosphere.h"
#include <stdio.h>
#include <star/ssp.h>
#include <star/s3d.h>
#include <rsys/double3.h>
#include <rsys/logger.h>
#include <rsys/mem_allocator.h>

/* PRIV fields will be of size number_of_active_threads */
struct context ;

/* All possible events */
typedef enum {
  NO_EVENT, /* 0 */
  SURF_TOA, /* 1 */
  SURF_ABS, /* 2 */
  SURF_REF, /* 3 */
  SURF_NULL,/* 4 */
  VOL_ABS,  /* 5 */
  VOL_SCAT, /* 6 */
  VOL_NULL  /* 7 */
} type_event;


/* All possible species in the scene */
typedef enum {
  DROPLET,
  ICE,
  AIR,
  TYPE_COLLIDER_NUMBER__
} type_collider ;

/* Prototype phase function eval */
typedef double(*phase_function_eval)
  ( struct ssp_rng* rng, /* necessary for interpolation */
    const double up[3],
    const double sample[3],
    const void*ctx) ;

/* Prototype phase function */
typedef double*(*phase_function)
  ( struct ssp_rng* rng,
    const double up[3],
    double sample[4],
    const void*ctx) ;

union w {
  double* t; 
  double  s;
};
union w2 {
  double* t; 
  double  s;
};
struct counter{
  union w w;
  union w2 w2;
  size_t size;
};

struct results {
  struct counter * msi;
  struct counter * abs; 
  struct counter * net; 
  struct counter * fdn; 
  struct counter * fup; 
  struct counter * dir;
  struct counter * difdn;
  struct counter * difup;
  struct counter * fup_alb ;  /* count TOA photons as a function of number of reflections */
  struct counter * fdn_alb ;  /* count BOA photons as a function of number of reflections */
  struct counter * abs_alb ;  /* count abs photons as a function of number of reflections */
  struct counter * fup_alb_cov ;  /* associated covariance matrix */
  struct counter * fdn_alb_cov ;  /* associated covariance matrix */
  struct counter * abs_alb_cov ;  /* associated covariance matrix */
  struct counter * path_len;
  struct counter * transm;
  struct counter * albedo;
  struct counter * path_time;
  struct counter * real_time;
  struct counter * failures;
  double tmp_albedo;
  double tmp_transm;
  double * tmp_abs; 
  double * tmp_net; 
  double * tmp_fdn; 
  double * tmp_fup; 
  double * tmp_dir;
  double * tmp_difdn;
  double * tmp_difup;
  double * tmp_fup_alb;  /* count TOA photons as a function of number of reflections */
  double * tmp_fdn_alb;  /* count BOA photons as a function of number of reflections */
  double * tmp_abs_alb;  /* count abs photons as a function of number of reflections */
} ;
void 
accum(struct counter * cnt, int ind, double w);
void
init_counter(struct context * ctx, struct counter ** cnt, size_t size);
void
clean_counter(struct context * ctx, struct counter * cnt);

struct photon {
  size_t id;     /* Photon id */
  double nu ;    /* Photon wavelength */
  int liq_inu0 ; /* Wavelength index before wavelength value in liq cop LUT */
  int ice_inu0 ; /* Wavelength index before wavelength value in ice cop LUT */
  int liq_inu1 ; /* Wavelength index after wavelength value in liq cop LUT  */
  int ice_inu1 ; /* Wavelength index after wavelength value in ice cop LUT  */
  int liq_inu ;  /* Sampled wavelength index in liq cop LUT (inu0 or inu1)  */
  int ice_inu ;  /* Sampled wavelength index in ice cop LUT (inu0 or inu1)  */
  int ibd ;      /* Narrow band index in 0:nbbd-1 */
  int igp ;      /* Quadrature point index in 0:nbgp-1 */
  double w ;     /* Photon weight */
  double * sw ;  /* Array of photon sensivity weight to n parameters */
  int is_cloudy    ; /* Counts how many times the photon was scattered */
  int is_scattered ; /* Counts how many times the photon was scattered */
  int is_reflected ; /* Counts how many times the photon was reflected on the ground */
  int Ifirst, Jfirst ; /* index domain of first hit */
} ;

#define PHOTON_NULL__ {                                             \
  0,            /* id */                                            \
  0.,           /* wavelength */                                    \
  0, 0, 0, 0,   /* spectral indexes liq_inu[0|1], ice_inu[0|1] *\   \
  0, 0, 0, 0,   /* spectral indexes [liq|ice]_inu, ibd, igp    *\   \
  1.,           /* weight */                                        \
  NULL,         /* sensitivity weights */                           \
  0, 0, 0,      /* state (nb of scatt / refl) */                    \
  0, 0          /* index of first domain hit (periodicity) */       \
}
static const struct photon PHOTON_NULL = PHOTON_NULL__;

/* Ray structure */
struct ray {
  double path_len;    /* Physical length of the path */
  double org[3];       /* Origin of the ray */
  double dir[3];       /* Direction of the ray */
  double range[2];     /* Range along the ray on which intersections are tracked */
  struct s3d_hit hit;      /* Intersection */
  struct s3d_hit hit_from; /* Last intersection */
  struct photon ph ;  /* Electromag characteristics of the ray */
  int over ;          /* To end path */
};
#define RAY_NULL__ {\
  0.,\
  {0.,0.,0.},\
  {0.,0.,0.},\
  {0.,FLT_MAX},\
  S3D_HIT_NULL__,\
  S3D_HIT_NULL__,\
  PHOTON_NULL__,\
  0\
}
static const struct ray RAY_NULL = RAY_NULL__;

/* Collision properties structure to fill at each collision event */
struct collision {
  double * ka ;      /* Array absorption coefficient (one per collider) */
  double * ks ;      /* Array scattering coefficient (one per collider) */
  double * g ;       /* Array assymetry parameters (one per collider)   */ 
  double ka_tot ;    /* Total absorption coefficient */  
  double ks_tot ;    /* Total scattering coefficient */
  double ke_tot ;    /* Total extinction coefficient */
  int ind_collider ; /* Index of collider array */
} ;
#define COLLISION_NULL__ {\
  NULL,\
  NULL,\
  NULL,\
  0., 0., 0.,\
  0\
}
static const struct collision COLLISION_NULL = COLLISION_NULL__;


/* Event structure to fill at each new event */
struct event {
  double distance ;     /* Distance between ray origin and event */
  double location[3] ;  /* Event location */
  int is_at_surface ;  /* Event type (surfacic or volumic) */
  int filter ;         /* To know wether to filter last primitive */
  int has_scattered;   /* for MSI feature, find contribution to pixel?*/
  type_event type_event; /* type of event */
  struct collision collision;
} ;
#define EVENT_NULL__ {\
  0.,\
  {0., 0., 0.},\
  0,\
  0,\
  0,\
  NO_EVENT,\
  COLLISION_NULL__\
}
static const struct event EVENT_NULL = EVENT_NULL__;

/* Intersection properties structure to fill at each surfacic intersection */
struct intersection {
  type_surface type;
  double albedo;
  double spec;
  int ind_surf;
};
#define INTERSECTION_NULL__ {\
  0, \
  0, \
  0, \
  0  \
}
static const struct intersection INTERSECTION_NULL = INTERSECTION_NULL__;

/* Colliders user data, to fill at the context initialization */
struct col_data {
  int nbcolliders ;               /* Number of active colliders */
  int cloud, atmosphere ;         /* Boolean for active collider */
  type_collider * types ;         /* Array of type of active collider */
  phase_function * colliders_pf ; /* Phase functions of active colliders */
  phase_function_eval * colliders_pf_eval ; /* Eval pdf of active colliders */
} ;

/* User data, to fill at the initialization of context */
struct context {
  struct config * conf;
  struct results * results; /* array of results, one per CPU */
  struct les_data les_dat ;       /* LES user data (3D cloud fields) */
  struct cop_data liq_op_dat ;       /* Mie user data (qe,qs,g,pdf as a function of nu) */
  struct cop_data ice_op_dat ;   /* Mie user data (qe,qs,g,pdf as a function of nu) */
  struct atm_data atm_dat ;       /* Atmospheric user data (1D profiles) */
  struct scn_data scn_dat ;       /* Scene user data (s3d scene, virtual surfaces, bbox) */
  struct scart_ground grd_dat;    /* ground (albedo and brdf type) */
  struct col_data col_dat ;       /* Colliders data (nb, type, phase funcs) */
  struct sun_data sun_dat ;       /* Sun data (sun dir, inc sun as a function of lat and date) */
  int is_solved;
  int is_init_grd_dat;
  int is_init_les_dat;
  int is_init_liq_dat;
  int is_init_ice_dat;
  int is_init_atm_dat;
  int is_init_scn_dat;
  int is_init_col_dat;
  int is_init_sun_dat;
  int is_init_s3d ;
  int is_init_logger ;
  int is_init_allocator ;
  int is_init_ctx ;
  int is_init_conf;
  
  /* counting paths */
  size_t * first;
  size_t * real_num;

  /* Use context with caution : all variables are shared between active threads */

  int print  ;                  /* in debug mode : activate print log */
  size_t nthreads ;             /* Number of active threads */
  size_t nbpaths ;              /* Number of realisations to perform */
  int russian_ref ;             /* Switch to perform russian reflection at the ground */
  size_t ica ;                  /* Number of columns of subdomain in ICA mode */
  size_t map_ica ;              /* Write map corresponding to ICA subdomains */
  size_t max_reflections_symb ; /* Maximum reflections accounted for in symbolic MC */
  size_t nbbds;                 /* number of spectral bands in output */
  size_t nbabs;                 /* number of metrics "absorbed" */
  size_t nbnet;                 /* number of metrics "net flux" */
  size_t nbmsi;                 /* number of metrics "MSI rad" */
  size_t nbflx;                 /* number of metrics "flux"*/
  size_t nbsyf;                 /* number of metrics "symbolic flux" */
  size_t nbsya;                 /* number of metrics "symbolic abs" */
  size_t nbsyf2;                /* number of metrics "cov symb flux" */
  size_t nbsya2;                /* number of metrics "cov symb abs" */
  size_t nbres;                 /* number of metrics "flux" (remove LES bbox) */
  int two_stream ;              /* Switch to perform two-stream approximation */ 
  int delta_scaling ;           /* Switch to perform delta eddington scaling */
  int direct_diffuse ;          /* Switch to perform direct / diffuse computation */
  int spectral_integration ;    /* Switch to perform or not spectral integration */
  int write_spectral;           /* Switch to write spectral variables (in bands) */
  int do_direct_in_solid_angle; /* Switch to measure direct / diffuse as an instrument would */
  int remove_periodicity;       /* Switch to consider only one LES domain (of the first cloud hit) */
  int do_4VS;                   /* Switch to test <L> = 4V/S instead of RT computation */
  int do_msi;                   /* Switch to compute nadir radiances at TOA */
  int do_ad;                    /* Switch to average fluxes in "ad_coordinates" domain */
  double latitude;              /* If no latitudinal integration, latitude (degrees) */
  double day,hour;              /* If no time integration, day and hour */
  double mono_nu ;              /* If no spectral integration, monochromatic wavelength (nm) */
  double cut_nose ;             /* Demi-angle, in radians, under which scattered angle is considered as forward, rejecting the scattering event (cutting the [-cut_nose , cut_nose] phase function) */
  double direct_solid_angle ;   /* Demi-angle from the sun angle, in radians, under which the ray is considered as direct, counting it in the appropriate counter when partitionning direct / diffuse */
  double theta_diffus_down;     /* Angle of downward diffusion for 2-stream approximation, in radians */
  double theta_diffus_up;       /* Angle of upward diffusion for 2-stream approximation, in radians */
  double ad_coordinates[2][2];  /* Coordinates in which to average fluxes */
  double *the,*phi ;            /* PRIV Original or sampled theta,phi sun direction */
  size_t *I0,*J0 ;              /* PRIV In ICA mode: index of the subdomain where the photon is confined */
  double *ke_hat ;              /* PRIV To sample distance before event */
  int *index  ;                 /* PRIV to keep index in get_position */
  FILE * *debug ;               /* PRIV file to write into when in debug mode */
  struct logger logger ;        /* logger to manage log messages */
  struct mem_allocator allocator ;  /* allocator to allocate memory */
  double zboa, ztoa ;           /* altitudes of reflexion (boa) and emission (toa) */
} ;

res_T
init_context(struct context * ctx, 
             struct s3d_device ** s3d, 
             struct config * conf);

res_T
clean_context(struct context * ctx,
              struct s3d_device * s3d);
#endif
