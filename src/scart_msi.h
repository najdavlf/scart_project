#include "scart_context.h"
#include "path.h"

#include <star/ssp.h>
#include <star/s3d.h>
#include <omp.h>

res_T
compute_direct_to_msi(struct ssp_rng* rng,
                      struct context* ctx,
                      struct ray* r,
                      struct event* origin_event,
                      struct results* results)
{
  res_T res = RES_OK;
  struct event local_event = EVENT_NULL;
  const struct collision collision = origin_event->collision;
  const struct cop_data * cd;
  const int indcol = collision.ind_collider;
  const type_collider type_col = ctx->col_dat.types[indcol];
  const double dir_down[3] = {0., 0., -1.};
  const double view_dir[3] = {0., 0., 1.};
  const double origin_dir[3];
  double norm[3];
  double view[3];
  double w_pdf;       /* brdf or phase func evaluated in view dir */
  double w = r->ph.w; /* weight of current phantom path to nadir MSI pixel */
  size_t ind_h, ind_res;
  int print = 0;

  ASSERT(w>0.);

  d3_set_f3(norm, r->hit.normal);
  d3_normalize(norm, norm);
  d3_normalize(view, view_dir);

  local_event.filter = origin_event->filter;
  res = init_collision(&(local_event.collision), ctx->col_dat.nbcolliders);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not get init collision properties\n",FUNC_NAME);
    goto error;
  }

  if (print) printf("compute fr %f %f %f\n", SPLIT3(r->org));
  d3_set(origin_dir, r->dir);
  d3_set(r->dir, view);

  while (!(r->over)) {
    /* find next event in ray direction */
    res = find_next_event(rng, ctx, r, &local_event);
    if (res!=RES_OK) {
      logger_print(&ctx->logger, LOG_ERROR,
          "%s: could not find next event\n",FUNC_NAME);
      goto error;
    }

    /* move ray origin to event location */
    r->org[0] = local_event.location[0];
    r->org[1] = local_event.location[1];
    r->org[2] = local_event.location[2];
    if (print) printf("  moved to %f %f %f\t", SPLIT3(r->org));

    if (local_event.is_at_surface==1) {
      res = handle_surfacic_intersection(rng, ctx, r, &local_event, NULL);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not handle surfacic intersection\n",FUNC_NAME);
        goto error;
      }
      if (print) printf("surface\t");
    } else {
      res = handle_volumic_collision(rng, ctx, r, &local_event, NULL);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not handle volumic intersection\n",FUNC_NAME);
        goto error;
      }
      if (print) printf("volume\t");
    }
    if (print) printf("type %i\n", local_event.type_event);
    if (local_event.has_scattered) {
      r->over = 1;
      w *= 0; /* no direct contribution to MSI pixel */
    } else {
      if ((local_event.type_event == SURF_TOA) && (r->over)) { /* TOA */
        if (origin_event->is_at_surface) { /* brdf */
          w_pdf = ssp_ran_hemisphere_cos_pdf(norm, view);
        } else { /* phase func */
          switch (type_col) {
            case DROPLET:
              cd = &ctx->liq_op_dat;
              break;
            case ICE:
              cd = &ctx->ice_op_dat;
              break;
            case AIR:
              cd = NULL;
              break;
          }
          w_pdf = ctx->col_dat.colliders_pf_eval[indcol](rng, origin_dir, view, cd);
        }
        w *= w_pdf; /* weighted by phase function */
        if (print) printf("  reached TOA w/ weight %f (w_pdf from %f = %f)\n", w, origin_dir[2], w_pdf);

        /* register result */
        ind_res = get_e_ind(ctx, local_event.location, 0, &ind_h);
        accum(results->msi, ind_res, w);
      }
    }
  }

exit:
  if (clean_collision(&(local_event.collision)) != RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not clean collision properties\n",FUNC_NAME);
  }
  return res;
error:
  goto exit;
}
