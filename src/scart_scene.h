#ifndef SCENE_H
#define SCENE_H

#include <star/s3d.h>
struct context;

typedef enum {
  BOA,
  TOA,
  VIRTUAL,
  TYPE_SURFACE_NUMBER__
} type_surface;

/* Virtual surface to track photons */
struct surface_virtual {
  float position[3] ;
  float direction[3] ;
  float dimension[3] ;
  unsigned int id ;
  type_surface type ;
};

struct scart_surface {
  double height; /* all surfaces are horizontally infinite */
  double ql_max_above;             /* Maximum liquid cloud water content in layer below surface (one value) */
  double ql_max_below;             /* Maximum liquid cloud water content in layer below surface (one value) */
  double qi_max_above;             /* Maximum liquid cloud water content in layer below surface (one value) */
  double qi_max_below;             /* Maximum liquid cloud water content in layer below surface (one value) */
  double *ke_hat_gas_below;        /* Maximum extinction coefficient in layer below surface (spectral array) */
  double *ke_hat_gas_above;        /* Maximum extinction coefficient in layer above surface (spectral array) */
  unsigned int id ;
  type_surface type ;
};


struct scn_data {
  struct s3d_device * s3d ;            /* Scene containing s3d shapes               */
  struct s3d_scene * scene ;           /* Scene containing s3d shapes               */
  struct s3d_scene_view * scn_view ;   /* View of scene where rays can be traced    */
  struct scart_surface* surfaces;      /* Array of all the surfaces that can be intersected by s3d */
  size_t surfaces_number;              /* Total number of surfaces in the scene */
  size_t nbly ;                        /* Number of horizontal counters inc toa and boa */
  size_t nbx, nby  ;                   /* Discretization of horizontal counting surfaces */
  double zmin,zmax ;                   /* Lower and upper levels for virtual layers */
  double *z_surfaces;
  double *z_tab ;
};


res_T
init_scn_data(struct logger* logger, 
              struct scn_data * sd, 
              struct s3d_device ** s3d, 
              size_t nbly, size_t nbx, size_t nby,
              double zmin, double zmax,
              struct context * ctx) ;

res_T 
clean_scn(struct scn_data * sd);

#endif
