#ifndef CONST_H
#define CONST_H
#include <math.h>

#define CONST_MONO_NU 800.      /* default wavelength for monochromatic calculation [nm] */
#define Tref 273.15e+0          /* reference temperature [K] */
#define Pref 1.013E+5
#define R (Pref*22.4e-3)/Tref   /* perfect gas constant */
#define M_DA 28.96546e-3        /* molar mass of dry air [kg/mol] */
#define CP_DA 1005              /* specific heat capacity [J/kg/K] */
#define cgrav 9.81e+0           /* gravity constant [m.s^-2] */
#define rho_w 1000.             /* liquid water density [kg/m^-3] */
#define rho_a 1.204             /* dry air density [kg/m^-3] */
#define RHO_W 1000.             /* liquid water density [kg/m^-3] */
#define RHO_A 1.204             /* dry air density [kg/m^-3] */
#define LIQ_REFF 10.*pow(10,-9)     /* constant effective radius of droplets [km] */
#define LIQ_VEFF 0.01               /* constant effective variance of droplets distribution */
#define ICE_REFF 100.*pow(10,-9)    /* constant effective radius of ice crystals [km] */
#define ICE_VEFF 0.10               /* constant effective variance of crystals distribution */
#define LIQ_G_AS .86		            /* liquid water default g assymetry */
#define LIQ_SSA 1                   /* liquid water default single scattering albedo */
#define ICE_G_AS .86		            /* ice water default g assymetry */
#define ICE_SSA 1                   /* ice water default single scattering albedo */

/* With eta [cm^-1], rho [kg.m^-3] => ks [m^-1] */
#define c0 8.288e-23 
#define c1 1.13e-10  
#define c2 1.30e-20

#define SIZE_INF FLT_MAX/1e25
#define EPS 1e-5                /* max dist between surfaces to be considered at the same height */

#define KE_HAT_NULL 1.0e-30
#define KE_HAT_MAJ  1.00001  /* 1.00000001 */

#endif
