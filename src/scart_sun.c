#include "main.h"
#include "utils.h"
#include "scart_context.h"
#include "const.h"
#include "netCDF.h"
#include <omp.h>
#include <stdlib.h>
#include <string.h>

res_T
sample_sun(struct ssp_rng * rng, void * user_data) {
  res_T res = RES_OK;
  int ith=omp_get_thread_num();
  int ilat,idat;
  double dni;
  struct context * ctx = (struct context*)user_data;
  
  if ((ctx->sun_dat.do_time_int)&(ctx->sun_dat.do_lat_int)) {
    /* Case integration over space and time */
    /* First sample a latitude from timeaveraged cumulative */
    ilat = find_index(ctx->sun_dat.cumLatTable, ctx->sun_dat.nblat,  ssp_rng_canonical(rng));
    idat = find_index(&(ctx->sun_dat.cumLatDatTable[ilat*ctx->sun_dat.nbdate]), ctx->sun_dat.nbdate, ssp_rng_canonical(rng));

    ctx->the[ith] =  ctx->sun_dat.sza[ilat*ctx->sun_dat.nbdate + idat];
    ctx->phi[ith] = ctx->sun_dat.azim[ilat*ctx->sun_dat.nbdate + idat];
    ctx->sun_dat.w_dni[ith] = *(ctx->sun_dat.tab_dni);

  } else if ((ctx->sun_dat.do_time_int)&(!ctx->sun_dat.do_lat_int)) {
    /* Case integration over time at a given lat */
    ilat = find_index_after_value(ctx->sun_dat.lat, ctx->sun_dat.nblat, ctx->latitude);
    idat = find_index_after_value(&(ctx->sun_dat.cumLatDatTable[ilat*ctx->sun_dat.nbdate]), ctx->sun_dat.nbdate, ssp_rng_canonical(rng));

    ctx->the[ith] =  ctx->sun_dat.sza[ilat*ctx->sun_dat.nbdate + idat];
    ctx->phi[ith] = ctx->sun_dat.azim[ilat*ctx->sun_dat.nbdate + idat];
    ctx->sun_dat.w_dni[ith] = ctx->sun_dat.tab_dni[ilat];
  } else if (!((ctx->sun_dat.do_time_int)|(ctx->sun_dat.do_lat_int))&(ctx->latitude>=-90)) {
    /* Case given lat, day, hour */
    int iday = find_index(ctx->sun_dat.day, ctx->sun_dat.nbday, ctx->day);
    int ihou = find_index(ctx->sun_dat.hour, ctx->sun_dat.nbhour, ctx->hour);
    ilat = find_index(ctx->sun_dat.lat, ctx->sun_dat.nblat, ctx->latitude);
    idat = iday*ctx->sun_dat.nbhour + ihou;

    ctx->the[ith] =  ctx->sun_dat.sza[ilat*ctx->sun_dat.nbdate + idat];
    ctx->phi[ith] = ctx->sun_dat.azim[ilat*ctx->sun_dat.nbdate + idat];
    ctx->sun_dat.w_dni[ith] = ctx->sun_dat.tab_dni[ilat*ctx->sun_dat.nbdate + idat];

  } else if (!((ctx->sun_dat.do_time_int)|(ctx->sun_dat.do_lat_int))&(ctx->phi[ith]>-1)) {
    /* Case given theta, phi */
    /* Already filled ctx->the[i], ctx->phi[i] in init_context */
    if (ctx->atm_dat.atm_file) { dni=ctx->atm_dat.totISF; }
    else { dni=1.;} 
    ctx->sun_dat.w_dni[ith] = dni*cos(ctx->the[ith]/180.*PI);
  } else if (!((ctx->sun_dat.do_time_int)|(ctx->sun_dat.do_lat_int)) & (ctx->phi[ith]==-1)) {
    /* Case given theta, random phi */
    if (ctx->atm_dat.atm_file) { dni=ctx->atm_dat.totISF; }
    else { dni=1.;}
    ctx->phi[ith] = ssp_rng_canonical(rng)*2*180.;
    ctx->sun_dat.w_dni[ith] = dni*cos(ctx->the[ith]/180.*PI);
  } else {
    printf("not good\n"); }

  if (ctx->the[ith]>90) ctx->sun_dat.w_dni[ith]=0.;

  /*
  char filewrite[12];
  sprintf(filewrite,"angles%02i.txt",ith+1);
  FILE* fw = fopen(filewrite,"a");
  fprintf(fw,"%i %i %g %g %g\n",ilat,idat,ctx->the[ith],ctx->phi[ith],ctx->sun_dat.w_dni[ith]);
  fflush(fw);
  fclose(fw);
  */

  get_cart_dir(ctx->the[ith],ctx->phi[ith],ctx->sun_dat.sun_dir[ith]);

exit : 
  return res;
error : 
  goto exit;
}

res_T 
clean_sun(struct sun_data * sd, size_t nth) {
  res_T res = RES_OK;
  size_t i;
  /*int nth = omp_get_num_procs();*/
  if (sd->sza            != NULL) mem_rm(sd->sza           );
  if (sd->azim           != NULL) mem_rm(sd->azim          );
  if (sd->lat            != NULL) mem_rm(sd->lat           );
  if (sd->date           != NULL) mem_rm(sd->date          );
  if (sd->hour           != NULL) mem_rm(sd->hour          );
  if (sd->day            != NULL) mem_rm(sd->day           );
  if (sd->cumLatTable    != NULL) mem_rm(sd->cumLatTable   );
  if (sd->cumLatDatTable != NULL) mem_rm(sd->cumLatDatTable);
  for (i=0;i<nth;i++) {
    if (sd->sun_dir != NULL) mem_rm(sd->sun_dir[i]);
  }
  return res;
}

res_T 
set_sun_ncdf_dims(int ncid,struct sun_data * sd) {
  res_T res = RES_OK;
  sd->nblat = get_dim(ncid,latname);
  sd->nbdate = get_dim(ncid,datname);
  sd->nbhour = get_dim(ncid,hourname);
  sd->nbday = get_dim(ncid,dayname);
  return res;
}

res_T
set_sun_ncdf_fields(struct logger* logger, 
                    int ncid,
                    struct sun_data * sd,
                    size_t nth) {
  res_T res = RES_OK;
  size_t i; /* nth=omp_get_num_procs();*/

  sd->sza  = (double*)mem_alloc(sd->nblat*sd->nbdate*sizeof(*(sd->sza)));
  res = get_tab(ncid, szaname, sd->sza);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,szaname);
     goto error;
  } 

  sd->azim = (double*)mem_alloc(sd->nblat*sd->nbdate*sizeof(*(sd->azim)));
  res = get_tab(ncid, aziname, sd->azim);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,aziname);
     goto error;
  } 

  sd->lat  = (double*)mem_alloc(sd->nblat*sizeof(*(sd->lat)));
  res = get_tab(ncid, latname, sd->lat);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,latname);
     goto error;
  } 

  sd->date = (double*)mem_alloc(sd->nbdate*sizeof(*(sd->date)));
  res = get_tab(ncid, datname, sd->date);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,datname);
     goto error;
  } 

  sd->hour = (double*)mem_alloc(sd->nbhour*sizeof(*(sd->hour)));
  res = get_tab(ncid, hourname, sd->hour);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,hourname);
     goto error;
  } 

  sd->day  = (double*)mem_alloc(sd->nbday*sizeof(*(sd->day)));
  res = get_tab(ncid, dayname, sd->day);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,dayname);
     goto error;
  } 

  sd->cumLatTable  = (double*)mem_alloc(sd->nblat*sizeof(*(sd->cumLatTable)));
  res = get_tab(ncid, dilcumcd, sd->cumLatTable);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,dilcumcd);
     goto error;
  } 

  sd->cumLatDatTable  = (double*)mem_alloc(sd->nblat*sd->nbdate*sizeof(*(sd->cumLatDatTable)));
  res = get_tab(ncid, ltcumcd, sd->cumLatDatTable);
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s\n",FUNC_NAME,ltcumcd);
     goto error;
  } 

  if ((sd->do_time_int>0)&(sd->do_lat_int==0)) { 
    sd->tab_dni  = (double*)mem_alloc(sd->nblat*sizeof(*(sd->tab_dni)));
    res = get_tab(ncid, latcdname, sd->tab_dni);
  } else if ((sd->do_time_int>0)&(sd->do_lat_int>0)) {
    sd->tab_dni  = (double*)mem_alloc(1*sizeof(*(sd->tab_dni)));
    res = get_tab(ncid, tcdname, sd->tab_dni);
  } else if (sd->do_time_int+sd->do_lat_int<=0) {
    sd->tab_dni  = (double*)mem_alloc(sd->nblat*sd->nbdate*sizeof(*(sd->tab_dni)));
    res = get_tab(ncid, lcdname, sd->tab_dni);
  }
  if (res != RES_OK) {
     logger_print(logger, LOG_ERROR,
     "%s: could not read %s, %s or %s\n",FUNC_NAME,latcdname,tcdname,lcdname);
     goto error;
  } 

exit :
  return res;
error : 
  clean_sun(sd, nth);
  goto exit;
}

res_T
init_sun_data(struct logger* logger, 
              struct sun_data * sd,
              char * sun_file,
              double theta,
              double phi,
              int do_time_int,
              int do_lat_int,
              size_t nth) {
  res_T res = RES_OK;
  (void)theta;
  (void)phi;
  size_t i; /*nth=omp_get_num_procs();*/

  sd->do_time_int=do_time_int;
  sd->do_lat_int=do_lat_int;

  sd->sza=NULL;
  sd->azim=NULL;
  sd->lat=NULL;
  sd->date=NULL;
  sd->hour=NULL;
  sd->day=NULL;

  sd->cumLatTable=NULL;
  sd->cumLatDatTable=NULL;

  sd->tab_dni = NULL;

  sd->nblat=0;
  sd->nbdate=0;
  sd->nbhour=0;
  sd->nbday=0;
  
  sd->sun_file=sun_file;

  for (i=0;i<nth;i++) {
    sd->sun_dir[i] = (double*)mem_alloc(3*sizeof(*(sd->sun_dir[i])));
    sd->w_dni[i]=1;
  }

  if (sun_file!=NULL) {
    int ncid;
    res = get_ncid(sun_file, &ncid);
    if (res!=RES_OK) {
       logger_print(logger, LOG_ERROR,
       "%s: sun file does not exist or is not netCDF\n",FUNC_NAME);
       goto error;
    }

    res = set_sun_ncdf_dims(ncid,sd);
    if (res!=RES_OK) {
       logger_print(logger, LOG_ERROR,
       "%s: dim could not be found in sun file\n",FUNC_NAME);
       goto error;
    }

    res = set_sun_ncdf_fields(logger, ncid, sd, nth);
    if (res!=RES_OK) {
       logger_print(logger, LOG_ERROR,
       "%s: field could not be found in sun file\n",FUNC_NAME);
       goto error;
    }

    res = put_ncid(ncid);
    if (res!=RES_OK) {
      logger_print(logger, LOG_ERROR, 
        "%s: could not release netCDF id\n", FUNC_NAME);
      goto error;
    }
  }

exit : 
  return res;
error :
  goto exit;
}
