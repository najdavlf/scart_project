#ifndef REALIZATION_H
#define REALIZATION_H

res_T
emission(struct ssp_rng * rng, struct context * ctx, struct ray * r) ;

res_T
path(struct ssp_rng * rng, struct context * ctx, struct ray *r, struct results * res) ; 
#endif 
