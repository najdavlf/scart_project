#ifndef LES_H
#define LES_H

struct les_data {
  char * les_file ;            /* Name of the netCDF file for 3D cloud field */
  size_t nbx,nby,nbz ;         /* Number of points in the LES discretization */
  double dx,dy,dz ;            /* Spatial step for LES discretization */
  double xmin,xmax,ymin,ymax ; /* LES horizontal spatial boundaries */
  double zmin,zmax ;           /* LES vertical spatial boundaries */
  double *z_tab;               /* Center height of layers */
  double *lev_tab;             /* Levels height (interface between layers) */
  double *ql_tab;              /* Liquid water content (kg/m3) */
  double *qi_tab;              /* Ice water content (kg/m3) */
  double *rho_tab ;            /* Will compute rho in the whole field to get ks faster */
  double liq_reff, liq_veff;   /* effective radius and effective variance of liquid droplet size in km */
  double ice_reff, ice_veff;   /* effective radius and effective variance of ice crystals size in km */
  double *LWC_max; /* Max liquid water content at each level of the LES */
  double *IWC_max; /* Max ice water content at each level of the LES */
  double *rho_max; /* Max density at each level of the LES */
  size_t nbly;
  int use_default_rho;
  int z_reg ;
  int lev_bot, lev_top ;    /* 1st cloudy cell */
  double pos_bot, pos_top ; /* Delimitation of cloudy layer */
} ;

/* Fill the structure */

res_T
init_les_data(struct logger* logger, struct les_data * ld,  int cloud, int
    atmosphere, char * les_file, int z_reg, size_t nth);

/* Clean memory */
res_T 
clean_les(struct les_data * ld);

/* Access data */
double get_les_rho(struct les_data * ld, size_t ix, size_t iy, size_t iz);
double get_les_ql(struct les_data * ld,  size_t ix, size_t iy, size_t iz);
double get_les_qi(struct les_data * ld,  size_t ix, size_t iy, size_t iz);

#endif
