#include "main.h"
#include "utils.h"
#include "scart_context.h"
#include "scart_atmosphere.h"
#include "scart_les.h"
#include "scart_scene.h"
#include "const.h"
#include "netCDF.h"

#include <omp.h>
#include <stdlib.h>
#include <string.h>

#include <rsys/math.h>
#include <star/s3d.h>

res_T clean_scn(struct scn_data * sd) {
  int i;
  int nb=0; /* used to be passed as argument */
  res_T res = RES_OK;

  s3d_scene_clear(sd->scene);
  s3d_scene_view_ref_put(sd->scn_view);
  s3d_scene_ref_put(sd->scene);
  s3d_device_ref_put(sd->s3d);

  if (sd->surfaces != NULL) {
    for (i=0; i<sd->surfaces_number; i++) {
      if (sd->surfaces[i].ke_hat_gas_above != NULL) {
        mem_rm(sd->surfaces[i].ke_hat_gas_above);
      }
      if (sd->surfaces[i].ke_hat_gas_below != NULL) {
        mem_rm(sd->surfaces[i].ke_hat_gas_below);
      }
    }
    mem_rm(sd->surfaces);
    mem_rm(sd->z_surfaces);
  }

error : 
  return res ;
}

void 
scart_surf_get_position(const unsigned vertex_id, float position[3], void* data) /* of one vertex */
{
  const float *z = (float*) data;
  const float xmin = -SIZE_INF;
  const float xmax =  SIZE_INF;
  const float ymin = -SIZE_INF;
  const float ymax =  SIZE_INF;

  float xy_vertices[4/*#vertices*/][2/*#coords per vertex*/];

  /* Memory layout of the vertex positions
   *   (y)
   *   2.....3
   *  /     /
   * 0-----1  (x)
   */

  xy_vertices[0][0]=xmin; xy_vertices[0][1]=ymin;
  xy_vertices[1][0]=xmax; xy_vertices[1][1]=ymin;
  xy_vertices[2][0]=xmin; xy_vertices[2][1]=ymax;
  xy_vertices[3][0]=xmax; xy_vertices[3][1]=ymax;
  position[0] = xy_vertices[vertex_id][0];
  position[1] = xy_vertices[vertex_id][1];
  position[2] = *z; 
}

void
scart_surf_get_indexes(const unsigned primitive_id, unsigned indices[3], void* data) {

  /* The following array lists the indices toward the 3D vertices of each
   * triangle.

   *       ,2----,3
   *     ,'  \ ,'
   *    0----1'
   */
  unsigned rece_indices[2/*#triangles*/][3/*#indices per triangle*/] = { {0, 2, 1} , {1, 2, 3} };
  indices[0] = rece_indices[primitive_id][0];
  indices[1] = rece_indices[primitive_id][1];
  indices[2] = rece_indices[primitive_id][2];
}

int
hit_on_edge(const struct s3d_hit* hit)
{
   const float on_edge_eps = 1.e-4f;
   float w;
   ASSERT(hit && !S3D_HIT_NONE(hit));
   w = 1.f - hit->uv[0] - hit->uv[1];
   return eq_epsf(hit->uv[0], 0.f, on_edge_eps)
       || eq_epsf(hit->uv[0], 1.f, on_edge_eps)
       || eq_epsf(hit->uv[1], 0.f, on_edge_eps)
       || eq_epsf(hit->uv[1], 1.f, on_edge_eps)
       || eq_epsf(w, 0.f, on_edge_eps)
       || eq_epsf(w, 1.f, on_edge_eps);
}

int
discard_self_hit_and_edge
  (const struct s3d_hit* hit,
   const float ray_org[3],
   const float ray_dir[3],
   void * ray_data,
   void * filter_data) 
{
   const struct s3d_hit* hit_from = (struct s3d_hit*)ray_data;
   const struct context* ctx  = (struct context*)filter_data;
   (void)ray_org, (void)ray_dir;
   int ith = omp_get_thread_num() ;
   struct s3d_attrib pos_from ;
   struct s3d_attrib pos_new  ;
   s3d_primitive_get_attrib(&(hit_from->prim),S3D_POSITION, hit_from->uv, &pos_from);
   s3d_primitive_get_attrib(&(hit->prim),S3D_POSITION, hit->uv, &pos_new);
   if (ctx->print==1) {
       fprintf(ctx->debug[ith],"Entering discard self hit and edge, current hit %u at distance %g \n",hit->prim.scene_prim_id,hit->distance);
       fflush(ctx->debug[ith]) ;
   }
  
   if(!hit_from || S3D_HIT_NONE(hit_from) || S3D_PRIMITIVE_EQ(&(hit_from->prim),&S3D_PRIMITIVE_NULL) ) {
     if (ctx->print==1) {
       fprintf(ctx->debug[ith],"prim is null\n");
       fflush(ctx->debug[ith]) ;
     }
     return 0; /* No filtering */
   }

   if (ctx->print==1) {
     fprintf(ctx->debug[ith],"coming from prim %03i of shape %03i of type %i, filter prim %03i from shape %03i of type %i? \n",
       hit_from->prim.scene_prim_id, hit_from->prim.geom_id, ctx->scn_dat.surfaces[hit_from->prim.geom_id].type,
       hit->prim.scene_prim_id, hit->prim.geom_id, ctx->scn_dat.surfaces[hit->prim.geom_id].type);
     fprintf(ctx->debug[ith],"prim from was at pos %g %g %g \n",pos_from.value[0],pos_from.value[1],pos_from.value[2]);
     fprintf(ctx->debug[ith],"new prim is at pos %g %g %g \n",pos_new.value[0],pos_new.value[1],pos_new.value[2]);
     fflush(ctx->debug[ith]) ;
   }

   if(hit_from->prim.scene_prim_id==hit->prim.scene_prim_id) {
     if (ctx->print==1) {
       fprintf(ctx->debug[ith],"filter prim equals\n");
       fflush(ctx->debug[ith]) ;
     }
     return 1;
   }

   if(hit_from->prim.geom_id==hit->prim.geom_id) {
     if (ctx->print==1) {
       fprintf(ctx->debug[ith],"filter shape equals\n");
       fflush(ctx->debug[ith]) ;
     }
     return 1;
   }

   if (ctx->print==1) {
     fprintf(ctx->debug[ith],"distances hit %g hit_from %g\n", hit->distance, hit_from->distance) ;
     fflush(ctx->debug[ith]); 
   }
   if(eq_epsf(pos_from.value[2], pos_new.value[2], EPS)) {
     /* If the distances are the same, check that the hit lies on an edge
      * shared by the 2 primitives. */
     if (ctx->print==1) {
       fprintf(ctx->debug[ith],"filter hit on edge :\n\t distance hit prec at %i = %g; distance new hit at %i = %g\n",
                       hit_from->prim.scene_prim_id, hit_from->distance,hit->prim.scene_prim_id,hit->distance);
       fflush(ctx->debug[ith]) ;
     }
     return hit_on_edge(hit_from) && hit_on_edge(hit);
   }

   if (ctx->print==1) {
       fprintf(ctx->debug[ith],"no filtering\n");
       fflush(ctx->debug[ith]) ;
   }
   return 0;   
}


res_T
init_scn_data(struct logger* logger,
              struct scn_data * sd, 
              struct s3d_device ** s3d, 
              size_t nbly, size_t nbx, size_t nby,
              double zmin, double zmax,
              struct context * ctx) {
  res_T res = RES_OK;
  size_t nth, ngpoints;
  size_t nb_surfs;
  size_t ibd, igp, ilay, ilev, ibdgp, ibdgplay;
  double levlesbelow, levlesabove;
  size_t tmpcountpri;
  int cloud, atmosphere;
  struct scart_surface * surf; /* in the SCART world */
  struct s3d_shape* shape;     /* in the S3D world */
  struct s3d_vertex_data vertex_attribs[2];
  const struct atm_data ad = ctx->atm_dat;
  double maj = KE_HAT_MAJ;
  float z;
  size_t iz, set_ztop, set_zbot;
  double maxlwc, maxiwc, znext;
  size_t tmp;
  int izbelow, izabove;

  /*
  for (iz = 0; iz<ctx->les_dat.nbz; iz++) {
    printf("iz %lu, zlev %f \n", iz, ctx->les_dat.lev_tab[iz]);
  }
  */

  nth = ctx->nthreads; /*(size_t)omp_get_num_procs();*/
  ngpoints = ad.nbgp /*nb gp per band*/ * ad.nbbd /*nb bands*/;

  cloud = ctx->col_dat.cloud;
  atmosphere = ctx->col_dat.atmosphere;

  sd->nbly = nbly;         /* counters */
  sd->nbx  = nbx;
  sd->nby  = nby;
  sd->zmax = zmax;
  sd->zmin = zmin;

  res = s3d_device_create(NULL, NULL, 0, s3d);
  if (res!=RES_OK) {
    logger_print(logger,LOG_ERROR,
	  "%s: could not create scene device\n", FUNC_NAME);
    goto error;
  }

  sd->s3d = *s3d;
  ctx->is_init_s3d=1;

  res = s3d_scene_create(sd->s3d, &(sd->scene));
  if (res!=RES_OK) {
    logger_print(logger,LOG_ERROR,
	  "%s: could not create scene\n", FUNC_NAME);
    goto error;
  }

  /* allocate surfaces array */
  /* force atmosphere to be provided and take vertical levels from there */
  /* check if one of the atmo surfaces corresponds to the bottom or top cloud layer */
  set_zbot=1; set_ztop=1;
  for (ilev=0; ilev<ad.nbly; ilev++) { /* each level (interface) */
    if (eq_eps(ad.vertical_levels[ilev], ctx->les_dat.pos_bot, EPS)) {
      /* one of them corresponds to zbot, do not set surface for bottom of cloud layer */
      set_zbot=0;
    }
    if (eq_eps(ad.vertical_levels[ilev], ctx->les_dat.pos_top, EPS)) {
      /* one of them corresponds to ztop, do not set surface for top of cloud layer */
      set_ztop=0;
    }
  }
  /* if set_zbot or set_ztop, additional surfaces will be set after the nblv first ones
   * which means surface array will not be ordered in increasing heights
   * but z_surfaces can still be ordered in increasing heights? */

  nb_surfs = ad.nblv + set_ztop + set_zbot;
  sd->surfaces_number = nb_surfs;

  sd->z_surfaces = (double*)mem_alloc(nb_surfs*sizeof(*sd->z_surfaces));
  sd->surfaces   = (struct scart_surface*)mem_alloc(nb_surfs*sizeof(*sd->surfaces));

  /* loop over atmospheric levels */
  for (ilev=0; ilev<nb_surfs; ilev++) {  /* each level (interface) */
    surf = &(sd->surfaces[ilev]);       /* surface structure to fill */
    if (ilev < ad.nblv) {
      ilay=ilev;                             /* layer above the interface */
      z = (float)(ad.vertical_levels[ilev]); /* height of surface */
      sd->z_surfaces[ilev] = ad.vertical_levels[ilev];
      surf->height = ad.vertical_levels[ilev];
    } else if (ilev >= ad.nblv) { /* first of set_ztop or set_zbot */
      if (set_zbot==1) {
        ilay = find_index(ad.vertical_levels, ad.nblv, ctx->les_dat.pos_bot);
        z = (float)(ctx->les_dat.pos_bot);
        sd->z_surfaces[ilev] = ctx->les_dat.pos_bot;
        surf->height = ctx->les_dat.pos_bot;
        set_zbot = 0;
      } else if (set_ztop==1) {
        ilay = find_index(ad.vertical_levels, ad.nblv, ctx->les_dat.pos_top);
        z = (float)(ctx->les_dat.pos_top);
        sd->z_surfaces[ilev] = ctx->les_dat.pos_top;
        surf->height = ctx->les_dat.pos_top;
        set_ztop = 0;
      }
    }

    if (ilev==0) {
      surf->type = BOA;
    } else if (ilev==ad.nblv-1) {
      surf->type = TOA;
      surf->ql_max_above = 0.;
      surf->qi_max_above = 0.;
    } else {
      surf->type = VIRTUAL;
    }

    surf->ke_hat_gas_above = (double*)mem_alloc(ngpoints*sizeof(*surf->ke_hat_gas_above)); /* one per g point */
    surf->ke_hat_gas_below = (double*)mem_alloc(ngpoints*sizeof(*surf->ke_hat_gas_below)); /* one per g point */

    /* find the max wc below and above the current level */
    /* current level height ad.vertical_levels[ilev] */
    /* first level is BOA, LES cannot begin below that
       second level znext = ad.vertical_levels[ilev+1];
       find first izabove such that lev_tab[izabove] > znext 
    */

    z     = ad.vertical_levels[ilev];
    znext = ad.vertical_levels[ilev+1];

    if (ilev==0) {
      izbelow = 0;
      izabove = 0;
      maxlwc = 0.;
      maxiwc = 0.;
    } else if (ilev<=ctx->les_dat.nbz) {
      if (eq_eps(z, levlesabove, EPS)) {
        izbelow = izabove;
      } else {
        izbelow = izabove - 1;
      }
    }

    surf->ql_max_below = maxlwc; /* from previous "above" */
    surf->qi_max_below = maxiwc;

    levlesbelow = ctx->les_dat.lev_tab[izbelow];
    levlesabove = ctx->les_dat.lev_tab[izbelow];
    if (izabove >= ctx->les_dat.nbz) {
      levlesabove = ctx->les_dat.lev_tab[ctx->les_dat.nbz];
    }
    while ((izabove < ctx->les_dat.nbz) && (levlesabove < znext)) {
      izabove++;
      levlesabove = ctx->les_dat.lev_tab[izabove];
    }
    if (izabove > ctx->les_dat.nbz) {
      maxlwc = 0.;
      maxiwc = 0.;
    } else {
      maxlwc = ctx->col_dat.cloud * get_max(&(ctx->les_dat.LWC_max[izbelow]), izabove-izbelow, &tmp);
      maxiwc = ctx->col_dat.cloud * get_max(&(ctx->les_dat.IWC_max[izbelow]), izabove-izbelow, &tmp);
    }
    if (ilev > ctx->les_dat.nbz) {
      maxlwc = 0;
      maxiwc = 0;
    }
    if (ilev<ad.nblv-1) {
      /* wc max between levlesbelow and levlesabove */
      surf->ql_max_above = maxlwc; /*get_max(&(ctx->les_dat.LWC_max[izbelow]), izabove-izbelow, &tmp);*/
      surf->qi_max_above = maxiwc; /*get_max(&(ctx->les_dat.IWC_max[izbelow]), izabove-izbelow, &tmp);*/
    }

    /*
    printf("ilev %3i surf height %7.3f, z lev %7.3f, znext %7.3f, zbelow %7.3f, zabove %7.3f, \
        ql below %e, ql above %e, qi below %e, qi above %e\n", 
        ilev, surf->height, z, znext, levlesbelow, levlesabove, surf->ql_max_below,
        surf->ql_max_above, surf->qi_max_below, surf->qi_max_above);
    */

    for (ibd=0;ibd<ad.nbbd;ibd++) {
      for (igp=0;igp<ad.nbgp;igp++){
        ibdgp = ibd*ad.nbgp + igp;
        ibdgplay = ibd*ad.nbgp*ad.nbly + igp*ad.nbly + ilay;

        if (ilev==ad.nblv-1) {
          surf->ke_hat_gas_above[ibdgp] = KE_HAT_NULL; /* above TOA */
        } else {
          surf->ke_hat_gas_above[ibdgp] = fmax(atmosphere*maj*(ad.ka[ibdgplay] + ad.ks[ibdgplay]), KE_HAT_NULL);
        }

        if (ilev==0) {
          surf->ke_hat_gas_below[ibdgp] = KE_HAT_NULL;
        } else {
          surf->ke_hat_gas_below[ibdgp] = fmax(atmosphere*maj*(ad.ka[ibdgplay-1] + ad.ks[ibdgplay-1]), KE_HAT_NULL);
        }
      }
    }

    vertex_attribs[0].usage = S3D_POSITION;
    vertex_attribs[0].type = S3D_FLOAT3;                           
    vertex_attribs[0].get = scart_surf_get_position;
    vertex_attribs[1] = S3D_VERTEX_DATA_NULL;
    s3d_shape_create_mesh(sd->s3d, &shape);
    s3d_mesh_setup_indexed_vertices(shape, 2/*#triangles*/, scart_surf_get_indexes, 4/*#vertices*/, vertex_attribs, 1, &z);
    s3d_shape_get_id(shape, &(surf->id));
    s3d_mesh_set_hit_filter_function(shape, &discard_self_hit_and_edge, ctx);
    s3d_scene_attach_shape(sd->scene, shape);
    s3d_shape_ref_put(shape);
  }

  res = s3d_scene_view_create(sd->scene, S3D_TRACE, &(sd->scn_view));
  if (res!=RES_OK) {
    logger_print(logger,LOG_ERROR,
	  "%s: could not create scene view\n",FUNC_NAME);
    goto error;
  }

  s3d_scene_view_primitives_count(ctx->scn_dat.scn_view,&tmpcountpri); 
  printf("Nombre total de primitives dans la scène %i \n",tmpcountpri) ; 

exit :
  return res;
error :
  goto exit;
}
