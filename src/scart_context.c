#include "main.h"
#include "const.h"
#include "conf.h"
#include "scart_context.h"
#include "scart_atmosphere.h"
#include "scart_scene.h"
#include "scart_cop.h"
#include "scart_les.h"
#include "scart_ground.h"
#include "phase_funcs.h"
#include "utils.h"
#include <star/ssp.h>
#include <star/s3d.h>
#include <omp.h>
#include <math.h>

res_T
clean_context(struct context * ctx, struct s3d_device * s3d) {
  res_T res = RES_OK;
  struct config* conf; 
  size_t ith;
  size_t nth; /* = omp_get_num_procs();*/

  if (ctx->is_init_conf) {
    conf = ctx->conf;
    if (conf->write_file  != NULL) mem_rm(conf->write_file);
    if (conf->les_file    != NULL) mem_rm(conf->les_file);
    if (conf->atm_file    != NULL) mem_rm(conf->atm_file);
    if (conf->liq_op_file != NULL) mem_rm(conf->liq_op_file);
    if (conf->ice_op_file != NULL) mem_rm(conf->ice_op_file);
    if (conf->sun_file    != NULL) mem_rm(conf->sun_file);
  }
  if (ctx->is_init_ctx) {
    nth = ctx->nthreads;
    if ((ctx->is_init_scn_dat)!=0){
      res = clean_scn(&(ctx->scn_dat));
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean scene data\n",FUNC_NAME);
        goto error;
      }
    }

    if (ctx->is_init_les_dat!=0) {
      res = clean_les(&(ctx->les_dat));
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean LES data\n",FUNC_NAME);
        goto error;
      }
    }

    if (ctx->is_init_atm_dat!=0) {
      res = clean_atm(&(ctx->atm_dat));
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean atmospheric data\n",FUNC_NAME);
        goto error;
      }
    }
    
    if (ctx->is_init_liq_dat!=0) {
      res = clean_cop(&(ctx->liq_op_dat));
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean liquid optical properties data\n",FUNC_NAME);
        goto error;
      }
    }
    
    if (ctx->is_init_ice_dat!=0) {
      res = clean_cop(&(ctx->ice_op_dat));
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean ice optical properties data\n",FUNC_NAME);
        goto error;
      }
    }

    if (ctx->is_init_sun_dat!=0) {
      res = clean_sun(&(ctx->sun_dat), ctx->nthreads);
      if (res!=RES_OK) {
        logger_print(&ctx->logger,LOG_ERROR,
        "%s: could not clean sun data\n",FUNC_NAME);
        goto error;
      }
    }

    if ((ctx->is_init_col_dat)!=0){
      mem_rm(ctx->col_dat.types);
      mem_rm(ctx->col_dat.colliders_pf);
      mem_rm(ctx->col_dat.colliders_pf_eval);
    }

    mem_rm(ctx->first          );
    mem_rm(ctx->real_num       );
    mem_rm(ctx->liq_op_dat.g_nu   );
    mem_rm(ctx->liq_op_dat.ssa_nu );
    mem_rm(ctx->liq_op_dat.mse_nu );
    mem_rm(ctx->liq_op_dat.pdf_nu );
    mem_rm(ctx->liq_op_dat.icp_nu );
    mem_rm(ctx->ice_op_dat.g_nu   );
    mem_rm(ctx->ice_op_dat.ssa_nu );
    mem_rm(ctx->ice_op_dat.mse_nu );
    mem_rm(ctx->ice_op_dat.pdf_nu );
    mem_rm(ctx->ice_op_dat.icp_nu );
    mem_rm(ctx->atm_dat.ka_g   );
    mem_rm(ctx->atm_dat.ks_g   );
    mem_rm(ctx->atm_dat.ssa_g  );
    mem_rm(ctx->sun_dat.sun_dir);
    mem_rm(ctx->sun_dat.w_dni  );
    if (ctx->ica) {
      mem_rm(ctx->I0             );
      mem_rm(ctx->J0             );
    }
    mem_rm(ctx->the            );
    mem_rm(ctx->phi            );
    mem_rm(ctx->ke_hat         );
    mem_rm(ctx->index          );
    mem_rm(ctx->debug          );

    for (ith=0; ith<nth; ith++) {
      if (ctx->direct_diffuse) {
        clean_counter(ctx, ctx->results[ith].dir);
        clean_counter(ctx, ctx->results[ith].difdn);
        clean_counter(ctx, ctx->results[ith].difup);
      }
      if (ctx->do_msi) {
        clean_counter(ctx, ctx->results[ith].msi);
      }
      if (ctx->max_reflections_symb>0) {
        clean_counter(ctx, ctx->results[ith].fup_alb);
        clean_counter(ctx, ctx->results[ith].fdn_alb);
        clean_counter(ctx, ctx->results[ith].abs_alb);
        clean_counter(ctx, ctx->results[ith].fup_alb_cov);
        clean_counter(ctx, ctx->results[ith].fdn_alb_cov);
        clean_counter(ctx, ctx->results[ith].abs_alb_cov);
      }
      clean_counter(ctx, ctx->results[ith].net);
      clean_counter(ctx, ctx->results[ith].fdn);
      clean_counter(ctx, ctx->results[ith].fup);
      clean_counter(ctx, ctx->results[ith].abs);
      MEM_RM(&ctx->allocator, ctx->results[ith].path_len);
      MEM_RM(&ctx->allocator, ctx->results[ith].albedo);
      MEM_RM(&ctx->allocator, ctx->results[ith].transm);
      MEM_RM(&ctx->allocator, ctx->results[ith].path_time);
      MEM_RM(&ctx->allocator, ctx->results[ith].real_time);
      MEM_RM(&ctx->allocator, ctx->results[ith].failures);
    }
    mem_rm(ctx->results);
  }

exit:
  return res;
error : 
  goto exit;
}

/* ALLOCATION OF CRITICAL VARIABLES (NOT TO BE SHARED AMONGST THREADS) */
res_T
init_critical(struct context * ctx) {
  res_T res = RES_OK;
  size_t nth = ctx->nthreads; /*(size_t)omp_get_num_procs() ;*/
  struct config*conf = ctx->conf ; 

  ctx->first          = (size_t*)mem_alloc(nth*sizeof(*ctx->first));
  ctx->real_num       = (size_t*)mem_alloc(nth*sizeof(*ctx->real_num));

  ctx->liq_op_dat.g_nu   = (double*) mem_alloc(nth*sizeof(*ctx->liq_op_dat.g_nu  )) ;
  ctx->liq_op_dat.ssa_nu = (double*) mem_alloc(nth*sizeof(*ctx->liq_op_dat.ssa_nu  )) ;
  ctx->liq_op_dat.mse_nu = (double*) mem_alloc(nth*sizeof(*ctx->liq_op_dat.mse_nu)) ;
  ctx->liq_op_dat.pdf_nu = (double**)mem_alloc(nth*sizeof(*ctx->liq_op_dat.pdf_nu)) ;
  ctx->liq_op_dat.icp_nu = (double**)mem_alloc(nth*sizeof(*ctx->liq_op_dat.icp_nu)) ;

  ctx->ice_op_dat.g_nu   = (double*) mem_alloc(nth*sizeof(*ctx->ice_op_dat.g_nu  )) ;
  ctx->ice_op_dat.ssa_nu = (double*) mem_alloc(nth*sizeof(*ctx->ice_op_dat.ssa_nu  )) ;
  ctx->ice_op_dat.mse_nu = (double*) mem_alloc(nth*sizeof(*ctx->ice_op_dat.mse_nu)) ;
  ctx->ice_op_dat.pdf_nu = (double**)mem_alloc(nth*sizeof(*ctx->ice_op_dat.pdf_nu)) ;
  ctx->ice_op_dat.icp_nu = (double**)mem_alloc(nth*sizeof(*ctx->ice_op_dat.icp_nu)) ;

  ctx->atm_dat.ka_g   = (double**)mem_alloc(nth*sizeof(*ctx->atm_dat.ka_g)) ;
  ctx->atm_dat.ks_g   = (double**)mem_alloc(nth*sizeof(*ctx->atm_dat.ks_g)) ;
  ctx->atm_dat.ssa_g  = (double**)mem_alloc(nth*sizeof(*ctx->atm_dat.ssa_g)) ;

  ctx->sun_dat.sun_dir = (double**)mem_alloc(nth*sizeof(*ctx->sun_dat.sun_dir)) ;
  ctx->sun_dat.w_dni   = (float*)mem_alloc(nth*sizeof(*ctx->sun_dat.w_dni)) ;

  if (ctx->ica>0) {
    ctx->I0 = (size_t*)mem_alloc(nth*sizeof(*ctx->I0)) ;
    ctx->J0 = (size_t*)mem_alloc(nth*sizeof(*ctx->J0)) ;
  }

  ctx->the    = (double*)mem_alloc(nth*sizeof(*ctx->the)) ;
  ctx->phi    = (double*)mem_alloc(nth*sizeof(*ctx->phi)) ;
  ctx->ke_hat = (double*)mem_alloc(nth*sizeof(*ctx->ke_hat)) ;
  ctx->index  = (int*)mem_alloc(nth*sizeof(*ctx->index)) ;
  ctx->debug  = (FILE**)mem_alloc(nth*sizeof(*ctx->debug)) ;

  int i ;
  for (i=0;i<nth;i++) {
    ctx->the[i] = conf->theta_sun ;
    ctx->phi[i] = conf->phi_sun ;
    ctx->ke_hat[i] = 0. ;
    ctx->index[i]  = 0  ;
    ctx->debug[i]  = NULL ;
  }

  return res ;
}

res_T
init_col_data(struct col_data * cd, int mie_liq, int mie_ice, int cloud, int atmosphere) 
{
  res_T res = RES_OK;
  int ind = 0;

  cd->nbcolliders = (cloud*2)+atmosphere;
  cd->cloud = cloud;
  cd->atmosphere = atmosphere;
  cd->types = (type_collider*)mem_alloc(cd->nbcolliders*sizeof(*cd->types));
  cd->colliders_pf = (phase_function*)mem_alloc(cd->nbcolliders*sizeof(*cd->colliders_pf));
  cd->colliders_pf_eval = (phase_function_eval*)mem_alloc(cd->nbcolliders*sizeof(*cd->colliders_pf_eval));
  
  if (cloud>0) {
    ind = 2 ;
    cd->types[DROPLET] = DROPLET ; 
    cd->types[ICE] = ICE;
    if (mie_liq>0) { /* use mie pf */
      cd->colliders_pf[DROPLET]=&ssp_ran_sphere_mie;
      cd->colliders_pf_eval[DROPLET]=&ssp_ran_sphere_mie_pdf;
    } else {
      cd->colliders_pf[DROPLET]=&ssp_ran_sphere_myhg ;
      cd->colliders_pf_eval[DROPLET]=&ssp_ran_sphere_myhg_pdf;
    } 
    if (mie_ice>0) { /* use mie pf */
      cd->colliders_pf[ICE]=&ssp_ran_sphere_mie;
      cd->colliders_pf_eval[ICE]=&ssp_ran_sphere_mie_pdf;
    } else {
      cd->colliders_pf[ICE]=&ssp_ran_sphere_myhg ;
      cd->colliders_pf_eval[ICE]=&ssp_ran_sphere_myhg_pdf;
    }
  }

  if (atmosphere>0) {
    cd->types[ind] = AIR ; /* 0 if no cloud, 2 if cloud */
    cd->colliders_pf[ind]=&ssp_ran_sphere_ray ;
    cd->colliders_pf_eval[ind]=&ssp_ran_sphere_ray_pdf ;
  }

  return res ;
}

res_T
init_context(struct context * ctx, struct s3d_device ** s3d, struct config * conf)
{
  res_T res = RES_OK;
  ctx->nthreads=(size_t)(omp_get_num_procs());
  ctx->nbpaths = (size_t)(conf->nbpaths);
  ctx->cut_nose = conf->cut_nose*PI/180.; 
  ctx->two_stream = conf->do_two_stream;
  ctx->direct_solid_angle = conf->direct_solid_angle*PI/180.;
  ctx->theta_diffus_down = conf->theta_diffus_down*PI/180.;
  ctx->theta_diffus_up = conf->theta_diffus_up*PI/180.;
  ctx->do_direct_in_solid_angle = conf->do_direct_in_solid_angle;
  ctx->delta_scaling = conf->do_delta_scaling;
  ctx->spectral_integration=conf->do_spectral_integration;
  ctx->write_spectral=conf->do_write_spectral;
  ctx->latitude = conf->latitude;
  ctx->day = conf->day; 
  ctx->hour = conf->hour; 
  ctx->mono_nu = conf->monochromatic;
  ctx->direct_diffuse = conf->do_direct_diffuse;
  ctx->russian_ref = conf->do_russian_reflection;
  ctx->remove_periodicity = conf->do_remove_periodicity;
  ctx->do_4VS = conf->do_4VS;
  ctx->do_msi = conf->do_msi;
  ctx->do_ad  = conf->do_ad; /* store weights in 4 counters [[out_x&out_y , in_x&out_y], [out_x&in_y, in_x&in_y]] */
  ctx->ad_coordinates[0][0]  = conf->ad_coords[0][0];
  ctx->ad_coordinates[0][1]  = conf->ad_coords[0][1];
  ctx->ad_coordinates[1][0]  = conf->ad_coords[1][0];
  ctx->ad_coordinates[1][1]  = conf->ad_coords[1][1];
  if (ctx->do_ad) {
    conf->nbx=2; conf->nby=2;
  }
  /* print = 0 : no print
   * print = 1 : print debug 
   * print = 2 : print path
   * print = 3 : print angle at boa 
   * print = 4 : print horizontal distance */
  ctx->print = conf->print ;
  ctx->map_ica = conf->map_ica; /* Flag 0 or 1 */
  ctx->max_reflections_symb=(size_t)conf->max_reflections_symb;
  ctx->ica = conf->ica; /* facile si c'est 0, sinon, verifications plus loin */

  res = init_critical(ctx);
  if (res!=RES_OK) {
     logger_print(&ctx->logger, LOG_ERROR,
     "%s: error during initialization of critical variables\n",FUNC_NAME);
     goto error;
  }

  res = init_les_data(&(ctx->logger),
      &(ctx->les_dat),conf->cloud,conf->atmosphere,conf->les_file,conf->z_reg,
      ctx->nthreads);
  ctx->is_init_les_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: error during initialization of LES data\n",FUNC_NAME);
    goto error;
  }

  if ((conf->ica > 0) &&
      (((int)ctx->les_dat.nbx % (int)conf->ica)==0) && 
      (((int)ctx->les_dat.nby % (int)conf->ica)==0)) {
    ctx->ica = conf->ica;
    if (ctx->map_ica>0) { 
      conf->nbx = ctx->les_dat.nbx / ctx->ica;
      conf->nby = ctx->les_dat.nby / ctx->ica;
    }
  } else if (conf->ica>0) {
    logger_print(&ctx->logger, LOG_ERROR,
    "%s: parameter ica %lu must be a divider of domain size %lux%lu\n",
    FUNC_NAME,conf->ica,ctx->les_dat.nbx,ctx->les_dat.nby) ;
    res=RES_BAD_ARG ;
    goto error ;
  }

  res = init_cop_data(&(ctx->logger), &(ctx->liq_op_dat), conf->liq_op_file,
      conf->liq_g_assym, conf->liq_ssa, 0, ctx->nthreads);
  ctx->is_init_liq_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of liquid optical data\n",FUNC_NAME);
    goto error;
  } 

  res = init_cop_data(&(ctx->logger), &(ctx->ice_op_dat), conf->ice_op_file,
      conf->ice_g_assym, conf->ice_ssa, 1, ctx->nthreads);
  ctx->is_init_ice_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of ice optical data\n",FUNC_NAME);
    goto error;
  } 

  res = init_atm_data(&(ctx->logger), &(ctx->atm_dat),conf->atm_file,ctx->les_dat.zmin,ctx->les_dat.zmax, ctx->nthreads);
  ctx->is_init_atm_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of atmospheric data\n",FUNC_NAME);
    goto error;
  }
  if (ctx->atm_dat.vertical_levels[ctx->atm_dat.nblv-1] < ctx->les_dat.z_tab[ctx->les_dat.nbz-1]){
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: invalid atmospheric / LES fields; max height of atmosphere must be higher than max height of LES\n",FUNC_NAME);
    res = RES_BAD_ARG;
    goto error;
  };

  res = init_sun_data(&(ctx->logger), &(ctx->sun_dat), conf->sun_file,
		      conf->theta_sun, conf->phi_sun,
		      conf->do_time_integration,
		      conf->do_latitudinal_integration,
          ctx->nthreads);
  ctx->is_init_sun_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of physics data\n",FUNC_NAME);
    goto error;
  }

  ctx->zboa = fmin(ctx->les_dat.zmin,ctx->atm_dat.zmin);
  ctx->ztoa = fmax(ctx->les_dat.zmax,ctx->atm_dat.zmax);

  res = init_col_data(&(ctx->col_dat), (conf->liq_op_file!=NULL)&&(conf->use_mie_pf), (conf->ice_op_file!=NULL)&&(conf->use_mie_pf), conf->cloud, conf->atmosphere); 
  ctx->is_init_col_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of colliders data\n",FUNC_NAME);
    goto error;
  }

  res = init_scn_data(&(ctx->logger), &(ctx->scn_dat), s3d, conf->nbly, conf->nbx, conf->nby, conf->zmin, conf->zmax, ctx);
  ctx->is_init_scn_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of scene data\n",FUNC_NAME);
    goto error;
  }

  res = init_ground_data(&(ctx->logger), &(ctx->grd_dat), conf->ground_albedo, LAMBERTIAN, ctx->do_4VS);
  ctx->is_init_grd_dat=1;
  if (res!=RES_OK) {
    logger_print(&ctx->logger,LOG_ERROR,
    "%s: error during initialization of ground data\n",FUNC_NAME);
    goto error;
  }

  exit :
    return res ;
  error :
    goto exit ;
}
