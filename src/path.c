#include "main.h"
#include "scart_context.h"
#include "scart_msi.h"
#include "const.h"
#include "path.h"
#include <star/ssp.h>
#include <star/s3d.h>
#include <omp.h>

/*-------------------------------------------------------------*/
/*-------------------------- PATH -----------------------------*/
/*-------------------------------------------------------------*/

res_T
path(struct ssp_rng * rng, struct context * ctx, struct ray *r, struct results *results) {
  res_T res = RES_OK;
  
  int i;
  int ith = omp_get_thread_num() ;
  struct event ev ;
  struct ray inc_ray; /* incoming ray (before handling new event) */
  const size_t nth = ctx->nthreads; /*omp_get_num_procs();*/
  double ke_hat_save[nth];

  ev = EVENT_NULL; /* init event structure */

  res = init_collision(&(ev.collision), ctx->col_dat.nbcolliders);
  if (res!=RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not get init collision properties\n",FUNC_NAME);
    goto error;
  }

  if (ctx->print==1) {
    fprintf(ctx->debug[ith],
        "init sun dir %g %g weight %g\n\n",
        ctx->the[ith], ctx->phi[ith], ctx->sun_dat.w_dni[ith]);
    fprintf(ctx->debug[ith],
        "ke_hat ctx %g \n",
        ctx->ke_hat[ith]);
    fflush(ctx->debug[ith]) ;
  }

  while (!(r->over)){

    if (ctx->print==1) {
      fprintf(ctx->debug[ith],
          "\nLooking for next event...\n") ; 
    }

    res = find_next_event(rng, ctx, r, &ev);
    if (res!=RES_OK) {
      logger_print(&ctx->logger, LOG_ERROR,
          "%s: could not find next event\n",FUNC_NAME);
      goto error;
    }

    /* move ray origin to event location */
    r->org[0] = ev.location[0];
    r->org[1] = ev.location[1];
    r->org[2] = ev.location[2];

    if (ctx->print==1) {
      fprintf(ctx->debug[ith],
          "\n=> found next event at %g %g %g ;\t",
          ev.location[0], ev.location[1], ev.location[2]) ;
      fflush(ctx->debug[ith]) ;
    }

    if (ev.is_at_surface==1) {
      if (ctx->print==1) { fprintf(ctx->debug[ith], "event is at surface ");}

      res = handle_surfacic_intersection(rng, ctx, r, &ev, results);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not handle surfacic intersection\n",FUNC_NAME);
        goto error;
      }
    } else {
      if (ctx->print==1) { fprintf(ctx->debug[ith], "event is in volume  ");}

      res = handle_volumic_collision(rng, ctx, r, &ev, results);
      if (res!=RES_OK) {
        logger_print(&ctx->logger, LOG_ERROR,
            "%s: could not handle volumic intersection\n",FUNC_NAME);
        goto error;
      }
    }

    if (ctx->print==1) {
      fprintf(ctx->debug[ith], "has ray scattered ?  %i\n", ev.has_scattered);
      fprintf(ctx->debug[ith], "now ray org is at   %g %g %g\n", r->org[0],r->org[1],r->org[2]);
      fprintf(ctx->debug[ith], "with direction      %g %g %g\n", r->dir[0],r->dir[1],r->dir[2]);
    }

    if (ctx->do_msi) {
      if (ev.has_scattered) {
        inc_ray = *r;
        for (i=0;i<nth;i++) ke_hat_save[ith] = ctx->ke_hat[ith];
        res = compute_direct_to_msi(rng, ctx, &inc_ray, &ev, results);
        if (res!=RES_OK) {
          logger_print(&ctx->logger, LOG_ERROR,
              "%s: could not compute contribution to MSI\n",FUNC_NAME);
          goto error;
        }
        for (i=0;i<nth;i++) ctx->ke_hat[ith] = ke_hat_save[ith];
      }
    }
  }

  if (ctx->print==1) { fprintf(ctx->debug[ith], "\n\n END OF PATH \n\n"); }

exit:
  if (clean_collision(&(ev.collision)) != RES_OK){
    logger_print(&ctx->logger, LOG_ERROR,
        "%s: could not clean collision properties\n",FUNC_NAME);
  }
  accum(results->path_len, -1, r->path_len);
  if (ctx->print) fflush(ctx->debug[ith]);
  return res;
error :
  goto exit;
}
