#ifndef WRITE_RES_H
#define WRITE_RES_H

res_T
write_res(struct context * ctx, 
          struct results results,
          size_t nbpaths,
          double tolerance,
          char*write_file);

#endif 
