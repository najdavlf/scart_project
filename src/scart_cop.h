#ifndef COP_H
#define COP_H

struct cop_data {
  int ice;                           /* 0 for liq, 1 for ice */
  char * cop_file ;                  /* Name of netCDF file for cloud optical properties look up tables */
  size_t nbmu, nbnu, nbcp ;          /* Number of cos(th), nu, cumulated phase function*/
  double rmod, smod, reff, r2, r3 ;  /* Description of droplet size distribution (lognormal) */
  double g, ssa;                     /* Fixed g and ssa (provided by user) */
  double *mu_tab, *nu_tab, *cp_tab ; /* Arrays of cos(th), nu, cum phase func */
  double *g_tab, *ssa_tab ;          /* Arrays of asymmetry parameter and single scattering albedo as a function of wavelength */
  double *mse_tab, *mss_tab, *msa_tab ; /* Arrays of extinction, scattering and absorption massic cross sections as a function of wavelength */
  double *pdf_tab, *icp_tab ;        /* Phase function at all wavelengths, inverse cumulated pdf at all wavelengths */
  double *g_nu, *ssa_nu ;           /* PRIV asymetry parameter and single scattering albedo at a given wavelength */
  double *mse_nu;                    /* PRIV max extinction massic cross sections at wavelength */     
  double **pdf_nu, **icp_nu ;        /* PRIV pdf and invCDF at given wavelength (interpolated) */
};

res_T 
init_cop_data(struct logger* logger, struct cop_data * cd, char * cop_file,
    double g, double ssa, int ice, size_t nth);

res_T 
clean_cop(struct cop_data * cd);

/* access data */

/* ssa et g */
double get_cop_ssa(struct cop_data * cd, double reff, double veff, int ilambda);
double get_cop_g  (struct cop_data * cd, double reff, double veff, int ilambda);
/* sections efficaces */ 
double get_cop_se (struct cop_data * cd, double reff, double veff, int ilambda);
double get_cop_sa (struct cop_data * cd, double reff, double veff, int ilambda);
double get_cop_ss (struct cop_data * cd, double reff, double veff, int ilambda);

#endif
