#include "main.h"
#include "const.h"
#include "scart_ground.h"
#include "scart_context.h"


res_T
init_ground_data(struct logger* logger,
                 struct scart_ground * grd_dat,
                 double ground_albedo, 
                 type_brdf brdf,
                 int do_4VS) {
  grd_dat->albedo = ground_albedo;
  grd_dat->brdf = brdf;
  if (do_4VS) grd_dat->albedo = 1;
  return RES_OK;
}
